module.exports = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  testEnvironment: 'node',
  testRegex: '((\\.|/)(test|spec))\\.[jt]sx?$',
  preset: 'ts-jest',
  coverageDirectory: 'coverage',
};
