module.exports = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  testEnvironment: 'node',
  testRegex: '((\\.|/)(e2e-test|e2e-spec))\\.[jt]sx?$',
  preset: 'ts-jest',
  coverageDirectory: 'e2e-coverage',
};
