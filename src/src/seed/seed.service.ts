import { v4 as uuidv4 } from 'uuid';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { hash } from 'argon2';
import { UserRole, UserStatus } from '@beescreens/beescreens';
import { UsersService } from '../users/users.service';
import { UserEntity } from '../users/entities';

@Injectable()
export class SeedService implements OnModuleInit {
  constructor(
    readonly configService: ConfigService,
    readonly userService: UsersService,
  ) {}

  async onModuleInit(): Promise<void> {
    // Create the default user
    const defaultUser = new UserEntity(
      uuidv4(),
      this.configService.get<string>('DEFAULT_USERNAME') || 'admin',
      this.configService.get<string>('DEFAULT_PASSWORD') || 'admin',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    defaultUser.password = await hash(defaultUser.password);

    try {
      await this.userService.createOne(defaultUser);
    } catch (error) {
      if (error instanceof Error) {
        // Ignore
      } else {
        throw error;
      }
    }
  }
}
