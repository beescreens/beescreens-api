import { Logger } from 'winston';
import { Repository, DeepPartial, SelectQueryBuilder } from 'typeorm';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';

export abstract class AbstractService<T> {
  constructor(
    readonly className: string,
    readonly logger: Logger,
    readonly repository: Repository<T>,
  ) {}

  async createOne(entity: T): Promise<T> {
    this.logger.info(`entity: ${JSON.stringify(entity)}`, {
      context: `${this.className}::${this.createOne.name}`,
    });

    let createdEntity: T;

    try {
      createdEntity = await this.repository.save(entity);
    } catch (error) {
      this.logger.warn(`entity not created`, {
        context: `${this.className}::${this.createOne.name}`,
      });

      throw new Error();
    }

    return createdEntity;
  }

  async getPagination(
    route: string,
    page: number,
    limit: number,
    queryBuilder: SelectQueryBuilder<T>,
  ): Promise<Pagination<T>> {
    this.logger.info(`page: ${page}, limit: ${limit}`, {
      context: `${this.className}::${this.getPagination.name}`,
    });

    if (page < 1 || limit < 1) {
      throw new Error();
    }

    return paginate<T>(queryBuilder, {
      page,
      limit,
      route,
    });
  }

  async getOne(params: DeepPartial<T>): Promise<T> {
    this.logger.info(`params: ${JSON.stringify(params)}`, {
      context: `${this.className}::${this.getOne.name}`,
    });

    const foundEntity = await this.repository.findOne(params);

    if (!foundEntity) {
      this.logger.warn(`entity not found - params: ${JSON.stringify(params)}`, {
        context: `${this.className}::${this.getOne.name}`,
      });

      throw new Error();
    }

    return foundEntity;
  }

  async deleteOne(params: DeepPartial<T>): Promise<T> {
    this.logger.info(`params: ${JSON.stringify(params)}`, {
      context: `${this.className}::${this.deleteOne.name}`,
    });

    const entity = await this.repository.findOne(params);

    if (!entity) {
      this.logger.warn(`entity not found - params: ${JSON.stringify(params)}`, {
        context: `${this.className}::${this.deleteOne.name}`,
      });

      throw Error();
    }

    return this.repository.remove(entity);
  }

  async updateOne(entityId: string, params: DeepPartial<T>): Promise<T> {
    this.logger.info(`params: ${JSON.stringify(params)}`, {
      context: `${this.className}::${this.updateOne.name}`,
    });

    const user = await this.repository.findOne(entityId);

    if (!user) {
      this.logger.warn(`entity not found - params: ${JSON.stringify(params)}`, {
        context: `${this.className}::${this.updateOne.name}`,
      });

      throw Error();
    }

    await this.repository.merge(user, params);

    return this.repository.save(user);
  }
}
