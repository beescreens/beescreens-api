import { Socket } from 'socket.io';
import {
  WsCommonEvents,
  WsMessageEvents,
  Message,
} from '@beescreens/beescreens';
import { WsResponse } from '@nestjs/websockets';
import { Logger } from 'winston';
import { MessageDto } from '../../messages/dto';

export abstract class AbstractGateway {
  constructor(
    readonly className: string,
    readonly logger: Logger,
    readonly clients: Map<Socket, string>,
  ) {}

  handleConnection(client: Socket): void {
    this.logger.info(`client: ${client.id}`, {
      context: `${this.className}::${this.handleConnection.name}`,
    });
  }

  handleDisconnect(client: Socket): void {
    this.logger.info(`client: ${client.id}`, {
      context: `${this.className}::${this.handleDisconnect.name}`,
    });

    this.clients.delete(client);
  }

  async join(
    room: string,
    client: Socket,
    clientId: string,
  ): Promise<WsResponse> {
    this.logger.info(`room: ${room}, clientId: ${clientId}`, {
      context: `${this.className}::${this.join.name}`,
    });

    client.join(room);

    this.clients.set(client, clientId);

    return {
      event: `${WsCommonEvents.JOINED}`,
      data: undefined,
    };
  }

  async leave(clientId: string): Promise<void> {
    this.logger.info(`clientId: ${clientId}`, {
      context: `${this.className}::${this.leave.name}`,
    });

    const clientSockets = await this.lookup(clientId);

    clientSockets.forEach((socket: Socket) => {
      socket.disconnect(true);

      this.clients.delete(socket);
    });
  }

  async lookup(clientId: string): Promise<Socket[]> {
    this.logger.info(`clientId: ${clientId}`, {
      context: `${this.className}::${this.lookup.name}`,
    });

    const results = [...this.clients.entries()]
      .filter(({ 1: v }) => v === clientId)
      .map(([k]) => k);

    if (results.length === 0) {
      throw new Error();
    }

    return results;
  }

  async emitToAll<T>(
    rooms: string[],
    event: string,
    payload: T,
  ): Promise<void> {
    this.logger.info(
      `rooms: ${JSON.stringify(
        rooms,
      )}, event: ${event}, payload: ${JSON.stringify(payload)}`,
      {
        context: `${this.className}::${this.emitToAll.name}`,
      },
    );

    rooms.forEach(async (room) => {
      this.clients.forEach((clientId, client) => {
        if (client.rooms[room] !== undefined) {
          client.emit(event, payload);
        }
      });
    });
  }

  async emitToOne<T>(
    clientId: string,
    event: string,
    payload: T,
  ): Promise<void> {
    this.logger.info(
      `clientId: ${clientId}, event: ${event}, payload: ${JSON.stringify(
        payload,
      )}`,
      {
        context: `${this.className}::${this.emitToOne.name}`,
      },
    );

    const clientSockets = await this.lookup(clientId);

    clientSockets.forEach((socket: Socket) => {
      socket.emit(event, payload);
    });
  }

  async emitMessageToAll(rooms: string[], message: Message): Promise<void> {
    const messageDto = new MessageDto(
      message.title,
      message.content,
      message.level,
      message.type,
    );

    this.emitToAll(rooms, WsMessageEvents.MESSAGE, messageDto);
  }

  async emitMessageToOne(clientId: string, message: Message): Promise<void> {
    const messageDto = new MessageDto(
      message.title,
      message.content,
      message.level,
      message.type,
    );

    this.emitToOne(clientId, WsMessageEvents.MESSAGE, messageDto);
  }
}
