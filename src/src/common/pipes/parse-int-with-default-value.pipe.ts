import { PipeTransform, Injectable } from '@nestjs/common';

@Injectable()
export class DefaultIntValuePipe implements PipeTransform<string, number> {
  constructor(readonly defaultValue: number) {}

  transform(value: string): number {
    let val = parseInt(value, 10);

    if (Number.isNaN(val)) {
      val = this.defaultValue;
    }

    return val;
  }
}
