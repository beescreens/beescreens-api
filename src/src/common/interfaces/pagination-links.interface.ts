export interface PaginationLinks {
  first: string;
  last: string;
  next: string;
  previous: string;
}
