import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const ExceptionLinkSpecs: LinkObject = {
  description:
    'Check if an exception has been emitted by the WebSocket server.',
  operationId: 'onException',
};
