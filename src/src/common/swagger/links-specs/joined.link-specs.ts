import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const JoinedLinkSpecs: LinkObject = {
  description:
    'Check when entity has been authenticated and joined the server.',
  operationId: 'onJoined',
};
