import { ApiQueryOptions } from '@nestjs/swagger';

export const PaginationPageQuerySpecs: ApiQueryOptions = {
  name: 'page',
  description: 'The page to retrieve.',
  type: 'integer',
  required: false,
};
