import { ApiQueryOptions } from '@nestjs/swagger';

export const PaginationLimitQuerySpecs: ApiQueryOptions = {
  name: 'limit',
  description: 'The number of items to retrieve.',
  type: 'integer',
  required: false,
};
