import { ApiResponseOptions } from '@nestjs/swagger';

export const MissingOrIncorrectFieldsResponseSpecs: ApiResponseOptions = {
  description:
    'Some fields are missing or incorrect. Please check your inputs.',
};
