import { ApiResponseOptions } from '@nestjs/swagger';

export const MissingOrIncorrectParamsResponseSpecs: ApiResponseOptions = {
  description:
    'Some parameters are missing or incorrect. Please check your inputs.',
};
