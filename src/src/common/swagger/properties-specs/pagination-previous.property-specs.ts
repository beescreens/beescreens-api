import { ApiPropertyOptions } from '@nestjs/swagger';

export const PaginationPreviousPropertySpecs: ApiPropertyOptions = {
  description: 'The link to the previous page.',
  type: 'string',
};
