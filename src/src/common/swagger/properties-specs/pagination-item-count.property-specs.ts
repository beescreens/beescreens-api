import { ApiPropertyOptions } from '@nestjs/swagger';

export const PaginationItemCountPropertySpecs: ApiPropertyOptions = {
  description: 'The number of items retrieved.',
  type: 'integer',
};
