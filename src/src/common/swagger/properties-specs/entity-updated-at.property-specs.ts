import { ApiPropertyOptions } from '@nestjs/swagger';

export const EntityUpdatedAtPropertySpecs: ApiPropertyOptions = {
  description: 'The date when the entity was updated.',
  type: 'string',
  format: 'date-time',
};
