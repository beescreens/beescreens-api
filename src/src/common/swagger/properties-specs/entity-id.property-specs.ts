import { ApiPropertyOptions } from '@nestjs/swagger';

export const EntityIdPropertySpecs: ApiPropertyOptions = {
  description: 'ID of the entity',
  type: 'string',
  format: 'uuid',
};
