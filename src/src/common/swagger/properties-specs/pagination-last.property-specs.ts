import { ApiPropertyOptions } from '@nestjs/swagger';

export const PaginationLastPropertySpecs: ApiPropertyOptions = {
  description: 'The link to the last page.',
  type: 'string',
};
