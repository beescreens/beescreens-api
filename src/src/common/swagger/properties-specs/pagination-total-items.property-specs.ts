import { ApiPropertyOptions } from '@nestjs/swagger';

export const PaginationTotalItemsPropertySpecs: ApiPropertyOptions = {
  description: 'The total number of items.',
  type: 'integer',
};
