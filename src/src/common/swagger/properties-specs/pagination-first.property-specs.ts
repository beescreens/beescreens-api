import { ApiPropertyOptions } from '@nestjs/swagger';

export const PaginationFirstPropertySpecs: ApiPropertyOptions = {
  description: 'The link to the first page.',
  type: 'string',
};
