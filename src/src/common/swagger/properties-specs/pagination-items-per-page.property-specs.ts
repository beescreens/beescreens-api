import { ApiPropertyOptions } from '@nestjs/swagger';

export const PaginationItemsPerPagePropertySpecs: ApiPropertyOptions = {
  description: 'The number of items per page.',
  type: 'integer',
};
