import { ApiPropertyOptions } from '@nestjs/swagger';

export const PaginationNextPropertySpecs: ApiPropertyOptions = {
  description: 'The link to the next page.',
  type: 'string',
};
