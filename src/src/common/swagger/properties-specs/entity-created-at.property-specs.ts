import { ApiPropertyOptions } from '@nestjs/swagger';

export const EntityCreatedAtPropertySpecs: ApiPropertyOptions = {
  description: 'The date when the entity was created.',
  type: 'string',
  format: 'date-time',
};
