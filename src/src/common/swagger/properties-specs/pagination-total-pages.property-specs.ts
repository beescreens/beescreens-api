import { ApiPropertyOptions } from '@nestjs/swagger';

export const PaginationTotalPagesPropertySpecs: ApiPropertyOptions = {
  description: 'The number of total pages.',
  type: 'integer',
};
