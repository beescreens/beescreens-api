import { ApiPropertyOptions } from '@nestjs/swagger';

export const EntityApiKeyPropertySpecs: ApiPropertyOptions = {
  description: 'API key to access the API.',
  type: 'string',
  format: 'uuid',
};
