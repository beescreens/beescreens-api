import { ApiPropertyOptions } from '@nestjs/swagger';

export const PaginationCurrentPagePropertySpecs: ApiPropertyOptions = {
  description: 'The currant page number.',
  type: 'integer',
};
