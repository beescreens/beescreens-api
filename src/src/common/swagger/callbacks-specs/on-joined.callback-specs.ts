import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const OnJoinedCallbackSpecs: PathItemObject = {
  get: {
    summary:
      'The user/entity has been authenticated and joined the WebSockets server',
    description:
      'The user/entity has been authenticated and joined the WebSockets server.',
    operationId: 'onJoined',
    responses: {
      'N/A': {
        description:
          'The user/entity has been authenticated and joined the WebSockets server.',
      },
    },
  },
};
