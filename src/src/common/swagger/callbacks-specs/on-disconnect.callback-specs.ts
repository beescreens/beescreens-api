import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const OnDisconnectCallbackSpecs: PathItemObject = {
  get: {
    summary: 'The connection to the server has been lost',
    description: 'The connection to the server has been lost.',
    operationId: 'onDisconnect',
    responses: {
      'N/A': {
        description: 'The connection to the server has been lost.',
      },
    },
  },
};
