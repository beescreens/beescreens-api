import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const OnExceptionCallbackSpecs: PathItemObject = {
  get: {
    summary: 'An exception has been emitted by the WebSocket server',
    description: 'An exception has been emitted by the WebSocket server.',
    operationId: 'onException',
    responses: {
      'N/A': {
        description: 'An exception has been emitted by the WebSocket server.',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                status: {
                  type: 'string',
                  description: 'Status of the WebSocket exception.',
                },
                message: {
                  type: 'string',
                  description: 'Message of the WebSocket exception.',
                },
              },
            },
          },
        },
      },
    },
  },
};
