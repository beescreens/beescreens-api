import { ApiProperty } from '@nestjs/swagger';
import { IsInt, Min } from 'class-validator';
import {
  PaginationItemCountPropertySpecs,
  PaginationTotalItemsPropertySpecs,
  PaginationItemsPerPagePropertySpecs,
  PaginationTotalPagesPropertySpecs,
  PaginationCurrentPagePropertySpecs,
} from '../swagger/properties-specs';
import { PaginationMeta } from '../interfaces';

export class PaginationMetaDto implements PaginationMeta {
  @ApiProperty(PaginationCurrentPagePropertySpecs)
  @IsInt()
  @Min(0)
  readonly currentPage: number;

  @ApiProperty(PaginationItemCountPropertySpecs)
  @IsInt()
  @Min(0)
  readonly itemCount: number;

  @ApiProperty(PaginationItemsPerPagePropertySpecs)
  @IsInt()
  @Min(0)
  readonly itemsPerPage: number;

  @ApiProperty(PaginationTotalItemsPropertySpecs)
  @IsInt()
  @Min(0)
  readonly totalItems: number;

  @ApiProperty(PaginationTotalPagesPropertySpecs)
  @IsInt()
  @Min(0)
  readonly totalPages: number;

  constructor(
    currentPage: number,
    itemCount: number,
    itemsPerPage: number,
    totalItems: number,
    totalPages: number,
  ) {
    this.currentPage = currentPage;
    this.itemCount = itemCount;
    this.itemsPerPage = itemsPerPage;
    this.totalItems = totalItems;
    this.totalPages = totalPages;
  }
}
