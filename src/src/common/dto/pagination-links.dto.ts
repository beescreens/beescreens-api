import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import {
  PaginationNextPropertySpecs,
  PaginationPreviousPropertySpecs,
  PaginationFirstPropertySpecs,
  PaginationLastPropertySpecs,
} from '../swagger/properties-specs';
import { PaginationLinks } from '../interfaces';

export class PaginationLinksDto implements PaginationLinks {
  @ApiProperty(PaginationFirstPropertySpecs)
  @IsString()
  readonly first: string;

  @ApiProperty(PaginationLastPropertySpecs)
  @IsString()
  readonly last: string;

  @ApiProperty(PaginationNextPropertySpecs)
  @IsString()
  readonly next: string;

  @ApiProperty(PaginationPreviousPropertySpecs)
  @IsString()
  readonly previous: string;

  constructor(first: string, last: string, next: string, previous: string) {
    this.first = first;
    this.last = last;
    this.next = next;
    this.previous = previous;
  }
}
