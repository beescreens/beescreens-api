import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { EntityApiKey } from '../interfaces';
import { EntityApiKeyPropertySpecs } from '../swagger/properties-specs';

export class EntityApiKeyDto implements EntityApiKey {
  @ApiProperty(EntityApiKeyPropertySpecs)
  @IsString()
  readonly apiKey: string;

  constructor(apiKey: string) {
    this.apiKey = apiKey;
  }
}
