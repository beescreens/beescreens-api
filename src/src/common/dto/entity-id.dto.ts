import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { EntityId } from '../interfaces';
import { EntityIdPropertySpecs } from '../swagger/properties-specs';

export class EntityIdDto implements EntityId {
  @ApiProperty(EntityIdPropertySpecs)
  @IsString()
  readonly id: string;

  constructor(id: string) {
    this.id = id;
  }
}
