import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { JwtDto } from '../../auth/dto';

export const Jwt = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();

    const jwt: JwtDto = request.user;

    return jwt;
  },
);
