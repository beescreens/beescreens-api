import { applyDecorators, UseGuards, CanActivate } from '@nestjs/common';
import { ApiBearerAuth, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { UnauthorizedResponseSpecs } from '../../auth/swagger/responses-specs';

export function HttpAuth(
  // eslint-disable-next-line @typescript-eslint/ban-types
  ...guards: (Function | CanActivate)[]
): // eslint-disable-next-line @typescript-eslint/ban-types
<TFunction extends Function, Y>(
  // eslint-disable-next-line @typescript-eslint/ban-types
  target: object | TFunction,
  propertyKey?: string | symbol | undefined,
  descriptor?: TypedPropertyDescriptor<Y> | undefined,
) => void {
  return applyDecorators(
    UseGuards(...guards),
    ApiBearerAuth(),
    ApiUnauthorizedResponse(UnauthorizedResponseSpecs),
  );
}
