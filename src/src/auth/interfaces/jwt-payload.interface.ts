import { UserRole } from '@beescreens/beescreens';

export interface JwtPayload {
  readonly userId: string;
  readonly userRole: UserRole;
}
