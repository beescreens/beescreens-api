import { JwtPayload } from './jwt-payload.interface';

export interface RawJwt {
  readonly jti: string;
  readonly iat: number;
  readonly exp: number;
  readonly payload: JwtPayload;
}
