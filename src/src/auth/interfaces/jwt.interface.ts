import { JwtPayload } from './jwt-payload.interface';

export interface Jwt {
  readonly jwtId: string;
  readonly jwtIssuedAt: Date;
  readonly jwtExpirationTime: Date;
  readonly jwtPayload: JwtPayload;
}
