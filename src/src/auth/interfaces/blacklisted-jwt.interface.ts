import { JwtPayload } from './jwt-payload.interface';

export interface BlacklistedJwt {
  readonly jwtId: string;
  readonly jwtIssuedAt: Date;
  readonly jwtExpirationTime: Date;
  readonly jwtPayload: JwtPayload;
}
