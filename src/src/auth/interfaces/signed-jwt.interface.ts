export interface SignedJwt {
  readonly jwt: string;
}
