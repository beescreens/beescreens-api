import { Injectable, Inject } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { verify } from 'argon2';
import { MoreThanOrEqual, Repository } from 'typeorm';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Logger } from 'winston';
import { v4 as uuidv4 } from 'uuid';
import { UserStatus } from '@beescreens/beescreens';
import { InjectRepository } from '@nestjs/typeorm';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { UserEntity } from '../users/entities';
import { UsersService } from '../users/users.service';
import { JwtPayloadDto, CredentialsDto } from './dto';
import { BlacklistedJwtEntity } from './entities';

@Injectable()
export class AuthService {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) readonly logger: Logger,
    @InjectRepository(BlacklistedJwtEntity)
    readonly blacklistedJwtRepository: Repository<BlacklistedJwtEntity>,
    readonly usersService: UsersService,
    readonly jwtService: JwtService,
  ) {}

  async validateCredentials(credentials: CredentialsDto): Promise<UserEntity> {
    this.logger.info(`credentials: ${credentials.username}`, {
      context: `${AuthService.name}::${this.validateCredentials.name}`,
    });

    const { username } = credentials;

    const user = await this.usersService.getOne({
      username,
    });

    if (
      !(await verify(user.password, credentials.password)) ||
      user.status === UserStatus.DISABLED
    ) {
      this.logger.warn(`user failed to login - username: ${username}`, {
        context: `${AuthService.name}::${this.validateCredentials.name}`,
      });

      throw new Error();
    }

    return user;
  }

  async validateJwtId(jwtId: string): Promise<void> {
    this.logger.info(`jwtId: ${jwtId}`, {
      context: `${AuthService.name}::${this.validateJwtId.name}`,
    });

    const blacklistedJwtFound = await this.blacklistedJwtRepository.findOne({
      jwtId,
    });

    if (blacklistedJwtFound) {
      this.logger.warn(`jwt is blacklisted - jwtId: ${jwtId}`, {
        context: `${AuthService.name}::${this.validateJwtId.name}`,
      });

      throw new Error();
    }
  }

  async validateUserId(userId: string): Promise<UserEntity> {
    return this.usersService.getOne({ id: userId });
  }

  @Cron(CronExpression.EVERY_30_MINUTES)
  async removeExpiredBlacklistedJwt(): Promise<number> {
    const removedBlacklistedJwt = await this.blacklistedJwtRepository.delete({
      jwtExpirationTime: MoreThanOrEqual(new Date()),
    });

    const numberOfBlacklistedJwt = removedBlacklistedJwt.affected || 0;

    this.logger.info(
      `removed ${numberOfBlacklistedJwt} expired blacklisted jwt`,
      {
        context: `${AuthService.name}::${this.removeExpiredBlacklistedJwt.name}`,
      },
    );

    return numberOfBlacklistedJwt;
  }

  async login(user: UserEntity): Promise<string> {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { password, ...userProperties } = user;

    this.logger.info(`params: ${JSON.stringify(userProperties)}`, {
      context: `${AuthService.name}::${this.login.name}`,
    });

    const jwtPayloadDto = new JwtPayloadDto(user.id, user.role);

    return this.jwtService.sign(
      { payload: jwtPayloadDto },
      { jwtid: uuidv4() },
    );
  }

  async logout(
    blacklistedDecodedJwt: BlacklistedJwtEntity,
  ): Promise<BlacklistedJwtEntity> {
    this.logger.info(`params: ${JSON.stringify(blacklistedDecodedJwt)}`, {
      context: `${AuthService.name}::${this.logout.name}`,
    });

    return this.blacklistedJwtRepository.save(blacklistedDecodedJwt);
  }
}
