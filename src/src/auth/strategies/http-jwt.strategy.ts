import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtDto, RawJwtDto } from '../dto';
import { AuthService } from '../auth.service';
import { HTTP_JWT_STRATEGY } from '../../constants';

@Injectable()
export class HttpJwtStrategy extends PassportStrategy(
  Strategy,
  HTTP_JWT_STRATEGY,
) {
  constructor(
    readonly authService: AuthService,
    readonly configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get<string>('JWT_SECRET'),
    });
  }

  async validate(rawJwt: RawJwtDto): Promise<JwtDto> {
    const jwt = JwtDto.fromRawJwt(rawJwt);

    try {
      await this.authService.validateJwtId(jwt.jwtId);
      await this.authService.validateUserId(jwt.jwtPayload.userId);
    } catch (error) {
      throw new UnauthorizedException();
    }

    return jwt;
  }
}
