import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserEntity } from '../../users/entities';
import { AuthService } from '../auth.service';
import { CredentialsDto } from '../dto';
import { HTTP_LOCAL_STRATEGY } from '../../constants';

@Injectable()
export class HttpLocalStrategy extends PassportStrategy(
  Strategy,
  HTTP_LOCAL_STRATEGY,
) {
  constructor(readonly authService: AuthService) {
    super();
  }

  async validate(username: string, password: string): Promise<UserEntity> {
    try {
      return await this.authService.validateCredentials(
        new CredentialsDto(username, password),
      );
    } catch (error) {
      throw new UnauthorizedException();
    }
  }
}
