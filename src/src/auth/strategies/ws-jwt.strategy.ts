import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { UnauthorizedWsException } from '../ws-exceptions';
import { JwtDto, RawJwtDto } from '../dto';
import { AuthService } from '../auth.service';
import { WS_JWT_STRATEGY } from '../../constants';

@Injectable()
export class WsJwtStrategy extends PassportStrategy(Strategy, WS_JWT_STRATEGY) {
  constructor(
    readonly authService: AuthService,
    readonly configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromUrlQueryParameter('jwt'),
      ignoreExpiration: false,
      secretOrKey: configService.get<string>('JWT_SECRET'),
    });
  }

  async validate(rawJwt: RawJwtDto): Promise<JwtDto> {
    const jwt = JwtDto.fromRawJwt(rawJwt);

    try {
      await this.authService.validateJwtId(jwt.jwtId);
      await this.authService.validateUserId(jwt.jwtPayload.userId);
    } catch (error) {
      throw new UnauthorizedWsException();
    }

    return jwt;
  }
}
