import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const LoginOperationSpecs: Partial<OperationObject> = {
  summary: 'Login to BeeScreens API',
  description: 'Login to BeeScreens API.',
  operationId: 'login',
};
