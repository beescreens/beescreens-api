import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const LogoutOperationSpecs: Partial<OperationObject> = {
  summary: 'Logout from BeeScreens API',
  description: 'Logout from BeeScreens API.',
  operationId: 'logout',
};
