import { ApiPropertyOptions } from '@nestjs/swagger';

export const SignedJwtPropertySpecs: ApiPropertyOptions = {
  name: 'jwt',
  description: 'JWT to access protected endpoint.',
  type: 'string',
};
