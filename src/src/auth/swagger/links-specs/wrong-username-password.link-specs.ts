import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const WrongUsernamePasswordLinkSpecs: LinkObject = {
  description:
    'Please authenticate with your `username`/`password` in `POST /login`',
  operationId: 'POST /login',
  requestBody: {
    username: '',
    password: '',
  },
};
