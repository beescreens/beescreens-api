import { ApiBodyOptions } from '@nestjs/swagger';
import { CredentialsDto } from '../../dto';

export const CredentialsBodySpecs: ApiBodyOptions = {
  description: "The user's credentials.",
  type: CredentialsDto,
};
