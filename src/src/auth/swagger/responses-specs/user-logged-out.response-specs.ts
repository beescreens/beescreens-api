import { ApiResponseOptions } from '@nestjs/swagger';

export const UserLoggedOutResponseSpecs: ApiResponseOptions = {
  description: 'The user has been successfully logged out.',
};
