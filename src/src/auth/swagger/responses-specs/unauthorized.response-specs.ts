import { ApiResponseOptions } from '@nestjs/swagger';
import { WrongUsernamePasswordLinkSpecs } from '../links-specs';

export const UnauthorizedResponseSpecs: ApiResponseOptions = {
  description: 'Wrong username/password or authentication JWT.',
  links: {
    WrongUsernamePassword: WrongUsernamePasswordLinkSpecs,
  },
};
