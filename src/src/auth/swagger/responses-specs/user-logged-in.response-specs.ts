import { ApiResponseOptions } from '@nestjs/swagger';
import { SignedJwtDto } from '../../dto';

export const UserLoggedInResponseSpecs: ApiResponseOptions = {
  description: 'The user has been successfully logged in.',
  type: SignedJwtDto,
};
