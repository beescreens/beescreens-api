import { Injectable, ExecutionContext } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { WS_JWT_STRATEGY } from '../../constants';
import { UnauthorizedWsException } from '../ws-exceptions';

@Injectable()
export class WsJwtGuard extends AuthGuard(WS_JWT_STRATEGY) {
  // eslint-disable-next-line class-methods-use-this, @typescript-eslint/no-explicit-any
  getRequest<T = any>(context: ExecutionContext): T {
    return context.switchToWs().getClient().handshake;
  }

  // eslint-disable-next-line class-methods-use-this, @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
  handleRequest(err: any, user: any): any {
    if (err || !user) {
      throw err || new UnauthorizedWsException();
    }

    return user;
  }
}
