import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { HTTP_LOCAL_STRATEGY } from '../../constants';

@Injectable()
export class HttpLocalGuard extends AuthGuard(HTTP_LOCAL_STRATEGY) {}
