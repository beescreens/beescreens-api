import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { HTTP_JWT_STRATEGY } from '../../constants';

@Injectable()
export class HttpJwtGuard extends AuthGuard(HTTP_JWT_STRATEGY) {}
