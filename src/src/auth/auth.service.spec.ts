import { Test, TestingModule } from '@nestjs/testing';
import { v4 as uuidv4 } from 'uuid';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { transports } from 'winston';
import { UserRole, UserStatus } from '@beescreens/beescreens';
import { forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { CreateUserDto } from '../users/dto';
import { UserEntity } from '../users/entities';
import { AuthService } from './auth.service';
import { UsersController } from '../users/users.controller';
import { AuthController } from './auth.controller';
import {
  HttpLocalStrategy,
  HttpJwtStrategy,
  WsJwtStrategy,
} from './strategies';
import { UsersModule } from '../users/users.module';
import { JwtDto, RawJwtDto } from './dto';
import { BlacklistedJwtEntity, JwtPayloadEntity } from './entities';

describe('AuthService', () => {
  let app: TestingModule;
  let authService: AuthService;
  let jwtService: JwtService;
  let usersController: UsersController;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(),
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        TypeOrmModule.forFeature([BlacklistedJwtEntity, JwtPayloadEntity]),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
        PassportModule,
        JwtModule.registerAsync({
          imports: [ConfigModule],
          inject: [ConfigService],
          useFactory: async (configService: ConfigService) => ({
            secret: configService.get<string>('JWT_SECRET'),
            signOptions: {
              expiresIn: configService.get<string>('JWT_EXPIRE_TIME'),
            },
          }),
        }),
        forwardRef(() => UsersModule),
      ],
      controllers: [AuthController],
      providers: [
        AuthService,
        HttpLocalStrategy,
        HttpJwtStrategy,
        WsJwtStrategy,
      ],
    }).compile();

    authService = app.get<AuthService>(AuthService);
    jwtService = app.get<JwtService>(JwtService);
    usersController = app.get<UsersController>(UsersController);
  });

  afterEach(async () => {
    await app.close();
  });

  it('should be able to login with an existing user', async () => {
    const userDto: CreateUserDto = new CreateUserDto(
      'user',
      'password',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser = await usersController.createUser(userDto);

    const user: UserEntity = new UserEntity(
      createdUser.id,
      userDto.username,
      userDto.password,
      userDto.role,
      userDto.status,
    );

    const jwt = await authService.login(user);

    expect(typeof jwt).toBe('string');

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const decodedJwt: any = jwtService.decode(jwt);

    expect(decodedJwt).toHaveProperty('jti');
    expect(decodedJwt).toHaveProperty('iat');
    expect(decodedJwt).toHaveProperty('exp');
    expect(decodedJwt).toHaveProperty('payload');

    const rawDecodedJwt = new RawJwtDto(
      decodedJwt.jti,
      decodedJwt.iat,
      decodedJwt.exp,
      decodedJwt.payload,
    );

    const formattedDecodedJwt = JwtDto.fromRawJwt(rawDecodedJwt);

    expect(formattedDecodedJwt).toHaveProperty('jwtId');
    expect(formattedDecodedJwt).toHaveProperty('jwtIssuedAt');
    expect(formattedDecodedJwt).toHaveProperty('jwtExpirationTime');
    expect(formattedDecodedJwt).toHaveProperty('jwtPayload');

    expect(formattedDecodedJwt.jwtPayload).toHaveProperty('userId');
    expect(formattedDecodedJwt.jwtPayload).toHaveProperty('userRole');

    const {
      jwtPayload: { userId, userRole },
    } = formattedDecodedJwt;

    expect(userId).toEqual(createdUser.id);
    expect(userRole).toEqual(createdUser.role);
  });

  it('should be able to logout', async () => {
    const userDto: CreateUserDto = new CreateUserDto(
      'user',
      'password',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser = await usersController.createUser(userDto);

    const user: UserEntity = new UserEntity(
      createdUser.id,
      userDto.username,
      userDto.password,
      userDto.role,
      userDto.status,
    );

    const jwt = await authService.login(user);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const decodedJwt: any = jwtService.decode(jwt);

    const rawDecodedJwt = new RawJwtDto(
      decodedJwt.jti,
      decodedJwt.iat,
      decodedJwt.exp,
      decodedJwt.payload,
    );

    const formattedDecodedJwt = JwtDto.fromRawJwt(rawDecodedJwt);

    const {
      jwtId,
      jwtIssuedAt,
      jwtExpirationTime,
      jwtPayload,
    } = formattedDecodedJwt;

    await authService.logout(
      new BlacklistedJwtEntity(
        jwtId,
        jwtIssuedAt,
        jwtExpirationTime,
        new JwtPayloadEntity(jwtPayload.userId, jwtPayload.userRole),
      ),
    );
  });

  it('should be able to remove expired blacklisted JWT', async () => {
    const userDto: CreateUserDto = new CreateUserDto(
      'user',
      'password',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser = await usersController.createUser(userDto);

    const jwt = await authService.login(
      new UserEntity(
        createdUser.id,
        userDto.username,
        userDto.password,
        userDto.role,
        userDto.status,
      ),
    );

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const decodedJwt: any = jwtService.decode(jwt);

    const formattedDecodedJwt = JwtDto.fromRawJwt(
      new RawJwtDto(
        decodedJwt.jti,
        decodedJwt.iat,
        decodedJwt.exp,
        decodedJwt.payload,
      ),
    );

    const blacklistedJwt = new BlacklistedJwtEntity(
      formattedDecodedJwt.jwtId,
      formattedDecodedJwt.jwtIssuedAt,
      formattedDecodedJwt.jwtExpirationTime,
      new JwtPayloadEntity(
        formattedDecodedJwt.jwtPayload.userId,
        formattedDecodedJwt.jwtPayload.userRole,
      ),
    );

    await authService.logout(blacklistedJwt);

    const numberOfBlacklistedJwt = await authService.removeExpiredBlacklistedJwt();

    expect(numberOfBlacklistedJwt).toEqual(0);
  });

  it('should throw an error when trying to logout twice', async () => {
    const userDto: CreateUserDto = new CreateUserDto(
      'user',
      'password',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser = await usersController.createUser(userDto);

    const jwt = await authService.login(
      new UserEntity(
        createdUser.id,
        userDto.username,
        userDto.password,
        userDto.role,
        userDto.status,
      ),
    );

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const decodedJwt: any = jwtService.decode(jwt);

    const formattedDecodedJwt = JwtDto.fromRawJwt(
      new RawJwtDto(
        decodedJwt.jti,
        decodedJwt.iat,
        decodedJwt.exp,
        decodedJwt.payload,
      ),
    );

    const blacklistedJwt = new BlacklistedJwtEntity(
      formattedDecodedJwt.jwtId,
      formattedDecodedJwt.jwtIssuedAt,
      formattedDecodedJwt.jwtExpirationTime,
      new JwtPayloadEntity(
        formattedDecodedJwt.jwtPayload.userId,
        formattedDecodedJwt.jwtPayload.userRole,
      ),
    );

    try {
      await authService.logout(blacklistedJwt);
      await authService.logout(blacklistedJwt);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should throw an error when trying to login with incorrect password', async () => {
    const userDto: CreateUserDto = new CreateUserDto(
      'user2',
      'password',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser = await usersController.createUser(userDto);

    const user: UserEntity = new UserEntity(
      createdUser.id,
      userDto.username,
      'wrongpassword',
      userDto.role,
      userDto.status,
    );

    try {
      await authService.login(user);
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should throw an error when trying to login with an inexisting user', async () => {
    const user: UserEntity = new UserEntity(
      uuidv4(),
      '404',
      'notfound',
      UserRole.DEVELOPER,
      UserStatus.ENABLED,
    );

    try {
      await authService.login(user);
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should throw an error when trying to login with a disabled user', async () => {
    const userDto: CreateUserDto = new CreateUserDto(
      'disableduser',
      'password',
      UserRole.MODERATOR,
      UserStatus.DISABLED,
    );

    const createdUser = await usersController.createUser(userDto);

    const user: UserEntity = new UserEntity(
      createdUser.id,
      userDto.username,
      userDto.password,
      userDto.role,
      userDto.status,
    );

    try {
      await authService.login(user);
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });
});
