import { Controller, Post, UseGuards, Inject, HttpCode } from '@nestjs/common';
import {
  ApiTags,
  ApiBody,
  ApiUnauthorizedResponse,
  ApiOkResponse,
  ApiOperation,
} from '@nestjs/swagger';
import { Logger } from 'winston';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { AuthService } from './auth.service';
import { HttpLocalGuard, HttpJwtGuard } from './guards';
import {
  UserLoggedInResponseSpecs,
  UnauthorizedResponseSpecs,
  UserLoggedOutResponseSpecs,
} from './swagger/responses-specs';
import {
  LoginOperationSpecs,
  LogoutOperationSpecs,
} from './swagger/operations-specs';
import { CredentialsBodySpecs } from './swagger/bodies-specs';
import { SignedJwtDto, JwtDto } from './dto';
import { BlacklistedJwtEntity, JwtPayloadEntity } from './entities';
import { User, Jwt, HttpAuth } from '../common/decorators';
import { UserEntity } from '../users/entities';

@ApiTags('Auth')
@Controller()
export class AuthController {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) readonly logger: Logger,
    readonly authService: AuthService,
  ) {}

  @Post('login')
  @UseGuards(HttpLocalGuard)
  @HttpCode(200)
  @ApiOkResponse(UserLoggedInResponseSpecs)
  @ApiUnauthorizedResponse(UnauthorizedResponseSpecs)
  @ApiOperation(LoginOperationSpecs)
  @ApiBody(CredentialsBodySpecs)
  async login(@User() user: UserEntity): Promise<SignedJwtDto> {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { password, ...userProperties } = user;

    this.logger.info(`user: ${JSON.stringify(userProperties)}`, {
      context: `${AuthController.name}::${this.login.name}`,
    });

    const jwt = await this.authService.login(user);

    return new SignedJwtDto(jwt);
  }

  @Post('logout')
  @HttpAuth(HttpJwtGuard)
  @HttpCode(200)
  @ApiOkResponse(UserLoggedOutResponseSpecs)
  @ApiOperation(LogoutOperationSpecs)
  async logout(@Jwt() jwt: JwtDto): Promise<void> {
    this.logger.info(`jwt: ${JSON.stringify(jwt)}`, {
      context: `${AuthController.name}::${this.logout.name}`,
    });

    await this.authService.logout(
      new BlacklistedJwtEntity(
        jwt.jwtId,
        jwt.jwtIssuedAt,
        jwt.jwtExpirationTime,
        new JwtPayloadEntity(jwt.jwtPayload.userId, jwt.jwtPayload.userRole),
      ),
    );
  }
}
