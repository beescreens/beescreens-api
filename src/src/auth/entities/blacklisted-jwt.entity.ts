import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinColumn,
  OneToOne,
} from 'typeorm';
import { BlacklistedJwt } from '../interfaces';
import { JwtPayloadEntity } from './jwt-payload.entity';

@Entity({
  name: 'blacklisted_jwts',
})
export class BlacklistedJwtEntity implements BlacklistedJwt {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Column()
  jwtId: string;

  @Column()
  jwtIssuedAt: Date;

  @Column()
  jwtExpirationTime: Date;

  @OneToOne(() => JwtPayloadEntity, {
    cascade: true,
  })
  @JoinColumn()
  jwtPayload: JwtPayloadEntity;

  constructor(
    jwtId: string,
    jwtIssuedAt: Date,
    jwtExpirationTime: Date,
    jwtPayload: JwtPayloadEntity,
  ) {
    this.jwtId = jwtId;
    this.jwtIssuedAt = jwtIssuedAt;
    this.jwtExpirationTime = jwtExpirationTime;
    this.jwtPayload = jwtPayload;
  }
}
