import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { UserRole } from '@beescreens/beescreens';
import { JwtPayload } from '../interfaces';

@Entity({
  name: 'jwt_payloads',
})
export class JwtPayloadEntity implements JwtPayload {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Column()
  userId: string;

  @Column()
  userRole: UserRole;

  constructor(userId: string, userRole: UserRole) {
    this.userId = userId;
    this.userRole = userRole;
  }
}
