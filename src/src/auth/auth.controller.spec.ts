import { Test, TestingModule } from '@nestjs/testing';
import { v4 as uuidv4 } from 'uuid';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { transports } from 'winston';
import { UnauthorizedException, forwardRef } from '@nestjs/common';
import { UserRole, UserStatus } from '@beescreens/beescreens';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { CreateUserDto } from '../users/dto';
import { UserEntity } from '../users/entities';
import { AuthService } from './auth.service';
import { UsersController } from '../users/users.controller';
import { AuthController } from './auth.controller';
import {
  HttpLocalStrategy,
  HttpJwtStrategy,
  WsJwtStrategy,
} from './strategies';
import { UsersModule } from '../users/users.module';
import { JwtDto, RawJwtDto } from './dto';
import { BlacklistedJwtEntity, JwtPayloadEntity } from './entities';

describe('AuthController', () => {
  let app: TestingModule;
  let authController: AuthController;
  let jwtService: JwtService;
  let usersController: UsersController;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(),
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        TypeOrmModule.forFeature([BlacklistedJwtEntity, JwtPayloadEntity]),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
        PassportModule,
        JwtModule.registerAsync({
          imports: [ConfigModule],
          inject: [ConfigService],
          useFactory: async (configService: ConfigService) => ({
            secret: configService.get<string>('JWT_SECRET'),
            signOptions: {
              expiresIn: configService.get<string>('JWT_EXPIRE_TIME'),
            },
          }),
        }),
        forwardRef(() => UsersModule),
      ],
      controllers: [AuthController],
      providers: [
        AuthService,
        HttpLocalStrategy,
        HttpJwtStrategy,
        WsJwtStrategy,
      ],
    }).compile();

    authController = app.get<AuthController>(AuthController);
    jwtService = app.get<JwtService>(JwtService);
    usersController = app.get<UsersController>(UsersController);
  });

  afterEach(async () => {
    await app.close();
  });

  it('should be able to login with an existing user', async () => {
    const userDto: CreateUserDto = new CreateUserDto(
      'user',
      'password',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser = await usersController.createUser(userDto);

    const user: UserEntity = new UserEntity(
      createdUser.id,
      userDto.username,
      userDto.password,
      userDto.role,
      userDto.status,
    );

    const signedJwtDto = await authController.login(user);

    expect(signedJwtDto).toHaveProperty('jwt');
  });

  it('should be able to logout', async () => {
    const userDto: CreateUserDto = new CreateUserDto(
      'user',
      'password',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser = await usersController.createUser(userDto);

    const user: UserEntity = new UserEntity(
      createdUser.id,
      userDto.username,
      userDto.password,
      userDto.role,
      userDto.status,
    );

    const signedJwtDto = await authController.login(user);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const decodedJwt: any = jwtService.decode(signedJwtDto.jwt);

    const rawDecodedJwt = new RawJwtDto(
      decodedJwt.jti,
      decodedJwt.iat,
      decodedJwt.exp,
      decodedJwt.payload,
    );

    const jwt = JwtDto.fromRawJwt(rawDecodedJwt);

    await authController.logout(jwt);
  });

  it('should throw an error when trying to logout again', async () => {
    const userDto: CreateUserDto = new CreateUserDto(
      'user',
      'password',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser = await usersController.createUser(userDto);

    const user: UserEntity = new UserEntity(
      createdUser.id,
      userDto.username,
      userDto.password,
      userDto.role,
      userDto.status,
    );

    const signedJwtDto = await authController.login(user);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const decodedJwt: any = jwtService.decode(signedJwtDto.jwt);

    const rawDecodedJwt = new RawJwtDto(
      decodedJwt.jti,
      decodedJwt.iat,
      decodedJwt.exp,
      decodedJwt.payload,
    );

    const jwt = JwtDto.fromRawJwt(rawDecodedJwt);

    try {
      await authController.logout(jwt);
      await authController.logout(jwt);
    } catch (error) {
      expect(error).toBeInstanceOf(UnauthorizedException);
    }
  });

  it('should throw an error when trying to login with incorrect password', async () => {
    const userDto: CreateUserDto = new CreateUserDto(
      'user2',
      'password',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser = await usersController.createUser(userDto);

    const user: UserEntity = new UserEntity(
      createdUser.id,
      userDto.username,
      'wrongpassword',
      userDto.role,
      userDto.status,
    );

    try {
      await authController.login(user);
    } catch (error) {
      expect(error).toBeInstanceOf(UnauthorizedException);
    }
  });

  it('should throw an error when trying to login with an inexisting user', async () => {
    const user: UserEntity = new UserEntity(
      uuidv4(),
      '404',
      'notfound',
      UserRole.DEVELOPER,
      UserStatus.ENABLED,
    );

    try {
      await authController.login(user);
    } catch (error) {
      expect(error).toBeInstanceOf(UnauthorizedException);
    }
  });

  it('should throw an error when trying to login with a disabled user', async () => {
    const userDto: CreateUserDto = new CreateUserDto(
      'disableduser',
      'password',
      UserRole.MODERATOR,
      UserStatus.DISABLED,
    );

    const createdUser = await usersController.createUser(userDto);

    const user: UserEntity = new UserEntity(
      createdUser.id,
      userDto.username,
      userDto.password,
      userDto.role,
      userDto.status,
    );

    try {
      await authController.login(user);
    } catch (error) {
      expect(error).toBeInstanceOf(UnauthorizedException);
    }
  });
});
