import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, HttpServer, ValidationPipe } from '@nestjs/common';
import request from 'supertest';
import { ScheduleModule } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { transports } from 'winston';
import { UserRole, UserStatus } from '@beescreens/beescreens';
import compression from 'compression';
import helmet from 'helmet';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { MessagesModule } from '../messages/messages.module';
import { UsersModule } from '../users/users.module';
import { AuthModule } from './auth.module';
import { UsersController } from '../users/users.controller';
import { CreateUserDto } from '../users/dto';
import { DisplayersModule } from '../displayers/displayers.module';
import { ApplicationsModule } from '../applications/applications.module';

describe('Auth (e2e)', () => {
  let app: INestApplication;
  let server: HttpServer;
  let configService: ConfigService;
  let usersController: UsersController;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ScheduleModule.forRoot(),
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
        AuthModule,
        MessagesModule,
        UsersModule,
        ApplicationsModule,
        DisplayersModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    app.use(compression());

    app.use(helmet());

    app.enableCors();

    app.useGlobalPipes(
      new ValidationPipe({
        transform: true,
      }),
    );

    app.enableShutdownHooks();

    server = app.getHttpServer();
    configService = app.get<ConfigService>(ConfigService);
    usersController = app.get<UsersController>(UsersController);

    await app.init();
  });

  afterEach(async () => {
    await app.close();
  });

  it('cannot access to /login (POST) without a body', async () => {
    return request(server).post('/login').expect(401);
  });

  it('can access to /login (POST) with correct credentials', async () => {
    const res = await request(server)
      .post('/login')
      .send({
        username: configService.get<string>('DEFAULT_USERNAME'),
        password: configService.get<string>('DEFAULT_PASSWORD'),
      });

    expect(res.status).toEqual(200);
    expect(res.body).toHaveProperty('jwt');
  });

  it('cannot access to /login (POST) with incorrect credentials', async () => {
    return request(server)
      .post('/login')
      .send({
        username: 'admin',
        password: 'wrong password',
      })
      .expect(401);
  });

  it('cannot access to /login (POST) with disabled user', async () => {
    await usersController.createUser(
      new CreateUserDto(
        'temp',
        'password',
        UserRole.MODERATOR,
        UserStatus.DISABLED,
      ),
    );

    return request(server)
      .post('/login')
      .send({
        username: 'temp',
        password: 'password',
      })
      .expect(401);
  });

  it('cannot access to /logout (POST) without Authorization header', async () => {
    return request(server).post('/logout').expect(401);
  });

  it('can access to /logout (POST) with correct credentials', async () => {
    const res = await request(server)
      .post('/login')
      .send({
        username: configService.get<string>('DEFAULT_USERNAME'),
        password: configService.get<string>('DEFAULT_PASSWORD'),
      });

    return request(server)
      .post('/logout')
      .set('Authorization', `Bearer ${res.body.jwt}`)
      .expect(200);
  });

  it('cannot access to /logout (POST) with the same JWT', async () => {
    const res = await request(server)
      .post('/login')
      .send({
        username: configService.get<string>('DEFAULT_USERNAME'),
        password: configService.get<string>('DEFAULT_PASSWORD'),
      });

    await request(server)
      .post('/logout')
      .set('Authorization', `Bearer ${res.body.jwt}`);
    return request(server)
      .post('/logout')
      .set('Authorization', `Bearer ${res.body.jwt}`)
      .expect(401);
  });

  it('cannot access to /logout (POST) with incorrect JWT', async () => {
    return request(server)
      .post('/logout')
      .set('Authorization', `Bearer wrongjwt`)
      .expect(401);
  });
});
