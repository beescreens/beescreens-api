import { Module, forwardRef } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService, ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '../users/users.module';
import { HttpLocalStrategy } from './strategies/http-local.strategy';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { HttpJwtStrategy, WsJwtStrategy } from './strategies';
import { BlacklistedJwtEntity, JwtPayloadEntity } from './entities';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forFeature([BlacklistedJwtEntity, JwtPayloadEntity]),
    PassportModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET'),
        signOptions: {
          expiresIn: configService.get<string>('JWT_EXPIRE_TIME'),
        },
      }),
    }),
    forwardRef(() => UsersModule),
  ],
  controllers: [AuthController],
  providers: [AuthService, HttpLocalStrategy, HttpJwtStrategy, WsJwtStrategy],
})
export class AuthModule {}
