import { Jwt } from '../interfaces';
import { JwtPayloadDto } from './jwt-payload.dto';
import { RawJwtDto } from './raw-jwt.dto';

export class JwtDto implements Jwt {
  jwtId: string;

  jwtIssuedAt: Date;

  jwtExpirationTime: Date;

  jwtPayload: JwtPayloadDto;

  constructor(
    jwtId: string,
    jwtIssuedAt: Date,
    jwtExpirationTime: Date,
    jwtPayload: JwtPayloadDto,
  ) {
    this.jwtId = jwtId;
    this.jwtIssuedAt = jwtIssuedAt;
    this.jwtExpirationTime = jwtExpirationTime;
    this.jwtPayload = jwtPayload;
  }

  static fromRawJwt(rawDecodedJwt: RawJwtDto): JwtDto {
    const jwtIssuedAt = new Date(0);
    const jwtExpirationTime = new Date(0);

    jwtIssuedAt.setUTCSeconds(rawDecodedJwt.iat);
    jwtExpirationTime.setUTCSeconds(rawDecodedJwt.exp);

    return new this(
      rawDecodedJwt.jti,
      jwtIssuedAt,
      jwtExpirationTime,
      rawDecodedJwt.payload,
    );
  }
}
