import { RawJwt } from '../interfaces';
import { JwtPayloadDto } from './jwt-payload.dto';

export class RawJwtDto implements RawJwt {
  jti: string;

  iat: number;

  exp: number;

  payload: JwtPayloadDto;

  constructor(jti: string, iat: number, exp: number, payload: JwtPayloadDto) {
    this.jti = jti;
    this.iat = iat;
    this.exp = exp;
    this.payload = payload;
  }
}
