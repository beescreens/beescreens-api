import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { SignedJwt } from '../interfaces';
import { SignedJwtPropertySpecs } from '../swagger/properties-specs';

export class SignedJwtDto implements SignedJwt {
  @ApiProperty(SignedJwtPropertySpecs)
  @IsString()
  jwt: string;

  constructor(jwt: string) {
    this.jwt = jwt;
  }
}
