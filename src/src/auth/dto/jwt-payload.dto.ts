import { UserRole } from '@beescreens/beescreens';
import { JwtPayload } from '../interfaces';

export class JwtPayloadDto implements JwtPayload {
  userId: string;

  userRole: UserRole;

  constructor(userId: string, userRole: UserRole) {
    this.userId = userId;
    this.userRole = userRole;
  }
}
