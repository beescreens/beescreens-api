import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import {
  UserUsernamePropertySpecs,
  UserPasswordPropertySpecs,
} from '../../users/swagger/properties-specs';
import { Credentials } from '../interfaces';

export class CredentialsDto implements Credentials {
  @ApiProperty(UserUsernamePropertySpecs)
  @IsString()
  username: string;

  @ApiProperty(UserPasswordPropertySpecs)
  @IsString()
  password: string;

  constructor(username: string, password: string) {
    this.username = username;
    this.password = password;
  }
}
