import { WsCommonEvents } from '@beescreens/beescreens';

// Source: https://stackblitz.com/edit/typescript-random-enum-value
// eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
export const selectRandomEnum = <T>(enumeration: any): T => {
  const values = Object.keys(enumeration);
  const enumKey = values[Math.floor(Math.random() * values.length)];
  return enumeration[enumKey];
};

export const waitForJoined = (socket: SocketIOClient.Socket): Promise<void> => {
  return new Promise((resolve) => {
    socket.on(WsCommonEvents.JOINED, async () => {
      resolve();
    });
  });
};
