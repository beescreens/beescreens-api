import { Injectable, ExecutionContext, CanActivate } from '@nestjs/common';
import { PlayerStatus } from '@beescreens/beescreens';
import { PlayersService } from '../players.service';
import { PlayerEntity } from '../entities';

@Injectable()
export class PlayersIdGuard implements CanActivate {
  constructor(readonly playersService: PlayersService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const client = context.switchToWs().getClient();

    let player: PlayerEntity;

    try {
      player = await this.playersService.getOne({
        id: client.handshake.query.id,
      });
    } catch (error) {
      return false;
    }

    if (
      player.status !== PlayerStatus.QUIT &&
      player.status !== PlayerStatus.KICKED
    ) {
      client.handshake.user = player;
      return true;
    }

    return false;
  }
}
