import { Test, TestingModule } from '@nestjs/testing';
import { v4 as uuidv4 } from 'uuid';
import { NotFoundException, BadRequestException } from '@nestjs/common';
import { transports } from 'winston';
import {
  PlayerStatus,
  ApplicationRole,
  ApplicationStatus,
} from '@beescreens/beescreens';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { CreateApplicationDto } from '../applications/dto';
import { PlayersController } from './players.controller';
import {
  ReadPlayerDto,
  PlayersPaginationDto,
  ReadPlayableApplicationDto,
} from './dto';
import { PlayersService } from './players.service';
import { PaginationMetaDto, PaginationLinksDto } from '../common/dto';
import { PlayersGateway } from './players.gateway';
import { PlayerEntity } from './entities';
import { ApplicationsController } from '../applications/applications.controller';
import { ApplicationsModule } from '../applications/applications.module';
import { selectRandomEnum } from '../helpers';

describe('PlayersController', () => {
  let app: TestingModule;
  let playersController: PlayersController;
  let applicationsController: ApplicationsController;
  let playersService: PlayersService;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
        TypeOrmModule.forFeature([PlayerEntity]),
        ApplicationsModule,
      ],
      controllers: [PlayersController],
      providers: [PlayersService, PlayersGateway],
    }).compile();

    playersController = app.get<PlayersController>(PlayersController);
    applicationsController = app.get<ApplicationsController>(
      ApplicationsController,
    );
    playersService = app.get<PlayersService>(PlayersService);
  });

  afterEach(async () => {
    await app.close();
  });

  it('should be able to get the new playable application and have all the properties', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const createdApplication = await applicationsController.createApplication(
      newApplication,
    );

    const foundPlayableApplication = await playersController.getPlayableApplication(
      createdApplication.id,
    );

    expect(foundPlayableApplication.id).toEqual(createdApplication.id);
    expect(foundPlayableApplication.name).toEqual(createdApplication.name);
    expect(foundPlayableApplication.description).toEqual(
      createdApplication.description,
    );
    expect(foundPlayableApplication.version).toEqual(
      createdApplication.version,
    );
    expect(foundPlayableApplication.contact).toEqual(
      createdApplication.contact,
    );
    expect(foundPlayableApplication.timeLimit).toEqual(
      createdApplication.timeLimit,
    );
    expect(foundPlayableApplication.logoUrl).toEqual(
      createdApplication.logoUrl,
    );
    expect(foundPlayableApplication.homepageUrl).toEqual(
      createdApplication.homepageUrl,
    );
    expect(foundPlayableApplication.documentationUrl).toEqual(
      createdApplication.documentationUrl,
    );
    expect(foundPlayableApplication).not.toHaveProperty('endpointUrl');
    expect(foundPlayableApplication.minimumNumberOfPlayers).toEqual(
      createdApplication.minimumNumberOfPlayers,
    );
    expect(foundPlayableApplication.maximumNumberOfPlayers).toEqual(
      createdApplication.maximumNumberOfPlayers,
    );
    expect(foundPlayableApplication.minimumNumberOfScreens).toEqual(
      createdApplication.minimumNumberOfScreens,
    );
    expect(foundPlayableApplication.maximumNumberOfScreens).toEqual(
      createdApplication.maximumNumberOfScreens,
    );
    expect(foundPlayableApplication).not.toHaveProperty('role');
    expect(foundPlayableApplication).not.toHaveProperty('status');
    expect(foundPlayableApplication).not.toHaveProperty('createdAt');
    expect(foundPlayableApplication).not.toHaveProperty('updatedAt');
  });

  it('should throw an error when trying to get an undefined playable application', async () => {
    try {
      await playersController.getPlayableApplication('404');
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should throw an error when trying to get a disable application', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.DISABLED,
    );

    const createdApplication = await applicationsController.createApplication(
      newApplication,
    );

    try {
      await playersController.getPlayableApplication(createdApplication.id);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should throw an error when trying to get a passive application', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.PASSIVE,
      ApplicationStatus.ENABLED,
    );

    const createdApplication = await applicationsController.createApplication(
      newApplication,
    );

    try {
      await playersController.getPlayableApplication(createdApplication.id);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should throw an error when trying to get a passive disabled application', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.PASSIVE,
      ApplicationStatus.DISABLED,
    );

    const createdApplication = await applicationsController.createApplication(
      newApplication,
    );

    try {
      await playersController.getPlayableApplication(createdApplication.id);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to get all playable applications', async () => {
    const createdPlayableApplications: ReadPlayableApplicationDto[] = [];

    // eslint-disable-next-line no-plusplus
    for (let applicationNb = 0; applicationNb < 35; applicationNb++) {
      // eslint-disable-next-line no-await-in-loop
      const createdApplication = await applicationsController.createApplication(
        new CreateApplicationDto(
          `name${applicationNb}`,
          `description${applicationNb}`,
          `version${applicationNb}`,
          `contact${applicationNb}@example.com`,
          120,
          `https://example${applicationNb}.com/logo.png`,
          `https://example${applicationNb}.com`,
          `https://example${applicationNb}.com/docs`,
          `https://play.example${applicationNb}.com`,
          applicationNb,
          applicationNb,
          applicationNb,
          applicationNb,
          selectRandomEnum<ApplicationRole>(ApplicationRole),
          selectRandomEnum<ApplicationStatus>(ApplicationStatus),
        ),
      );

      if (
        createdApplication.role === ApplicationRole.ACTIVE &&
        createdApplication.status === ApplicationStatus.ENABLED
      ) {
        createdPlayableApplications.push(
          new ReadPlayableApplicationDto(
            createdApplication.id,
            createdApplication.name,
            createdApplication.description,
            createdApplication.version,
            createdApplication.contact,
            createdApplication.timeLimit,
            createdApplication.logoUrl,
            createdApplication.homepageUrl,
            createdApplication.documentationUrl,
            createdApplication.minimumNumberOfPlayers,
            createdApplication.maximumNumberOfPlayers,
            createdApplication.minimumNumberOfScreens,
            createdApplication.maximumNumberOfScreens,
          ),
        );
      }
    }

    createdPlayableApplications.sort(
      (
        application1: ReadPlayableApplicationDto,
        application2: ReadPlayableApplicationDto,
      ) => application1.name.localeCompare(application2.name),
    );

    const limit = 10;
    let page = Math.floor(createdPlayableApplications.length / limit);

    if (page === 0) page = 1;

    const start = (page - 1) * limit;
    const end = start + limit;

    const pagination = await playersController.getPlayableApplications(
      page,
      limit,
    );

    expect(createdPlayableApplications.slice(start, end)).toEqual(
      pagination.items,
    );
  });

  it('should throw an error when trying to retrieve playable applications page under 1', async () => {
    try {
      await playersController.getPlayableApplications(0);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(BadRequestException);
    }
  });

  it('should throw an error when trying to retrieve playable applications limit under 1', async () => {
    try {
      await playersController.getPlayableApplications(1, 0);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(BadRequestException);
    }
  });

  it('should be able to get the new player and have all the properties', async () => {
    const newPlayer = new PlayerEntity(uuidv4());

    const createdPlayer = await playersService.createOne(newPlayer);

    const foundPlayer = await playersController.getPlayer(createdPlayer.id);

    expect(foundPlayer.id).toEqual(createdPlayer.id);
    expect(foundPlayer.status).toEqual(createdPlayer.status);
    expect(foundPlayer.createdAt).toEqual(createdPlayer.createdAt);
    expect(foundPlayer.updatedAt).toEqual(createdPlayer.updatedAt);
  });

  it('should throw an error when trying to get an undefined player', async () => {
    try {
      await playersController.getPlayer('404');
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to kick the player', async () => {
    const newPlayer = new PlayerEntity(uuidv4());

    const createdPlayer = await playersService.createOne(newPlayer);

    await playersController.kickPlayer(createdPlayer.id);

    const foundPlayer = await playersController.getPlayer(createdPlayer.id);

    expect(foundPlayer.id).toEqual(createdPlayer.id);
    expect(foundPlayer.status).toEqual(PlayerStatus.KICKED);
    expect(foundPlayer.createdAt).toEqual(createdPlayer.createdAt);
    expect(foundPlayer.updatedAt).toBeDefined();
  });

  it('should throw an error when trying to kick an undefined player', async () => {
    try {
      await playersController.kickPlayer('404');
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to get all players', async () => {
    const page = 2;
    const limit = 10;

    const start = (page - 1) * limit;
    const end = start + limit;

    const createdPlayers: ReadPlayerDto[] = [];

    // eslint-disable-next-line no-plusplus
    for (let playerNb = 0; playerNb < 35; playerNb++) {
      // eslint-disable-next-line no-await-in-loop
      const createdPlayer = await playersService.createOne(
        new PlayerEntity(uuidv4()),
      );

      createdPlayers.push(
        new ReadPlayerDto(
          createdPlayer.id,
          createdPlayer.status,
          createdPlayer.createdAt,
          createdPlayer.updatedAt,
        ),
      );
    }

    createdPlayers.sort((player1: ReadPlayerDto, player2: ReadPlayerDto) =>
      player1.id.localeCompare(player2.id),
    );

    const pagination = await playersController.getPlayers(page, limit);

    const results = new PlayersPaginationDto(
      createdPlayers.slice(start, end),
      new PaginationMetaDto(2, 10, 10, 35, 4),
      new PaginationLinksDto(
        '/players?limit=10',
        '/players?page=4&limit=10',
        '/players?page=3&limit=10',
        '/players?page=1&limit=10',
      ),
    );

    expect(pagination).toEqual(results);
  });

  it('should throw an error when trying to retrieve players page under 1', async () => {
    try {
      await playersController.getPlayers(0);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(BadRequestException);
    }
  });

  it('should throw an error when trying to retrieve players limit under 1', async () => {
    try {
      await playersController.getPlayers(1, 0);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(BadRequestException);
    }
  });
});
