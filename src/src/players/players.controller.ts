import {
  Controller,
  Get,
  Param,
  Inject,
  NotFoundException,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  Query,
  BadRequestException,
  ParseUUIDPipe,
  Patch,
  HttpCode,
  Post,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOperation,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiParam,
  ApiBadRequestResponse,
  ApiQuery,
} from '@nestjs/swagger';
import { Logger } from 'winston';
import { Pagination } from 'nestjs-typeorm-paginate';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { v4 as uuidv4 } from 'uuid';
import {
  PlayerStatus,
  ApplicationRole,
  ApplicationStatus,
  UserRole,
  WsPlayerEvents,
} from '@beescreens/beescreens';
import { ApplicationEntity } from '../applications/entities';
import { ApplicationIdApiParameterSpecs } from '../applications/swagger/parameters-specs';
import { HttpJwtGuard } from '../auth/guards';
import { MissingOrIncorrectParamsResponseSpecs } from '../common/swagger/responses-specs';
import { PlayersService } from './players.service';
import {
  PlayersRetrievedResponseSpecs,
  PlayerRetrievedResponseSpecs,
  PlayerNotFoundResponseSpecs,
  PlayerKickedResponseSpecs,
  PlayableApplicationsRetrievedResponseSpecs,
  PlayableApplicationRetrievedResponseSpecs,
  PlayableApplicationNotFoundResponseSpecs,
  PlayerIdResponseSpecs,
} from './swagger/responses-specs';
import {
  GetPlayersOperationSpecs,
  GetPlayerOperationSpecs,
  KickPlayerOperationSpecs,
  GetPlayableApplicationsOperationSpecs,
  GetPlaybleApplicationOperationSpecs,
  PlayApplicationOperationSpecs,
} from './swagger/operations-specs';
import {
  ReadPlayerDto,
  PlayersPaginationDto,
  PlayableApplicationsPaginationDto,
  ReadPlayableApplicationDto,
} from './dto';
import { PlayerIdApiParameterSpecs } from './swagger/parameters-specs';
import {
  PaginationPageQuerySpecs,
  PaginationLimitQuerySpecs,
} from '../common/swagger/queries-specs';
import { PlayerEntity } from './entities';
import {
  PaginationMetaDto,
  PaginationLinksDto,
  EntityIdDto,
} from '../common/dto';
import { PlayersGateway } from './players.gateway';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DefaultIntValuePipe } from '../common/pipes';
import { HttpAuth } from '../common/decorators';
import { ApplicationsService } from '../applications/applications.service';

@ApiTags('Players')
@Controller('players')
export class PlayersController {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) readonly logger: Logger,
    readonly playersService: PlayersService,
    readonly playersGateway: PlayersGateway,
    readonly applicationsService: ApplicationsService,
  ) {}

  @Get('/play')
  @ApiQuery(PaginationPageQuerySpecs)
  @ApiQuery(PaginationLimitQuerySpecs)
  @ApiOkResponse(PlayableApplicationsRetrievedResponseSpecs)
  @ApiOperation(GetPlayableApplicationsOperationSpecs)
  async getPlayableApplications(
    @Query('page', new DefaultIntValuePipe(1)) page = 10,
    @Query('limit', new DefaultIntValuePipe(10)) limit = 10,
  ): Promise<PlayableApplicationsPaginationDto> {
    this.logger.info(`page: ${page}, limit: ${limit}`, {
      context: `${PlayersController.name}::${this.getPlayableApplications.name}`,
    });

    const query = this.applicationsService.repository
      .createQueryBuilder('a')
      .where('a.role = :role')
      .andWhere('a.status = :status')
      .orderBy('a.name', 'ASC')
      .setParameter('role', ApplicationRole.ACTIVE)
      .setParameter('status', ApplicationStatus.ENABLED);

    let pagination: Pagination<ApplicationEntity>;

    try {
      pagination = await this.applicationsService.getPagination(
        '/players/play',
        page,
        limit,
        query,
      );
    } catch (error) {
      throw new BadRequestException();
    }

    const applications = pagination.items.map(
      async (application) =>
        new ReadPlayableApplicationDto(
          application.id,
          application.name,
          application.description,
          application.version,
          application.contact,
          application.timeLimit,
          application.logoUrl,
          application.homepageUrl,
          application.documentationUrl,
          application.minimumNumberOfPlayers,
          application.maximumNumberOfPlayers,
          application.minimumNumberOfScreens,
          application.maximumNumberOfScreens,
        ),
    );

    const { meta, links } = pagination;

    return new PlayableApplicationsPaginationDto(
      await Promise.all(applications),
      new PaginationMetaDto(
        meta.currentPage,
        meta.itemCount,
        meta.itemsPerPage,
        meta.totalItems,
        meta.totalPages,
      ),
      new PaginationLinksDto(
        links.first || '',
        links.last || '',
        links.next || '',
        links.previous || '',
      ),
    );
  }

  @Get('/play/:applicationId')
  @ApiParam(ApplicationIdApiParameterSpecs)
  @ApiOkResponse(PlayableApplicationRetrievedResponseSpecs)
  @ApiNotFoundResponse(PlayableApplicationNotFoundResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectParamsResponseSpecs)
  @ApiOperation(GetPlaybleApplicationOperationSpecs)
  async getPlayableApplication(
    @Param('applicationId', ParseUUIDPipe) applicationId: string,
  ): Promise<ReadPlayableApplicationDto> {
    this.logger.info(`applicationId: ${applicationId}`, {
      context: `${PlayersController.name}::${this.getPlayableApplication.name}`,
    });

    let foundApplication: ApplicationEntity;

    try {
      foundApplication = await this.applicationsService.getOne({
        id: applicationId,
        role: ApplicationRole.ACTIVE,
        status: ApplicationStatus.ENABLED,
      });
    } catch (error) {
      throw new NotFoundException();
    }

    const {
      id,
      name,
      description,
      version,
      contact,
      timeLimit,
      logoUrl,
      homepageUrl,
      documentationUrl,
      minimumNumberOfPlayers,
      maximumNumberOfPlayers,
      minimumNumberOfScreens,
      maximumNumberOfScreens,
    } = foundApplication;

    return new ReadPlayableApplicationDto(
      id,
      name,
      description,
      version,
      contact,
      timeLimit,
      logoUrl,
      homepageUrl,
      documentationUrl,
      minimumNumberOfPlayers,
      maximumNumberOfPlayers,
      minimumNumberOfScreens,
      maximumNumberOfScreens,
    );
  }

  @Post('/play/:applicationId')
  @HttpCode(200)
  @ApiOkResponse(PlayerIdResponseSpecs)
  @ApiNotFoundResponse(PlayableApplicationNotFoundResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectParamsResponseSpecs)
  @ApiOperation(PlayApplicationOperationSpecs)
  async playApplication(
    @Param('applicationId', ParseUUIDPipe) applicationId: string,
  ): Promise<EntityIdDto> {
    this.logger.info(`applicationId: ${applicationId}}`, {
      context: `${PlayersController.name}::${this.playApplication.name}`,
    });

    try {
      await this.applicationsService.getOne({
        id: applicationId,
        role: ApplicationRole.ACTIVE,
        status: ApplicationStatus.ENABLED,
      });
    } catch (error) {
      throw new NotFoundException();
    }

    const player = new PlayerEntity(uuidv4());

    const newPlayer = await this.playersService.createOne(player);

    await this.playersGateway.emitToAll(
      [UserRole.ADMIN, UserRole.MODERATOR, UserRole.READ_ONLY],
      WsPlayerEvents.PLAYER_CREATED,
      new EntityIdDto(player.id),
    );

    const playerIdDto = new EntityIdDto(newPlayer.id);

    return playerIdDto;
  }

  @Get()
  @HttpAuth(HttpJwtGuard)
  @ApiQuery(PaginationPageQuerySpecs)
  @ApiQuery(PaginationLimitQuerySpecs)
  @ApiOkResponse(PlayersRetrievedResponseSpecs)
  @ApiOperation(GetPlayersOperationSpecs)
  async getPlayers(
    @Query('page', new DefaultIntValuePipe(1)) page = 10,
    @Query('limit', new DefaultIntValuePipe(10)) limit = 10,
  ): Promise<PlayersPaginationDto> {
    this.logger.info(`page: ${page}, limit: ${limit}`, {
      context: `${PlayersController.name}::${this.getPlayers.name}`,
    });

    const query = this.playersService.repository
      .createQueryBuilder('p')
      .orderBy('p.id', 'ASC');

    let pagination: Pagination<PlayerEntity>;

    try {
      pagination = await this.playersService.getPagination(
        '/players',
        page,
        limit,
        query,
      );
    } catch (error) {
      throw new BadRequestException();
    }

    const players = pagination.items.map(
      async (player) =>
        new ReadPlayerDto(
          player.id,
          player.status,
          player.createdAt,
          player.updatedAt,
        ),
    );

    const { meta, links } = pagination;

    return new PlayersPaginationDto(
      await Promise.all(players),
      new PaginationMetaDto(
        meta.currentPage,
        meta.itemCount,
        meta.itemsPerPage,
        meta.totalItems,
        meta.totalPages,
      ),
      new PaginationLinksDto(
        links.first || '',
        links.last || '',
        links.next || '',
        links.previous || '',
      ),
    );
  }

  @Get(':playerId')
  @HttpAuth(HttpJwtGuard)
  @ApiParam(PlayerIdApiParameterSpecs)
  @ApiOkResponse(PlayerRetrievedResponseSpecs)
  @ApiNotFoundResponse(PlayerNotFoundResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectParamsResponseSpecs)
  @ApiOperation(GetPlayerOperationSpecs)
  async getPlayer(
    @Param('playerId', ParseUUIDPipe) playerId: string,
  ): Promise<ReadPlayerDto> {
    this.logger.info(`playerId: ${playerId}`, {
      context: `${PlayersController.name}::${this.getPlayer.name}`,
    });

    let foundPlayer: PlayerEntity;

    try {
      foundPlayer = await this.playersService.getOne({
        id: playerId,
      });
    } catch (error) {
      throw new NotFoundException();
    }

    const { id, status, createdAt, updatedAt } = foundPlayer;

    return new ReadPlayerDto(id, status, createdAt, updatedAt);
  }

  @Patch(':playerId/kick')
  @HttpCode(204)
  @HttpAuth(HttpJwtGuard)
  @ApiOkResponse(PlayerKickedResponseSpecs)
  @ApiNotFoundResponse(PlayerNotFoundResponseSpecs)
  @ApiParam(PlayerIdApiParameterSpecs)
  @ApiOperation(KickPlayerOperationSpecs)
  async kickPlayer(
    @Param('playerId', ParseUUIDPipe) playerId: string,
  ): Promise<void> {
    this.logger.info(`playerId: ${playerId}`, {
      context: `${PlayersController.name}::${this.kickPlayer.name}`,
    });

    let playerToKick: PlayerEntity;

    try {
      playerToKick = await this.playersService.getOne({
        id: playerId,
      });
    } catch (error) {
      throw new NotFoundException();
    }

    playerToKick.status = PlayerStatus.KICKED;

    await this.playersService.updateOne(playerId, playerToKick);
  }
}
