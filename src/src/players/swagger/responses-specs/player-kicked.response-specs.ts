import { ApiResponseOptions } from '@nestjs/swagger';

export const PlayerKickedResponseSpecs: ApiResponseOptions = {
  description: 'Player has been successfully kicked.',
};
