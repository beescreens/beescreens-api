import { ApiResponseOptions } from '@nestjs/swagger';

export const PlayableApplicationNotFoundResponseSpecs: ApiResponseOptions = {
  description: 'Playable application has not been found.',
};
