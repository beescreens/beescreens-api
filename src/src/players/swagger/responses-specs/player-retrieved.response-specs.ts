import { ApiResponseOptions } from '@nestjs/swagger';
import { ReadPlayerDto } from '../../dto';

export const PlayerRetrievedResponseSpecs: ApiResponseOptions = {
  description: 'Player has been successfully retrieved.',
  type: ReadPlayerDto,
};
