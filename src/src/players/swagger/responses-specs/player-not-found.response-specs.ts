import { ApiResponseOptions } from '@nestjs/swagger';

export const PlayerNotFoundResponseSpecs: ApiResponseOptions = {
  description: 'Player has not been found.',
};
