import { ApiResponseOptions } from '@nestjs/swagger';
import { EntityIdDto } from '../../../common/dto';
import { JoinAsPlayerLinkSpecs } from '../links-specs';

export const PlayerIdResponseSpecs: ApiResponseOptions = {
  description: 'The Player has been successfully created.',
  type: EntityIdDto,
  links: {
    JoinAsPlayer: JoinAsPlayerLinkSpecs,
  },
};
