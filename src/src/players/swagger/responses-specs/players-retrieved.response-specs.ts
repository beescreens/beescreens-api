import { ApiResponseOptions } from '@nestjs/swagger';
import { PlayersPaginationDto } from '../../dto';

export const PlayersRetrievedResponseSpecs: ApiResponseOptions = {
  description: 'Players have been successfully retrieved.',
  type: PlayersPaginationDto,
};
