import { ApiResponseOptions } from '@nestjs/swagger';
import { ReadPlayableApplicationDto } from '../../dto';

export const PlayableApplicationRetrievedResponseSpecs: ApiResponseOptions = {
  description: 'Playable application has been successfully retrieved.',
  type: ReadPlayableApplicationDto,
};
