import { ApiResponseOptions } from '@nestjs/swagger';
import { PlayableApplicationsPaginationDto } from '../../dto';

export const PlayableApplicationsRetrievedResponseSpecs: ApiResponseOptions = {
  description: 'Playable applications have been successfully retrieved.',
  type: PlayableApplicationsPaginationDto,
};
