import { ApiPropertyOptions } from '@nestjs/swagger';
import { PlayerStatus } from '@beescreens/beescreens';

export const PlayerStatusPropertySpecs: ApiPropertyOptions = {
  description: 'Status of the player',
  type: 'string',
  enum: PlayerStatus,
};
