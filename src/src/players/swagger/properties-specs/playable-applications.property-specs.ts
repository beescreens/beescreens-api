import { ApiPropertyOptions } from '@nestjs/swagger';
import { ReadPlayableApplicationDto } from '../../dto';

export const PlayableApplicationsPropertySpecs: ApiPropertyOptions = {
  description: 'The retrieved playable applications.',
  type: () => [ReadPlayableApplicationDto],
};
