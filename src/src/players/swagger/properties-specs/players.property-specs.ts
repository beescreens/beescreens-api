import { ApiPropertyOptions } from '@nestjs/swagger';
import { ReadPlayerDto } from '../../dto';

export const PlayersPropertySpecs: ApiPropertyOptions = {
  description: 'The retrieved players.',
  type: () => [ReadPlayerDto],
};
