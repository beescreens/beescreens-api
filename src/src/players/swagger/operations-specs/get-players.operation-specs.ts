import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { PlayersCallbacksApiCommon } from '../common';

export const GetPlayersOperationSpecs: Partial<OperationObject> = {
  summary: 'Get the players',
  description: 'Get the players.',
  operationId: 'getPlayers',
  callbacks: PlayersCallbacksApiCommon,
};
