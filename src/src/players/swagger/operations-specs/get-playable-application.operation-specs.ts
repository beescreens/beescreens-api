import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { PlayersCallbacksApiCommon } from '../common';

export const GetPlaybleApplicationOperationSpecs: Partial<OperationObject> = {
  summary: 'Get the specified playable application',
  description: 'Get the specified playable application.',
  operationId: 'getPlayableApplication',
  callbacks: PlayersCallbacksApiCommon,
};
