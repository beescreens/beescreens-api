import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { PlayersCallbacksApiCommon } from '../common';

export const GetPlayerOperationSpecs: Partial<OperationObject> = {
  summary: 'Get the specified player',
  description: 'Get the specified player.',
  operationId: 'getPlayer',
  callbacks: PlayersCallbacksApiCommon,
};
