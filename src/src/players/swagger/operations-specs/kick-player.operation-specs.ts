import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { PlayersCallbacksApiCommon } from '../common';

export const KickPlayerOperationSpecs: Partial<OperationObject> = {
  summary: 'Kick the specified player',
  description: 'Kick the specified player.',
  operationId: 'kickPlayer',
  callbacks: PlayersCallbacksApiCommon,
};
