import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { PlayersCallbacksApiCommon } from '../common';

export const PlayApplicationOperationSpecs: Partial<OperationObject> = {
  summary: 'Play the specified application',
  description: 'Play the specified application.',
  operationId: 'playApplication',
  callbacks: PlayersCallbacksApiCommon,
};
