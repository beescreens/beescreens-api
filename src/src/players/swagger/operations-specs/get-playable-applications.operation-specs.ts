import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { PlayersCallbacksApiCommon } from '../common';

export const GetPlayableApplicationsOperationSpecs: Partial<OperationObject> = {
  summary: 'Get the playable applications',
  description: 'Get the playable applications.',
  operationId: 'getPlayableApplications',
  callbacks: PlayersCallbacksApiCommon,
};
