import { ApiParamOptions } from '@nestjs/swagger';

export const PlayerIdApiParameterSpecs: ApiParamOptions = {
  name: 'playerId',
  description: 'The player ID.',
};
