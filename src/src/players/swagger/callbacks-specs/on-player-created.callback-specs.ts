import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { PlayersLinksApiCommon } from '../common';

export const OnPlayerCreatedCallbackSpecs: PathItemObject = {
  get: {
    summary: 'A new player has been emitted by the WebSocket server',
    description: 'A new player has been emitted by the WebSocket server.',
    operationId: 'onPlayerCreated',
    responses: {
      'N/A': {
        description: 'A new player has been emitted by the WebSocket server.',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: {
                  description: 'ID of the entity',
                  type: 'string',
                  format: 'uuid',
                },
              },
            },
          },
        },
        links: PlayersLinksApiCommon,
      },
    },
  },
};
