import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const EmitUpdatedPlayerResolutionCallbackSpecs: PathItemObject = {
  post: {
    summary:
      "The player's resolution has changed and can notify the WebSocket server",
    description:
      "The player's resolution has changed and can notify the WebSocket server.",
    operationId: 'emitUpdatedPlayerResolution',
    responses: {
      'N/A': {
        description: "The player's resolution has been updated.",
      },
    },
  },
};
