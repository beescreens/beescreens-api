import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const OnPlayerUpdatedCallbackSpecs: PathItemObject = {
  get: {
    summary: 'An updated player has been emitted by the WebSocket server',
    description: 'An updated player has been emitted by the WebSocket server.',
    operationId: 'onPlayerUpdated',
    responses: {
      'N/A': {
        description:
          'An updated player has been emitted by the WebSocket server.',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: {
                  description: 'ID of the entity',
                  type: 'string',
                  format: 'uuid',
                },
              },
            },
          },
        },
      },
    },
  },
};
