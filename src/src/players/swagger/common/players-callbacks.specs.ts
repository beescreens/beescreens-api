import {
  PathItemObject,
  ReferenceObject,
} from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import {
  OnJoinedCallbackSpecs,
  OnExceptionCallbackSpecs,
  OnDisconnectCallbackSpecs,
} from '../../../common/swagger/callbacks-specs';
import { OnMessageCallbackSpecs } from '../../../messages/swagger/callbacks-specs';
import {
  OnPlayerCreatedCallbackSpecs,
  OnPlayerUpdatedCallbackSpecs,
  EmitJoinAsPlayerCallbackSpecs,
} from '../callbacks-specs';
import { EmitJoinAsUserCallbackSpecs } from '../../../users/swagger/callbacks-specs';

export const PlayersCallbacksApiCommon: Record<
  string,
  ReferenceObject | Record<string, PathItemObject>
> = {
  'WebSockets (common)': {
    // onConnect: OnConnectPlayer,
    onJoined: OnJoinedCallbackSpecs,
    onException: OnExceptionCallbackSpecs,
    onDisconnect: OnDisconnectCallbackSpecs,
    onMessage: OnMessageCallbackSpecs,
  },
  'WebSockets (as user)': {
    emitJoinAsUser: EmitJoinAsUserCallbackSpecs,
    onPlayerCreated: OnPlayerCreatedCallbackSpecs,
    onPlayerUpdated: OnPlayerUpdatedCallbackSpecs,
  },
  'WebSockets (as player)': {
    emitJoinAsPlayer: EmitJoinAsPlayerCallbackSpecs,
  },
};
