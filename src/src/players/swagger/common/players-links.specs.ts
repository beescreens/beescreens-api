import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import {
  GetPlayerByIdLinkSpecs,
  KickPlayerByIdLinkSpecs,
  GetPlayableApplicationByIdLinkSpecs,
} from '../links-specs';

export const PlayersLinksApiCommon: Record<string, LinkObject> = {
  GetPlayerById: GetPlayerByIdLinkSpecs,
  KickPlayerById: KickPlayerByIdLinkSpecs,
  GetPlayableApplicationById: GetPlayableApplicationByIdLinkSpecs,
};
