import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const JoinAsPlayerLinkSpecs: LinkObject = {
  description:
    'The `id` value returned in the response can be used to join the WebSockets server in `emitJoinAsPlayer`.',
  operationId: 'emitJoinAsPlayer',
  parameters: { id: '$response.body#/id' },
};
