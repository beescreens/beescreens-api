import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const KickPlayerByIdLinkSpecs: LinkObject = {
  description:
    'The `id` value returned in the response can be used as the `playerId` parameter in `PATCH /players/{playerId}/kick`.',
  operationId: 'PATCH /players/{playerId}/kick',
  parameters: { playerId: '$response.body#/id' },
};
