import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const GetPlayerByIdLinkSpecs: LinkObject = {
  description:
    'The `id` value returned in the response can be used as the `playerId` parameter in `GET /players/{playerId}`.',
  operationId: 'GET /players/{playerId}',
  parameters: { playerId: '$response.body#/id' },
};
