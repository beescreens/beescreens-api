import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const GetPlayableApplicationByIdLinkSpecs: LinkObject = {
  description:
    'The `id` value returned in the response can be used as the `applicationId` parameter in `GET /play/{applicationId}`.',
  operationId: 'GET /play/{applicationId}',
  parameters: { applicationId: '$response.body#/id' },
};
