import { ReadPlayableApplication } from './read-playable-application.interface';
import { PaginationMeta, PaginationLinks } from '../../common/interfaces';

export interface PlayableApplicationsPagination {
  readonly items: ReadPlayableApplication[];
  readonly meta: PaginationMeta;
  readonly links: PaginationLinks;
}
