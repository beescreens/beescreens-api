import { ReadPlayer } from './read-player.interface';
import { PaginationMeta, PaginationLinks } from '../../common/interfaces';

export interface PlayersPagination {
  readonly items: ReadPlayer[];
  readonly meta: PaginationMeta;
  readonly links: PaginationLinks;
}
