import { Application } from '@beescreens/beescreens';

export type ReadPlayableApplication = Omit<
  Application,
  | 'apiKey'
  | 'endpointUrl'
  | 'tier'
  | 'role'
  | 'status'
  | 'createdAt'
  | 'updatedAt'
>;
