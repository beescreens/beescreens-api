import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlayersController } from './players.controller';
import { PlayersService } from './players.service';
import { PlayersGateway } from './players.gateway';
import { PlayerEntity } from './entities';
import { ApplicationsModule } from '../applications/applications.module';

@Module({
  imports: [TypeOrmModule.forFeature([PlayerEntity]), ApplicationsModule],
  controllers: [PlayersController],
  providers: [PlayersService, PlayersGateway],
  exports: [PlayersService, PlayersGateway],
})
export class PlayersModule {}
