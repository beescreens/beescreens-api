import { Test, TestingModule } from '@nestjs/testing';
import { v4 as uuidv4 } from 'uuid';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import request from 'supertest';
import { ScheduleModule } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { transports } from 'winston';
import {
  WsCommonEvents,
  WsUserEvents,
  WsPlayerEvents,
  ApplicationRole,
  ApplicationStatus,
  PlayerStatus,
} from '@beescreens/beescreens';
import { connect } from 'socket.io-client';
import compression from 'compression';
import helmet from 'helmet';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { CreateApplicationDto } from '../applications/dto';
import { PaginationMetaDto, PaginationLinksDto } from '../common/dto';
import { MessagesModule } from '../messages/messages.module';
import { PlayersModule } from './players.module';
import { AuthModule } from '../auth/auth.module';
import {
  ReadPlayerDto,
  PlayersPaginationDto,
  ReadPlayableApplicationDto,
} from './dto';
import { UsersModule } from '../users/users.module';
import { ApplicationsModule } from '../applications/applications.module';
import { PlayersService } from './players.service';
import { PlayerEntity } from './entities';
import { selectRandomEnum } from '../helpers';

describe('Players (e2e)', () => {
  let app: INestApplication;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let server: any;
  let serverAddress: string;
  let configService: ConfigService;
  let playersService: PlayersService;
  let jwt: string;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ScheduleModule.forRoot(),
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
        AuthModule,
        MessagesModule,
        UsersModule,
        ApplicationsModule,
        PlayersModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    app.use(compression());

    app.use(helmet());

    app.enableCors();

    app.useGlobalPipes(
      new ValidationPipe({
        transform: true,
      }),
    );

    app.enableShutdownHooks();

    server = app.getHttpServer();
    configService = app.get<ConfigService>(ConfigService);
    playersService = app.get<PlayersService>(PlayersService);

    const { address, port } = server.listen().address();

    serverAddress = `http://[${address}]:${port}`;

    await app.init();

    const res = await request(server)
      .post('/login')
      .send({
        username: configService.get<string>('DEFAULT_USERNAME'),
        password: configService.get<string>('DEFAULT_PASSWORD'),
      });

    jwt = res.body.jwt;
  });

  afterEach(async () => {
    await app.close();
  });

  it('should be able to get the new playable application and have all the properties', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const resCreatedApplication = await request(server)
      .post('/applications')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newApplication);

    const foundPlayableApplicationRes = await request(server)
      .get(`/players/play/${resCreatedApplication.body.id}`)
      .expect(200);

    expect(foundPlayableApplicationRes.body.id).toEqual(
      resCreatedApplication.body.id,
    );
    expect(foundPlayableApplicationRes.body.name).toEqual(
      resCreatedApplication.body.name,
    );
    expect(foundPlayableApplicationRes.body.description).toEqual(
      resCreatedApplication.body.description,
    );
    expect(foundPlayableApplicationRes.body.version).toEqual(
      resCreatedApplication.body.version,
    );
    expect(foundPlayableApplicationRes.body.contact).toEqual(
      resCreatedApplication.body.contact,
    );
    expect(foundPlayableApplicationRes.body.timeLimit).toEqual(
      resCreatedApplication.body.timeLimit,
    );
    expect(foundPlayableApplicationRes.body.logoUrl).toEqual(
      resCreatedApplication.body.logoUrl,
    );
    expect(foundPlayableApplicationRes.body.homepageUrl).toEqual(
      resCreatedApplication.body.homepageUrl,
    );
    expect(foundPlayableApplicationRes.body.documentationUrl).toEqual(
      resCreatedApplication.body.documentationUrl,
    );
    expect(foundPlayableApplicationRes.body).not.toHaveProperty('endpointUrl');
    expect(foundPlayableApplicationRes.body.minimumNumberOfPlayers).toEqual(
      resCreatedApplication.body.minimumNumberOfPlayers,
    );
    expect(foundPlayableApplicationRes.body.maximumNumberOfPlayers).toEqual(
      resCreatedApplication.body.maximumNumberOfPlayers,
    );
    expect(foundPlayableApplicationRes.body.minimumNumberOfScreens).toEqual(
      resCreatedApplication.body.minimumNumberOfScreens,
    );
    expect(foundPlayableApplicationRes.body.maximumNumberOfScreens).toEqual(
      resCreatedApplication.body.maximumNumberOfScreens,
    );
    expect(foundPlayableApplicationRes.body).not.toHaveProperty('role');
    expect(foundPlayableApplicationRes.body).not.toHaveProperty('status');
    expect(foundPlayableApplicationRes.body).not.toHaveProperty('createdAt');
    expect(foundPlayableApplicationRes.body).not.toHaveProperty('updatedAt');
  });

  it('should return a 400 error when trying to get a playable application with a malformed UUID on /players/play/{applicationId} (GET)', async () => {
    return request(server).get('/players/play/notuuid').expect(400);
  });

  it('should return a 404 error when trying to get an undefined playable application on /players/play/{applicationId} (GET)', async () => {
    return request(server)
      .get('/players/play/00000000-0000-0000-0000-000000000000')
      .expect(404);
  });

  it('should return a 404 error when trying to get a disable application on /players/play/{applicationId} (GET)', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.DISABLED,
    );

    const resCreatedApplication = await request(server)
      .post('/applications')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newApplication);

    return request(server)
      .get(`/players/play/${resCreatedApplication.body.id}`)
      .expect(404);
  });

  it('should return a 404 error when trying to get a passive application on /players/play/{applicationId} (GET)', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.PASSIVE,
      ApplicationStatus.ENABLED,
    );

    const resCreatedApplication = await request(server)
      .post('/applications')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newApplication);

    return request(server)
      .get(`/players/play/${resCreatedApplication.body.id}`)
      .expect(404);
  });

  it('should return a 404 error when trying to get a passive disabled application on /players/play/{applicationId} (GET)', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.PASSIVE,
      ApplicationStatus.DISABLED,
    );

    const resCreatedApplication = await request(server)
      .post('/applications')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newApplication);

    return request(server)
      .get(`/players/play/${resCreatedApplication.body.id}`)
      .expect(404);
  });

  it('should be able to get all playable applications', async () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const createdPlayableApplications: any[] = [];

    // eslint-disable-next-line no-plusplus
    for (let applicationNb = 0; applicationNb < 35; applicationNb++) {
      // eslint-disable-next-line no-await-in-loop
      const res = await request(server)
        .post('/applications')
        .set('Authorization', `Bearer ${jwt}`)
        .send(
          new CreateApplicationDto(
            `name${applicationNb}`,
            `description${applicationNb}`,
            `version${applicationNb}`,
            `contact${applicationNb}@example.com`,
            120,
            `https://example${applicationNb}.com/logo.png`,
            `https://example${applicationNb}.com`,
            `https://example${applicationNb}.com/docs`,
            `https://play.example${applicationNb}.com`,
            applicationNb + 1,
            applicationNb + 1,
            applicationNb + 1,
            applicationNb + 1,
            selectRandomEnum<ApplicationRole>(ApplicationRole),
            selectRandomEnum<ApplicationStatus>(ApplicationStatus),
          ),
        );

      if (
        res.body.role === ApplicationRole.ACTIVE &&
        res.body.status === ApplicationStatus.ENABLED
      ) {
        createdPlayableApplications.push({
          id: res.body.id,
          name: res.body.name,
          description: res.body.description,
          version: res.body.version,
          contact: res.body.contact,
          timeLimit: res.body.timeLimit,
          logoUrl: res.body.logoUrl,
          homepageUrl: res.body.homepageUrl,
          documentationUrl: res.body.documentationUrl,
          minimumNumberOfPlayers: res.body.minimumNumberOfPlayers,
          maximumNumberOfPlayers: res.body.maximumNumberOfPlayers,
          minimumNumberOfScreens: res.body.minimumNumberOfScreens,
          maximumNumberOfScreens: res.body.maximumNumberOfScreens,
        });
      }
    }

    createdPlayableApplications.sort(
      (
        application1: ReadPlayableApplicationDto,
        application2: ReadPlayableApplicationDto,
      ) => application1.name.localeCompare(application2.name),
    );

    const limit = 10;
    let page = Math.floor(createdPlayableApplications.length / limit);

    if (page === 0) page = 1;

    const start = (page - 1) * limit;
    const end = start + limit;

    const res = await request(server).get(
      `/players/play?page=${page}&limit=${limit}`,
    );

    expect(createdPlayableApplications.slice(start, end)).toEqual(
      res.body.items,
    );
  });

  it('should return a 400 error when trying to retrieve playable applications page under 1 on /players/play (GET)', async () => {
    return request(server).get('/players/play?page=0').expect(400);
  });

  it('should return a 400 error when trying to retrieve playable applications limit under 1 on /players/play (GET)', async () => {
    return request(server).get('/players/play?page=1&limit=0').expect(400);
  });

  it('cannot access to /players (GET) without a JWT', () => {
    return request(server).get('/players').expect(401);
  });

  it('should be able to get the new player and have all the properties on /players/{playerId} (GET)', async () => {
    const newPlayer = new PlayerEntity(uuidv4());

    const createdPlayer = await playersService.createOne(newPlayer);

    const resFoundPlayer = await request(server)
      .get(`/players/${createdPlayer.id}`)
      .set('Authorization', `Bearer ${jwt}`)
      .expect(200);

    expect(resFoundPlayer.body.id).toEqual(createdPlayer.id);
    expect(resFoundPlayer.body.status).toEqual(createdPlayer.status);
    expect(new Date(resFoundPlayer.body.createdAt)).toEqual(
      createdPlayer.createdAt,
    );
    expect(new Date(resFoundPlayer.body.updatedAt)).toEqual(
      createdPlayer.updatedAt,
    );
  });

  it('should return a 400 error when trying to get a player with a malformed UUID on /players/{playerId} (GET)', async () => {
    return request(server)
      .get('/players/notuuid')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('should return a 404 error when trying to get an undefined player on /players/{playerId} (GET)', async () => {
    return request(server)
      .get('/players/00000000-0000-0000-0000-000000000000')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(404);
  });

  it('should be able to kick the player on /players/{playerId}/kick (PATCH)', async () => {
    const newPlayer = new PlayerEntity(uuidv4());

    const createdPlayer = await playersService.createOne(newPlayer);

    await request(server)
      .patch(`/players/${createdPlayer.id}/kick`)
      .set('Authorization', `Bearer ${jwt}`)
      .expect(204);

    const resFoundPlayer = await request(server)
      .get(`/players/${createdPlayer.id}`)
      .set('Authorization', `Bearer ${jwt}`)
      .expect(200);

    expect(resFoundPlayer.body.status).toEqual(PlayerStatus.KICKED);
  });

  it('should return a 400 error when trying to kick a player with a malformed UUID on /players/{playerId}/kick (PATCH)', async () => {
    return request(server)
      .patch('/players/notuuid/kick')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('should return a 404 error when trying to kick an undefined kick on /players/{playerId}/kick (PATCH)', async () => {
    return request(server)
      .patch('/players/00000000-0000-0000-0000-000000000000/kick')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(404);
  });

  it('should be able to get all players on /players (GET)', async () => {
    const page = 2;
    const limit = 10;

    const start = (page - 1) * limit;
    const end = start + limit;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const createdPlayers: any[] = [];

    // eslint-disable-next-line no-plusplus
    for (let playerNb = 0; playerNb < 35; playerNb++) {
      // eslint-disable-next-line no-await-in-loop
      const createdPlayer = await playersService.createOne(
        new PlayerEntity(uuidv4()),
      );

      createdPlayers.push({
        id: createdPlayer.id,
        status: createdPlayer.status,
        createdAt: createdPlayer.createdAt.toJSON(),
        updatedAt: createdPlayer.updatedAt.toJSON(),
      });
    }

    createdPlayers.sort((player1: ReadPlayerDto, player2: ReadPlayerDto) =>
      player1.id.localeCompare(player2.id),
    );

    const res = await request(server)
      .get(`/players?page=${page}&limit=${limit}`)
      .set('Authorization', `Bearer ${jwt}`);

    const { items, meta, links } = res.body;

    const pagination = new PlayersPaginationDto(
      items,
      new PaginationMetaDto(
        parseInt(meta.currentPage, 10),
        parseInt(meta.itemCount, 10),
        parseInt(meta.itemsPerPage, 10),
        parseInt(meta.totalItems, 10),
        parseInt(meta.totalPages, 10),
      ),
      new PaginationLinksDto(
        links.first,
        links.last,
        links.next,
        links.previous,
      ),
    );

    const results = new PlayersPaginationDto(
      createdPlayers.slice(start, end),
      new PaginationMetaDto(2, 10, 10, 35, 4),
      new PaginationLinksDto(
        '/players?limit=10',
        '/players?page=4&limit=10',
        '/players?page=3&limit=10',
        '/players?page=1&limit=10',
      ),
    );

    expect(pagination).toEqual(results);
  });

  it('should return a 400 error when trying to retrieve players page under 1 on /players (GET)', async () => {
    return request(server)
      .get('/players?page=0')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('should return a 400 error when trying to retrieve players limit under 1 on /players (GET)', async () => {
    return request(server)
      .get('/players?page=1&limit=0')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('cannot access to WS as an user without a JWT', async (done) => {
    const socket = connect(`${serverAddress}/players`, {
      forceNew: true,
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.EXCEPTION, async () => {
      socket.close();
      done();
    });
  });

  it('cannot access to WS as an user with a wrong JWT', async (done) => {
    const socket = connect(`${serverAddress}/players`, {
      forceNew: true,
      query: {
        jwt: 'wrongjwt',
      },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.EXCEPTION, async () => {
      socket.close();
      done();
    });
  });

  it('can access to WS as an user with a JWT', async (done) => {
    const socket = connect(`${serverAddress}/players`, {
      forceNew: true,
      query: { jwt },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      socket.close();
      done();
    });
  });

  it('cannot access to WS as an entity without a player ID', async (done) => {
    const socket = connect(`${serverAddress}/players`, {
      forceNew: true,
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsPlayerEvents.JOIN_AS_PLAYER);
    });

    socket.on(WsCommonEvents.EXCEPTION, async () => {
      socket.close();
      done();
    });
  });

  it('cannot access to WS as an entity with a wrong player ID', async (done) => {
    const socket = connect(`${serverAddress}/players`, {
      forceNew: true,
      query: { id: 'wrongid' },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsPlayerEvents.JOIN_AS_PLAYER);
    });

    socket.on(WsCommonEvents.EXCEPTION, async () => {
      socket.close();
      done();
    });
  });

  it('can access to WS as an entity with a player ID', async (done) => {
    const newPlayer = new PlayerEntity(uuidv4());

    const createdPlayer = await playersService.createOne(newPlayer);

    const socket = connect(`${serverAddress}/players`, {
      forceNew: true,
      query: { id: createdPlayer.id },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsPlayerEvents.JOIN_AS_PLAYER);
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      socket.close();
      done();
    });
  });
});
