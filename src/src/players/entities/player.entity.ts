import { Entity, Column } from 'typeorm';
import { Player, PlayerStatus } from '@beescreens/beescreens';
import { AbstractEntity } from '../../common/abstract-classes';

@Entity({
  name: 'players',
})
export class PlayerEntity extends AbstractEntity implements Player {
  @Column()
  status: PlayerStatus;

  constructor(id: string) {
    super(id);
    this.status = PlayerStatus.CREATED;
  }
}
