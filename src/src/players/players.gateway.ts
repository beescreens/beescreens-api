import {
  WebSocketGateway,
  ConnectedSocket,
  SubscribeMessage,
  WsResponse,
} from '@nestjs/websockets';
import { Inject, UseGuards } from '@nestjs/common';
import { Logger } from 'winston';
import { WsUserEvents, WsPlayerEvents } from '@beescreens/beescreens';
import { Socket } from 'socket.io';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { WsJwtGuard } from '../auth/guards';
import { AbstractGateway } from '../common/abstract-classes';
import { PlayersService } from './players.service';
import { PlayerEntity } from './entities';
import { JwtDto } from '../auth/dto';
import { PlayersIdGuard } from './guards';

@WebSocketGateway({ namespace: 'players' })
export class PlayersGateway extends AbstractGateway {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) readonly logger: Logger,
    readonly playersService: PlayersService,
  ) {
    super(PlayersGateway.name, logger, new Map<Socket, string>());
  }

  @SubscribeMessage(WsUserEvents.JOIN_AS_USER)
  @UseGuards(WsJwtGuard)
  async joinAsUser(@ConnectedSocket() client: Socket): Promise<WsResponse> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const jwt = (client.handshake as any).user as JwtDto;

    return this.join(jwt.jwtPayload.userRole, client, jwt.jwtPayload.userId);
  }

  @SubscribeMessage(WsPlayerEvents.JOIN_AS_PLAYER)
  @UseGuards(PlayersIdGuard)
  async joinAsPlayer(@ConnectedSocket() client: Socket): Promise<WsResponse> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const player = (client.handshake as any).user as PlayerEntity;

    return this.join('external', client, player.id);
  }
}
