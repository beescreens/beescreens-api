import { Test, TestingModule } from '@nestjs/testing';
import { v4 as uuidv4 } from 'uuid';
import { transports } from 'winston';
import { Pagination } from 'nestjs-typeorm-paginate';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { PlayersService } from './players.service';
import { PlayerEntity } from './entities';
import { PlayersController } from './players.controller';
import { PlayersGateway } from './players.gateway';
import { ApplicationsModule } from '../applications/applications.module';

describe('PlayersService', () => {
  let app: TestingModule;
  let playersService: PlayersService;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
        TypeOrmModule.forFeature([PlayerEntity]),
        ApplicationsModule,
      ],
      controllers: [PlayersController],
      providers: [PlayersService, PlayersGateway],
    }).compile();

    playersService = app.get<PlayersService>(PlayersService);
  });

  afterEach(async () => {
    await app.close();
  });

  it('should be able to create the new player', async () => {
    const newPlayer = new PlayerEntity(uuidv4());

    const createdPlayer = await playersService.createOne(newPlayer);

    expect(createdPlayer.id).toEqual(newPlayer.id);
    expect(createdPlayer.status).toEqual(newPlayer.status);
    expect(createdPlayer.createdAt).toBeDefined();
    expect(createdPlayer.updatedAt).toBeDefined();
  });

  it('should be able to get the new player and have all the properties', async () => {
    const newPlayer = new PlayerEntity(uuidv4());

    const createdPlayer = await playersService.createOne(newPlayer);

    const foundPlayer = await playersService.getOne({
      id: createdPlayer.id,
    });

    expect(foundPlayer).toEqual(createdPlayer);
  });

  it('should throw an error when trying to get an undefined player', async () => {
    try {
      await playersService.getOne({ id: '404' });
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be able to update the player', async () => {
    const newPlayer = new PlayerEntity(uuidv4());

    const updatePlayer = new PlayerEntity(newPlayer.id);

    const createdPlayer = await playersService.createOne(newPlayer);

    const updatedPlayer = await playersService.updateOne(
      createdPlayer.id,
      updatePlayer,
    );

    expect(updatedPlayer.id).toEqual(createdPlayer.id);
    expect(updatedPlayer.status).toEqual(updatePlayer.status);
    expect(updatedPlayer.createdAt).toEqual(createdPlayer.createdAt);
    expect(updatedPlayer.updatedAt).toBeDefined();
  });

  it('should throw an error when trying to update an undefined player', async () => {
    try {
      await playersService.updateOne('404', new PlayerEntity(uuidv4()));
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be able to delete the player', async () => {
    const newPlayer = new PlayerEntity(uuidv4());

    const createdPlayer = await playersService.createOne(newPlayer);

    const deletedPlayer = await playersService.deleteOne({
      id: createdPlayer.id,
    });

    expect(deletedPlayer.id).not.toBeDefined();
    expect(deletedPlayer.status).toEqual(createdPlayer.status);
    expect(deletedPlayer.createdAt).toEqual(createdPlayer.createdAt);
    expect(deletedPlayer.updatedAt).toEqual(createdPlayer.updatedAt);
  });

  it('should throw an error when trying to delete an undefined player', async () => {
    try {
      await playersService.deleteOne({ id: '404' });
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be able to get all players', async () => {
    const page = 2;
    const limit = 10;

    const start = (page - 1) * limit;
    const end = start + limit;

    const createdPlayers: PlayerEntity[] = [];

    // eslint-disable-next-line no-plusplus
    for (let playerNb = 0; playerNb < 35; playerNb++) {
      // eslint-disable-next-line no-await-in-loop
      const createdPlayer = await playersService.createOne(
        new PlayerEntity(uuidv4()),
      );

      createdPlayers.push(createdPlayer);
    }

    createdPlayers.sort((player1: PlayerEntity, player2: PlayerEntity) =>
      player1.id.localeCompare(player2.id),
    );

    const query = playersService.repository
      .createQueryBuilder('p')
      .orderBy('p.id', 'ASC');

    const pagination = await playersService.getPagination(
      '/players',
      page,
      limit,
      query,
    );

    const results = new Pagination<PlayerEntity>(
      createdPlayers.slice(start, end),
      {
        currentPage: 2,
        itemCount: 10,
        itemsPerPage: 10,
        totalItems: 35,
        totalPages: 4,
      },
      {
        first: '/players?limit=10',
        last: '/players?page=4&limit=10',
        next: '/players?page=3&limit=10',
        previous: '/players?page=1&limit=10',
      },
    );

    expect(pagination).toEqual(results);
  });

  it('should throw an error when trying to retrieve players page under 1', async () => {
    const query = playersService.repository
      .createQueryBuilder('p')
      .orderBy('p.id', 'ASC');

    try {
      await playersService.getPagination('/players', 0, 1, query);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should throw an error when trying to retrieve players limit under 1', async () => {
    const query = playersService.repository
      .createQueryBuilder('p')
      .orderBy('p.id', 'ASC');

    try {
      await playersService.getPagination('/players', 1, 0, query);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });
});
