import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  MinLength,
  IsInt,
  IsUUID,
  IsEmail,
  Min,
  IsUrl,
} from 'class-validator';
import {
  ApplicationNamePropertySpecs,
  ApplicationMaximumNumberOfScreensPropertySpecs,
  ApplicationMinimumNumberOfScreensPropertySpecs,
  ApplicationMaximumNumberOfPlayersPropertySpecs,
  ApplicationMinimumNumberOfPlayersPropertySpecs,
  ApplicationDocumentationUrlPropertySpecs,
  ApplicationHomepageUrlPropertySpecs,
  ApplicationLogoUrlPropertySpecs,
  ApplicationTimeLimitPropertySpecs,
  ApplicationContactPropertySpecs,
  ApplicationVersionPropertySpecs,
  ApplicationDescriptionPropertySpecs,
} from '../../applications/swagger/properties-specs';
import { EntityIdPropertySpecs } from '../../common/swagger/properties-specs';
import { ReadPlayableApplication } from '../interfaces';

export class ReadPlayableApplicationDto implements ReadPlayableApplication {
  @ApiProperty(EntityIdPropertySpecs)
  @IsUUID()
  readonly id: string;

  @ApiProperty(ApplicationNamePropertySpecs)
  @IsString()
  @MinLength(1)
  readonly name: string;

  @ApiProperty(ApplicationDescriptionPropertySpecs)
  @IsString()
  @MinLength(1)
  readonly description: string;

  @ApiProperty(ApplicationVersionPropertySpecs)
  @IsString()
  @MinLength(1)
  readonly version: string;

  @ApiProperty(ApplicationContactPropertySpecs)
  @IsEmail()
  readonly contact: string;

  @ApiProperty(ApplicationTimeLimitPropertySpecs)
  @IsInt()
  @Min(1)
  readonly timeLimit: number;

  @ApiProperty(ApplicationLogoUrlPropertySpecs)
  @IsUrl()
  readonly logoUrl: string;

  @ApiProperty(ApplicationHomepageUrlPropertySpecs)
  @IsUrl()
  readonly homepageUrl: string;

  @ApiProperty(ApplicationDocumentationUrlPropertySpecs)
  @IsUrl()
  readonly documentationUrl: string;

  @ApiProperty(ApplicationMinimumNumberOfPlayersPropertySpecs)
  @IsInt()
  @Min(1)
  readonly minimumNumberOfPlayers: number;

  @ApiProperty(ApplicationMaximumNumberOfPlayersPropertySpecs)
  @IsInt()
  @Min(1)
  readonly maximumNumberOfPlayers: number;

  @ApiProperty(ApplicationMinimumNumberOfScreensPropertySpecs)
  @IsInt()
  @Min(1)
  readonly minimumNumberOfScreens: number;

  @ApiProperty(ApplicationMaximumNumberOfScreensPropertySpecs)
  @IsInt()
  @Min(1)
  readonly maximumNumberOfScreens: number;

  constructor(
    id: string,
    name: string,
    description: string,
    version: string,
    contact: string,
    timeLimit: number,
    logoUrl: string,
    homepageUrl: string,
    documentationUrl: string,
    minimumNumberOfPlayers: number,
    maximumNumberOfPlayers: number,
    minimumNumberOfScreens: number,
    maximumNumberOfScreens: number,
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.version = version;
    this.contact = contact;
    this.timeLimit = timeLimit;
    this.logoUrl = logoUrl;
    this.homepageUrl = homepageUrl;
    this.documentationUrl = documentationUrl;
    this.minimumNumberOfPlayers = minimumNumberOfPlayers;
    this.maximumNumberOfPlayers = maximumNumberOfPlayers;
    this.minimumNumberOfScreens = minimumNumberOfScreens;
    this.maximumNumberOfScreens = maximumNumberOfScreens;
  }
}
