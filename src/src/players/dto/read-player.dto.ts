import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsDate, IsUUID } from 'class-validator';
import { PlayerStatus } from '@beescreens/beescreens';
import {
  EntityIdPropertySpecs,
  EntityCreatedAtPropertySpecs,
  EntityUpdatedAtPropertySpecs,
} from '../../common/swagger/properties-specs';
import { ReadPlayer } from '../interfaces';
import { PlayerStatusPropertySpecs } from '../swagger/properties-specs';

export class ReadPlayerDto implements ReadPlayer {
  @ApiProperty(EntityIdPropertySpecs)
  @IsUUID()
  readonly id: string;

  @ApiProperty(PlayerStatusPropertySpecs)
  @IsEnum(PlayerStatus)
  readonly status: PlayerStatus;

  @ApiProperty(EntityCreatedAtPropertySpecs)
  @IsDate()
  readonly createdAt: Date;

  @ApiProperty(EntityUpdatedAtPropertySpecs)
  @IsDate()
  readonly updatedAt: Date;

  constructor(
    id: string,
    status: PlayerStatus,
    createdAt: Date,
    updatedAt: Date,
  ) {
    this.id = id;
    this.status = status;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
