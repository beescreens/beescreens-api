import { ApiProperty } from '@nestjs/swagger';
import { IsArray } from 'class-validator';
import { PaginationMetaDto, PaginationLinksDto } from '../../common/dto';
import { PlayersPagination } from '../interfaces';
import { ReadPlayerDto } from './read-player.dto';
import { PlayersPropertySpecs } from '../swagger/properties-specs';

export class PlayersPaginationDto implements PlayersPagination {
  @ApiProperty(PlayersPropertySpecs)
  @IsArray()
  readonly items: ReadPlayerDto[];

  @ApiProperty()
  readonly meta: PaginationMetaDto;

  @ApiProperty()
  readonly links: PaginationLinksDto;

  constructor(
    items: ReadPlayerDto[],
    meta: PaginationMetaDto,
    links: PaginationLinksDto,
  ) {
    this.items = items;
    this.meta = meta;
    this.links = links;
  }
}
