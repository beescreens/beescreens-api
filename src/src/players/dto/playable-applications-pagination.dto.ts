import { ApiProperty } from '@nestjs/swagger';
import { IsArray } from 'class-validator';
import { PaginationMetaDto, PaginationLinksDto } from '../../common/dto';
import { PlayableApplicationsPagination } from '../interfaces';
import { ReadPlayableApplicationDto } from './read-playable-application.dto';
import { PlayableApplicationsPropertySpecs } from '../swagger/properties-specs';

export class PlayableApplicationsPaginationDto
  implements PlayableApplicationsPagination {
  @ApiProperty(PlayableApplicationsPropertySpecs)
  @IsArray()
  readonly items: ReadPlayableApplicationDto[];

  @ApiProperty()
  readonly meta: PaginationMetaDto;

  @ApiProperty()
  readonly links: PaginationLinksDto;

  constructor(
    items: ReadPlayableApplicationDto[],
    meta: PaginationMetaDto,
    links: PaginationLinksDto,
  ) {
    this.items = items;
    this.meta = meta;
    this.links = links;
  }
}
