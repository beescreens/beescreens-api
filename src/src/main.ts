import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import compression from 'compression';
import helmet from 'helmet';
import { AppModule } from './app.module';
import { VERSION } from './constants';

async function bootstrap(): Promise<void> {
  const app = await NestFactory.create(AppModule);

  app.use(compression());

  app.use(helmet());

  app.enableCors();

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );

  app.enableShutdownHooks();

  const swaggerOptions = new DocumentBuilder()
    .setTitle('BeeScreens API')
    .setDescription(
      'BeeScreens is a framework allowing any third-party developpers to develop new interactive applications to be streamed over the Internet from one media (i.e. a phone, computer, etc) to any other media both only using a modern Web Browser.',
    )
    .setVersion(VERSION)
    .setLicense(
      'MIT License',
      'https://gitlab.com/beescreens/beescreens-api/-/blob/master/LICENSE.md',
    )
    .setContact('BeeScreens', 'https://beescreens.ch', 'contact@beescreens.ch')
    .setExternalDoc(
      'Additional Documentation',
      'https://docs.api.beescreens.ch',
    )
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, swaggerOptions);

  SwaggerModule.setup('/', app, document);

  await app.listen(63350);
}

bootstrap();
