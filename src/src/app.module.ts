import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  WinstonModule,
  utilities as nestWinstonModuleUtilities,
} from 'nest-winston';
import { ConfigService, ConfigModule } from '@nestjs/config';
import { format, transports } from 'winston';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { MessagesModule } from './messages/messages.module';
import { DisplayersModule } from './displayers/displayers.module';
import { ApplicationsModule } from './applications/applications.module';
import { PlayersModule } from './players/players.module';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'sqlite',
      entities: [`${__dirname}/**/*.entity{.ts,.js}`],
      synchronize: true,
      database: 'data/beescreens.db',
    }),
    WinstonModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        transports: [
          new transports.Console({
            level: configService.get<string>('CONSOLE_LOGGING_LEVEL'),
            format: format.combine(
              format.timestamp(),
              nestWinstonModuleUtilities.format.nestLike(),
            ),
            handleExceptions: true,
          }),
          new transports.File({
            level: configService.get<string>('FILE_LOGGING_LEVEL'),
            filename: 'data/beescreens.log',
            format: format.combine(format.timestamp(), format.json()),
            handleExceptions: true,
            maxsize: 5242880, // 5MB
            maxFiles: 5,
          }),
        ],
      }),
      inject: [ConfigService],
    }),
    AuthModule,
    MessagesModule,
    UsersModule,
    ApplicationsModule,
    DisplayersModule,
    PlayersModule,
  ],
})
export class AppModule {}
