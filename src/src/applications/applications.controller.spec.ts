import { Test, TestingModule } from '@nestjs/testing';
import {
  ConflictException,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { transports } from 'winston';
import { ApplicationRole, ApplicationStatus } from '@beescreens/beescreens';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { ApplicationsController } from './applications.controller';
import {
  ReadApplicationDto,
  CreateApplicationDto,
  UpdateApplicationDto,
  ApplicationsPaginationDto,
} from './dto';
import { ApplicationsService } from './applications.service';
import { PaginationMetaDto, PaginationLinksDto } from '../common/dto';
import { ApplicationsGateway } from './applications.gateway';
import { ApplicationEntity } from './entities';
import { selectRandomEnum } from '../helpers';

describe('ApplicationsController', () => {
  let app: TestingModule;
  let applicationsController: ApplicationsController;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        TypeOrmModule.forFeature([ApplicationEntity]),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
      ],
      controllers: [ApplicationsController],
      providers: [ApplicationsService, ApplicationsGateway],
    }).compile();

    applicationsController = app.get<ApplicationsController>(
      ApplicationsController,
    );
  });

  afterEach(async () => {
    await app.close();
  });

  it('should be able to add the new application', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const createdApplication = await applicationsController.createApplication(
      newApplication,
    );

    expect(createdApplication.id).toBeDefined();
    expect(createdApplication.apiKey).toBeDefined();
    expect(createdApplication.name).toEqual(newApplication.name);
    expect(createdApplication.description).toEqual(newApplication.description);
    expect(createdApplication.version).toEqual(newApplication.version);
    expect(createdApplication.contact).toEqual(newApplication.contact);
    expect(createdApplication.timeLimit).toEqual(newApplication.timeLimit);
    expect(createdApplication.logoUrl).toEqual(newApplication.logoUrl);
    expect(createdApplication.homepageUrl).toEqual(newApplication.homepageUrl);
    expect(createdApplication.documentationUrl).toEqual(
      newApplication.documentationUrl,
    );
    expect(createdApplication.endpointUrl).toEqual(newApplication.endpointUrl);
    expect(createdApplication.minimumNumberOfPlayers).toEqual(
      newApplication.minimumNumberOfPlayers,
    );
    expect(createdApplication.maximumNumberOfPlayers).toEqual(
      newApplication.maximumNumberOfPlayers,
    );
    expect(createdApplication.minimumNumberOfScreens).toEqual(
      newApplication.minimumNumberOfScreens,
    );
    expect(createdApplication.maximumNumberOfScreens).toEqual(
      newApplication.maximumNumberOfScreens,
    );
    expect(createdApplication.role).toEqual(newApplication.role);
    expect(createdApplication.status).toEqual(newApplication.status);
    expect(createdApplication.createdAt).toBeDefined();
    expect(createdApplication.updatedAt).toBeDefined();
  });

  it('adding a new application with the same name should throw an error', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    try {
      await applicationsController.createApplication(newApplication);
      await applicationsController.createApplication(newApplication);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(ConflictException);
    }
  });

  it('should be able to get the new application and have all the properties', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const createdApplication = await applicationsController.createApplication(
      newApplication,
    );

    const foundApplication = await applicationsController.getApplication(
      createdApplication.id,
    );

    expect(foundApplication.id).toEqual(createdApplication.id);
    expect(foundApplication).not.toHaveProperty('apiKey');
    expect(foundApplication.name).toEqual(createdApplication.name);
    expect(foundApplication.description).toEqual(
      createdApplication.description,
    );
    expect(foundApplication.version).toEqual(createdApplication.version);
    expect(foundApplication.contact).toEqual(createdApplication.contact);
    expect(foundApplication.timeLimit).toEqual(createdApplication.timeLimit);
    expect(foundApplication.logoUrl).toEqual(createdApplication.logoUrl);
    expect(foundApplication.homepageUrl).toEqual(
      createdApplication.homepageUrl,
    );
    expect(foundApplication.documentationUrl).toEqual(
      createdApplication.documentationUrl,
    );
    expect(foundApplication.endpointUrl).toEqual(
      createdApplication.endpointUrl,
    );
    expect(foundApplication.minimumNumberOfPlayers).toEqual(
      createdApplication.minimumNumberOfPlayers,
    );
    expect(foundApplication.maximumNumberOfPlayers).toEqual(
      createdApplication.maximumNumberOfPlayers,
    );
    expect(foundApplication.minimumNumberOfScreens).toEqual(
      createdApplication.minimumNumberOfScreens,
    );
    expect(foundApplication.maximumNumberOfScreens).toEqual(
      createdApplication.maximumNumberOfScreens,
    );
    expect(foundApplication.role).toEqual(createdApplication.role);
    expect(foundApplication.status).toEqual(createdApplication.status);
    expect(foundApplication.createdAt).toEqual(createdApplication.createdAt);
    expect(foundApplication.updatedAt).toEqual(createdApplication.updatedAt);
  });

  it('should throw an error when trying to get an undefined application', async () => {
    try {
      await applicationsController.getApplication('404');
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to update the application', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const updateApplication = new UpdateApplicationDto(
      'name2',
      'description2',
      'version2',
      'contact2@example.com',
      180,
      'https://example2.com/logo.png',
      'https://example2.com',
      'https://example2.com/docs',
      'https://play.example2.com',
      2,
      2,
      2,
      2,
      ApplicationRole.PASSIVE,
      ApplicationStatus.DISABLED,
    );

    const createdApplication = await applicationsController.createApplication(
      newApplication,
    );

    const updatedApplication = await applicationsController.updateApplication(
      createdApplication.id,
      updateApplication,
    );

    expect(updatedApplication.id).toEqual(createdApplication.id);
    expect(updatedApplication).not.toHaveProperty('apiKey');
    expect(updatedApplication.name).toEqual(updateApplication.name);
    expect(updatedApplication.description).toEqual(
      updateApplication.description,
    );
    expect(updatedApplication.version).toEqual(updateApplication.version);
    expect(updatedApplication.contact).toEqual(updateApplication.contact);
    expect(updatedApplication.timeLimit).toEqual(updateApplication.timeLimit);
    expect(updatedApplication.logoUrl).toEqual(updateApplication.logoUrl);
    expect(updatedApplication.homepageUrl).toEqual(
      updateApplication.homepageUrl,
    );
    expect(updatedApplication.documentationUrl).toEqual(
      updateApplication.documentationUrl,
    );
    expect(updatedApplication.endpointUrl).toEqual(
      updateApplication.endpointUrl,
    );
    expect(updatedApplication.minimumNumberOfPlayers).toEqual(
      updateApplication.minimumNumberOfPlayers,
    );
    expect(updatedApplication.maximumNumberOfPlayers).toEqual(
      updateApplication.maximumNumberOfPlayers,
    );
    expect(updatedApplication.minimumNumberOfScreens).toEqual(
      updateApplication.minimumNumberOfScreens,
    );
    expect(updatedApplication.maximumNumberOfScreens).toEqual(
      updateApplication.maximumNumberOfScreens,
    );
    expect(updatedApplication.role).toEqual(updateApplication.role);
    expect(updatedApplication.status).toEqual(updateApplication.status);
    expect(updatedApplication.updatedAt).toEqual(createdApplication.updatedAt);
    expect(updatedApplication.updatedAt).toBeDefined();
  });

  it('should throw an error when trying to update an undefined application', async () => {
    try {
      await applicationsController.updateApplication(
        '404',
        new UpdateApplicationDto(
          'name',
          'description',
          'version',
          'contact@example.com',
          120,
          'https://example.com/logo.png',
          'https://example.com',
          'https://example.com/docs',
          'https://play.example.com',
          1,
          1,
          1,
          1,
          ApplicationRole.ACTIVE,
          ApplicationStatus.ENABLED,
        ),
      );
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to revoke the application', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const createdApplication = await applicationsController.createApplication(
      newApplication,
    );

    const revokedApplication = await applicationsController.revokeApplication(
      createdApplication.id,
    );

    expect(revokedApplication.apiKey).not.toEqual(createdApplication.apiKey);
  });

  it('should throw an error when trying to revoke an undefined application', async () => {
    try {
      await applicationsController.revokeApplication('404');
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to delete the application', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const createdApplication = await applicationsController.createApplication(
      newApplication,
    );

    const deletedApplication = await applicationsController.deleteApplication(
      createdApplication.id,
    );

    expect(deletedApplication.id).toEqual(createdApplication.id);
    expect(deletedApplication).not.toHaveProperty('apiKey');
    expect(deletedApplication.name).toEqual(createdApplication.name);
    expect(deletedApplication.description).toEqual(
      createdApplication.description,
    );
    expect(deletedApplication.version).toEqual(createdApplication.version);
    expect(deletedApplication.contact).toEqual(createdApplication.contact);
    expect(deletedApplication.timeLimit).toEqual(createdApplication.timeLimit);
    expect(deletedApplication.logoUrl).toEqual(createdApplication.logoUrl);
    expect(deletedApplication.homepageUrl).toEqual(
      createdApplication.homepageUrl,
    );
    expect(deletedApplication.documentationUrl).toEqual(
      createdApplication.documentationUrl,
    );
    expect(deletedApplication.endpointUrl).toEqual(
      createdApplication.endpointUrl,
    );
    expect(deletedApplication.minimumNumberOfPlayers).toEqual(
      createdApplication.minimumNumberOfPlayers,
    );
    expect(deletedApplication.maximumNumberOfPlayers).toEqual(
      createdApplication.maximumNumberOfPlayers,
    );
    expect(deletedApplication.minimumNumberOfScreens).toEqual(
      createdApplication.minimumNumberOfScreens,
    );
    expect(deletedApplication.maximumNumberOfScreens).toEqual(
      createdApplication.maximumNumberOfScreens,
    );
    expect(deletedApplication.role).toEqual(createdApplication.role);
    expect(deletedApplication.status).toEqual(createdApplication.status);
    expect(deletedApplication.createdAt).toEqual(createdApplication.createdAt);
    expect(deletedApplication.updatedAt).toEqual(createdApplication.updatedAt);
  });

  it('should throw an error when trying to delete an undefined application', async () => {
    try {
      await applicationsController.deleteApplication('404');
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to get all applications', async () => {
    const page = 2;
    const limit = 10;

    const start = (page - 1) * limit;
    const end = start + limit;

    const createdApplications: ReadApplicationDto[] = [];

    // eslint-disable-next-line no-plusplus
    for (let applicationNb = 0; applicationNb < 35; applicationNb++) {
      // eslint-disable-next-line no-await-in-loop
      const createdApplication = await applicationsController.createApplication(
        new CreateApplicationDto(
          `name${applicationNb}`,
          `description${applicationNb}`,
          `version${applicationNb}`,
          `contact${applicationNb}@example.com`,
          120,
          `https://example${applicationNb}.com/logo.png`,
          `https://example${applicationNb}.com`,
          `https://example${applicationNb}.com/docs`,
          `https://play.example${applicationNb}.com`,
          applicationNb,
          applicationNb,
          applicationNb,
          applicationNb,
          selectRandomEnum<ApplicationRole>(ApplicationRole),
          selectRandomEnum<ApplicationStatus>(ApplicationStatus),
        ),
      );

      createdApplications.push(
        new ReadApplicationDto(
          createdApplication.id,
          createdApplication.name,
          createdApplication.description,
          createdApplication.version,
          createdApplication.contact,
          createdApplication.timeLimit,
          createdApplication.logoUrl,
          createdApplication.homepageUrl,
          createdApplication.documentationUrl,
          createdApplication.endpointUrl,
          createdApplication.minimumNumberOfPlayers,
          createdApplication.maximumNumberOfPlayers,
          createdApplication.minimumNumberOfScreens,
          createdApplication.maximumNumberOfScreens,
          createdApplication.tier,
          createdApplication.role,
          createdApplication.status,
          createdApplication.createdAt,
          createdApplication.updatedAt,
        ),
      );
    }

    createdApplications.sort(
      (application1: ReadApplicationDto, application2: ReadApplicationDto) =>
        application1.name.localeCompare(application2.name),
    );

    const pagination = await applicationsController.getApplications(
      page,
      limit,
    );

    const results = new ApplicationsPaginationDto(
      createdApplications.slice(start, end),
      new PaginationMetaDto(2, 10, 10, 35, 4),
      new PaginationLinksDto(
        '/applications?limit=10',
        '/applications?page=4&limit=10',
        '/applications?page=3&limit=10',
        '/applications?page=1&limit=10',
      ),
    );

    expect(pagination).toEqual(results);
  });

  it('should throw an error when trying to retrieve applications page under 1', async () => {
    try {
      await applicationsController.getApplications(0);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(BadRequestException);
    }
  });

  it('should throw an error when trying to retrieve applications limit under 1', async () => {
    try {
      await applicationsController.getApplications(1, 0);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(BadRequestException);
    }
  });
});
