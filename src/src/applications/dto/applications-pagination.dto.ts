import { ApiProperty } from '@nestjs/swagger';
import { IsArray } from 'class-validator';
import { PaginationMetaDto, PaginationLinksDto } from '../../common/dto';
import { ApplicationsPagination } from '../interfaces';
import { ReadApplicationDto } from './read-application.dto';
import { ApplicationsPropertySpecs } from '../swagger/properties-specs';

export class ApplicationsPaginationDto implements ApplicationsPagination {
  @ApiProperty(ApplicationsPropertySpecs)
  @IsArray()
  readonly items: ReadApplicationDto[];

  @ApiProperty()
  readonly meta: PaginationMetaDto;

  @ApiProperty()
  readonly links: PaginationLinksDto;

  constructor(
    items: ReadApplicationDto[],
    meta: PaginationMetaDto,
    links: PaginationLinksDto,
  ) {
    this.items = items;
    this.meta = meta;
    this.links = links;
  }
}
