import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsEnum,
  IsDate,
  MinLength,
  IsInt,
  IsUUID,
  IsEmail,
  Min,
  IsUrl,
} from 'class-validator';
import {
  ApplicationRole,
  ApplicationStatus,
  ApplicationTier,
} from '@beescreens/beescreens';
import {
  EntityIdPropertySpecs,
  EntityCreatedAtPropertySpecs,
  EntityUpdatedAtPropertySpecs,
  EntityApiKeyPropertySpecs,
} from '../../common/swagger/properties-specs';
import { ApplicationCreated } from '../interfaces';
import {
  ApplicationNamePropertySpecs,
  ApplicationRolePropertySpecs,
  ApplicationStatusPropertySpecs,
  ApplicationDescriptionPropertySpecs,
  ApplicationVersionPropertySpecs,
  ApplicationContactPropertySpecs,
  ApplicationTimeLimitPropertySpecs,
  ApplicationLogoUrlPropertySpecs,
  ApplicationHomepageUrlPropertySpecs,
  ApplicationDocumentationUrlPropertySpecs,
  ApplicationEndpointUrlPropertySpecs,
  ApplicationMinimumNumberOfPlayersPropertySpecs,
  ApplicationMaximumNumberOfPlayersPropertySpecs,
  ApplicationMinimumNumberOfScreensPropertySpecs,
  ApplicationMaximumNumberOfScreensPropertySpecs,
} from '../swagger/properties-specs';

export class ApplicationCreatedDto implements ApplicationCreated {
  @ApiProperty(EntityIdPropertySpecs)
  @IsUUID()
  readonly id: string;

  @ApiProperty(EntityApiKeyPropertySpecs)
  @IsUUID()
  readonly apiKey: string;

  @ApiProperty(ApplicationNamePropertySpecs)
  @IsString()
  @MinLength(1)
  readonly name: string;

  @ApiProperty(ApplicationDescriptionPropertySpecs)
  @IsString()
  @MinLength(1)
  readonly description: string;

  @ApiProperty(ApplicationVersionPropertySpecs)
  @IsString()
  @MinLength(1)
  readonly version: string;

  @ApiProperty(ApplicationContactPropertySpecs)
  @IsEmail()
  readonly contact: string;

  @ApiProperty(ApplicationTimeLimitPropertySpecs)
  @IsInt()
  @Min(1)
  readonly timeLimit: number;

  @ApiProperty(ApplicationLogoUrlPropertySpecs)
  @IsUrl()
  readonly logoUrl: string;

  @ApiProperty(ApplicationHomepageUrlPropertySpecs)
  @IsUrl()
  readonly homepageUrl: string;

  @ApiProperty(ApplicationDocumentationUrlPropertySpecs)
  @IsUrl()
  readonly documentationUrl: string;

  @ApiProperty(ApplicationEndpointUrlPropertySpecs)
  @IsUrl()
  readonly endpointUrl: string;

  @ApiProperty(ApplicationMinimumNumberOfPlayersPropertySpecs)
  @IsInt()
  @Min(1)
  readonly minimumNumberOfPlayers: number;

  @ApiProperty(ApplicationMaximumNumberOfPlayersPropertySpecs)
  @IsInt()
  @Min(1)
  readonly maximumNumberOfPlayers: number;

  @ApiProperty(ApplicationMinimumNumberOfScreensPropertySpecs)
  @IsInt()
  @Min(1)
  readonly minimumNumberOfScreens: number;

  @ApiProperty(ApplicationMaximumNumberOfScreensPropertySpecs)
  @IsInt()
  @Min(1)
  readonly maximumNumberOfScreens: number;

  @ApiProperty(ApplicationRolePropertySpecs)
  @IsEnum(ApplicationTier)
  readonly tier: ApplicationTier;

  @ApiProperty(ApplicationRolePropertySpecs)
  @IsEnum(ApplicationRole)
  readonly role: ApplicationRole;

  @ApiProperty(ApplicationStatusPropertySpecs)
  @IsEnum(ApplicationStatus)
  readonly status: ApplicationStatus;

  @ApiProperty(EntityCreatedAtPropertySpecs)
  @IsDate()
  readonly createdAt: Date;

  @ApiProperty(EntityUpdatedAtPropertySpecs)
  @IsDate()
  readonly updatedAt: Date;

  constructor(
    id: string,
    apiKey: string,
    name: string,
    description: string,
    version: string,
    contact: string,
    timeLimit: number,
    logoUrl: string,
    homepageUrl: string,
    documentationUrl: string,
    endpointUrl: string,
    minimumNumberOfPlayers: number,
    maximumNumberOfPlayers: number,
    minimumNumberOfScreens: number,
    maximumNumberOfScreens: number,
    tier: ApplicationTier,
    role: ApplicationRole,
    status: ApplicationStatus,
    createdAt: Date,
    updatedAt: Date,
  ) {
    this.id = id;
    this.apiKey = apiKey;
    this.name = name;
    this.description = description;
    this.version = version;
    this.contact = contact;
    this.timeLimit = timeLimit;
    this.logoUrl = logoUrl;
    this.homepageUrl = homepageUrl;
    this.documentationUrl = documentationUrl;
    this.endpointUrl = endpointUrl;
    this.minimumNumberOfPlayers = minimumNumberOfPlayers;
    this.maximumNumberOfPlayers = maximumNumberOfPlayers;
    this.minimumNumberOfScreens = minimumNumberOfScreens;
    this.maximumNumberOfScreens = maximumNumberOfScreens;
    this.tier = tier;
    this.role = role;
    this.status = status;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
