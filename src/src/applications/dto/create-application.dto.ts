import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsEnum,
  MinLength,
  IsEmail,
  IsInt,
  Min,
  IsUrl,
} from 'class-validator';
import { ApplicationRole, ApplicationStatus } from '@beescreens/beescreens';
import { CreateApplication } from '../interfaces';
import {
  ApplicationNamePropertySpecs,
  ApplicationDescriptionPropertySpecs,
  ApplicationVersionPropertySpecs,
  ApplicationContactPropertySpecs,
  ApplicationTimeLimitPropertySpecs,
  ApplicationLogoUrlPropertySpecs,
  ApplicationHomepageUrlPropertySpecs,
  ApplicationDocumentationUrlPropertySpecs,
  ApplicationEndpointUrlPropertySpecs,
  ApplicationMinimumNumberOfPlayersPropertySpecs,
  ApplicationMaximumNumberOfPlayersPropertySpecs,
  ApplicationMinimumNumberOfScreensPropertySpecs,
  ApplicationMaximumNumberOfScreensPropertySpecs,
  ApplicationRolePropertySpecs,
  ApplicationStatusPropertySpecs,
} from '../swagger/properties-specs';

export class CreateApplicationDto implements CreateApplication {
  @ApiProperty(ApplicationNamePropertySpecs)
  @IsString()
  @MinLength(1)
  readonly name: string;

  @ApiProperty(ApplicationDescriptionPropertySpecs)
  @IsString()
  @MinLength(1)
  readonly description: string;

  @ApiProperty(ApplicationVersionPropertySpecs)
  @IsString()
  @MinLength(1)
  readonly version: string;

  @ApiProperty(ApplicationContactPropertySpecs)
  @IsEmail()
  readonly contact: string;

  @ApiProperty(ApplicationTimeLimitPropertySpecs)
  @IsInt()
  @Min(1)
  readonly timeLimit: number;

  @ApiProperty(ApplicationLogoUrlPropertySpecs)
  @IsUrl()
  readonly logoUrl: string;

  @ApiProperty(ApplicationHomepageUrlPropertySpecs)
  @IsUrl()
  readonly homepageUrl: string;

  @ApiProperty(ApplicationDocumentationUrlPropertySpecs)
  @IsUrl()
  readonly documentationUrl: string;

  @ApiProperty(ApplicationEndpointUrlPropertySpecs)
  @IsUrl()
  readonly endpointUrl: string;

  @ApiProperty(ApplicationMinimumNumberOfPlayersPropertySpecs)
  @IsInt()
  @Min(1)
  readonly minimumNumberOfPlayers: number;

  @ApiProperty(ApplicationMaximumNumberOfPlayersPropertySpecs)
  @IsInt()
  @Min(1)
  readonly maximumNumberOfPlayers: number;

  @ApiProperty(ApplicationMinimumNumberOfScreensPropertySpecs)
  @IsInt()
  @Min(1)
  readonly minimumNumberOfScreens: number;

  @ApiProperty(ApplicationMaximumNumberOfScreensPropertySpecs)
  @IsInt()
  @Min(1)
  readonly maximumNumberOfScreens: number;

  @ApiProperty(ApplicationRolePropertySpecs)
  @IsEnum(ApplicationRole)
  readonly role: ApplicationRole;

  @ApiProperty(ApplicationStatusPropertySpecs)
  @IsEnum(ApplicationStatus)
  readonly status: ApplicationStatus;

  constructor(
    name: string,
    description: string,
    version: string,
    contact: string,
    timeLimit: number,
    logoUrl: string,
    homepageUrl: string,
    documentationUrl: string,
    endpointUrl: string,
    minimumNumberOfPlayers: number,
    maximumNumberOfPlayers: number,
    minimumNumberOfScreens: number,
    maximumNumberOfScreens: number,
    role: ApplicationRole,
    status: ApplicationStatus,
  ) {
    this.name = name;
    this.description = description;
    this.version = version;
    this.contact = contact;
    this.timeLimit = timeLimit;
    this.logoUrl = logoUrl;
    this.homepageUrl = homepageUrl;
    this.documentationUrl = documentationUrl;
    this.endpointUrl = endpointUrl;
    this.minimumNumberOfPlayers = minimumNumberOfPlayers;
    this.maximumNumberOfPlayers = maximumNumberOfPlayers;
    this.minimumNumberOfScreens = minimumNumberOfScreens;
    this.maximumNumberOfScreens = maximumNumberOfScreens;
    this.role = role;
    this.status = status;
  }
}
