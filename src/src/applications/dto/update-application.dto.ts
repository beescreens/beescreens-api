import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  MinLength,
  IsEnum,
  IsInt,
  Min,
  IsUrl,
  IsEmail,
} from 'class-validator';
import { ApplicationRole, ApplicationStatus } from '@beescreens/beescreens';
import { UpdateApplication } from '../interfaces';
import {
  ApplicationNamePropertySpecs,
  ApplicationRolePropertySpecs,
  ApplicationStatusPropertySpecs,
  ApplicationMinimumNumberOfPlayersPropertySpecs,
  ApplicationMaximumNumberOfPlayersPropertySpecs,
  ApplicationMinimumNumberOfScreensPropertySpecs,
  ApplicationMaximumNumberOfScreensPropertySpecs,
  ApplicationEndpointUrlPropertySpecs,
  ApplicationDocumentationUrlPropertySpecs,
  ApplicationHomepageUrlPropertySpecs,
  ApplicationLogoUrlPropertySpecs,
  ApplicationTimeLimitPropertySpecs,
  ApplicationContactPropertySpecs,
  ApplicationVersionPropertySpecs,
  ApplicationDescriptionPropertySpecs,
} from '../swagger/properties-specs';

export class UpdateApplicationDto implements UpdateApplication {
  @ApiProperty(ApplicationNamePropertySpecs)
  @IsString()
  @MinLength(1)
  readonly name: string;

  @ApiProperty(ApplicationDescriptionPropertySpecs)
  @IsString()
  @MinLength(1)
  readonly description: string;

  @ApiProperty(ApplicationVersionPropertySpecs)
  @IsString()
  @MinLength(1)
  readonly version: string;

  @ApiProperty(ApplicationContactPropertySpecs)
  @IsEmail()
  readonly contact: string;

  @ApiProperty(ApplicationTimeLimitPropertySpecs)
  @IsInt()
  @Min(1)
  readonly timeLimit: number;

  @ApiProperty(ApplicationLogoUrlPropertySpecs)
  @IsUrl()
  readonly logoUrl: string;

  @ApiProperty(ApplicationHomepageUrlPropertySpecs)
  @IsUrl()
  readonly homepageUrl: string;

  @ApiProperty(ApplicationDocumentationUrlPropertySpecs)
  @IsUrl()
  readonly documentationUrl: string;

  @ApiProperty(ApplicationEndpointUrlPropertySpecs)
  @IsUrl()
  readonly endpointUrl: string;

  @ApiProperty(ApplicationMinimumNumberOfPlayersPropertySpecs)
  @IsInt()
  @Min(1)
  readonly minimumNumberOfPlayers: number;

  @ApiProperty(ApplicationMaximumNumberOfPlayersPropertySpecs)
  @IsInt()
  @Min(1)
  readonly maximumNumberOfPlayers: number;

  @ApiProperty(ApplicationMinimumNumberOfScreensPropertySpecs)
  @IsInt()
  @Min(1)
  readonly minimumNumberOfScreens: number;

  @ApiProperty(ApplicationMaximumNumberOfScreensPropertySpecs)
  @IsInt()
  @Min(1)
  readonly maximumNumberOfScreens: number;

  @ApiProperty(ApplicationRolePropertySpecs)
  @IsEnum(ApplicationRole)
  readonly role: ApplicationRole;

  @ApiProperty(ApplicationStatusPropertySpecs)
  @IsEnum(ApplicationStatus)
  readonly status: ApplicationStatus;

  constructor(
    name: string,
    description: string,
    version: string,
    contact: string,
    timeLimit: number,
    logoUrl: string,
    homepageUrl: string,
    documentationUrl: string,
    endpointUrl: string,
    minimumNumberOfPlayers: number,
    maximumNumberOfPlayers: number,
    minimumNumberOfScreens: number,
    maximumNumberOfScreens: number,
    role: ApplicationRole,
    status: ApplicationStatus,
  ) {
    this.name = name;
    this.description = description;
    this.version = version;
    this.contact = contact;
    this.timeLimit = timeLimit;
    this.logoUrl = logoUrl;
    this.homepageUrl = homepageUrl;
    this.documentationUrl = documentationUrl;
    this.endpointUrl = endpointUrl;
    this.minimumNumberOfPlayers = minimumNumberOfPlayers;
    this.maximumNumberOfPlayers = maximumNumberOfPlayers;
    this.minimumNumberOfScreens = minimumNumberOfScreens;
    this.maximumNumberOfScreens = maximumNumberOfScreens;
    this.role = role;
    this.status = status;
  }
}
