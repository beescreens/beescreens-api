import { Injectable, ExecutionContext, CanActivate } from '@nestjs/common';
import { ApplicationStatus } from '@beescreens/beescreens';
import { ApplicationsService } from '../applications.service';
import { ApplicationEntity } from '../entities';

@Injectable()
export class ApplicationsApiKeyGuard implements CanActivate {
  constructor(readonly applicationsService: ApplicationsService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const client = context.switchToWs().getClient();

    let application: ApplicationEntity;

    try {
      application = await this.applicationsService.getOne({
        apiKey: client.handshake.query.apiKey,
      });
    } catch (error) {
      return false;
    }

    if (application.status !== ApplicationStatus.DISABLED) {
      client.handshake.user = application;
      return true;
    }

    return false;
  }
}
