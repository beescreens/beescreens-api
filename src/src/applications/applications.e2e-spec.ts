import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import request from 'supertest';
import { ScheduleModule } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { transports } from 'winston';
import {
  ApplicationRole,
  ApplicationStatus,
  WsCommonEvents,
  ApplicationTier,
  WsUserEvents,
  WsApplicationEvents,
} from '@beescreens/beescreens';
import { connect } from 'socket.io-client';
import compression from 'compression';
import helmet from 'helmet';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { MessagesModule } from '../messages/messages.module';
import { ApplicationsModule } from './applications.module';
import { AuthModule } from '../auth/auth.module';
import {
  ReadApplicationDto,
  CreateApplicationDto,
  UpdateApplicationDto,
  ApplicationsPaginationDto,
  ApplicationCreatedDto,
  ApplicationUpdatedDto,
  ApplicationDeletedDto,
} from './dto';
import { PaginationMetaDto, PaginationLinksDto } from '../common/dto';
import { UsersModule } from '../users/users.module';
import { DisplayersModule } from '../displayers/displayers.module';
import { selectRandomEnum } from '../helpers';

describe('Applications (e2e)', () => {
  let app: INestApplication;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let server: any;
  let serverAddress: string;
  let configService: ConfigService;
  let jwt: string;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ScheduleModule.forRoot(),
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
        AuthModule,
        MessagesModule,
        UsersModule,
        ApplicationsModule,
        DisplayersModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    app.use(compression());

    app.use(helmet());

    app.enableCors();

    app.useGlobalPipes(
      new ValidationPipe({
        transform: true,
      }),
    );

    app.enableShutdownHooks();

    server = app.getHttpServer();
    configService = app.get<ConfigService>(ConfigService);

    const { address, port } = server.listen().address();

    serverAddress = `http://[${address}]:${port}`;

    await app.init();

    const res = await request(server)
      .post('/login')
      .send({
        username: configService.get<string>('DEFAULT_USERNAME'),
        password: configService.get<string>('DEFAULT_PASSWORD'),
      });

    jwt = res.body.jwt;
  });

  afterEach(async () => {
    await app.close();
  });

  it('cannot access to /applications (GET) without a JWT', () => {
    return request(server).get('/applications').expect(401);
  });

  it('cannot access to /applications (POST) without a JWT', () => {
    return request(server).post('/applications').expect(401);
  });

  it('should be able to add the new application on /applications (POST)', async (done) => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const socket = connect(`${serverAddress}/applications`, {
      forceNew: true,
      query: { jwt },
    });

    socket.on(
      WsApplicationEvents.APPLICATION_CREATED,
      (createdApplication: ApplicationCreatedDto) => {
        expect(createdApplication.id).toBeDefined();

        socket.close();

        done();
      },
    );

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      const res = await request(server)
        .post('/applications')
        .set('Authorization', `Bearer ${jwt}`)
        .send(newApplication);

      expect(res.status).toEqual(201);

      expect(res.body.id).toBeDefined();
      expect(res.body.apiKey).toBeDefined();
      expect(res.body.name).toEqual(newApplication.name);
      expect(res.body.description).toEqual(newApplication.description);
      expect(res.body.version).toEqual(newApplication.version);
      expect(res.body.contact).toEqual(newApplication.contact);
      expect(res.body.timeLimit).toEqual(newApplication.timeLimit);
      expect(res.body.logoUrl).toEqual(newApplication.logoUrl);
      expect(res.body.homepageUrl).toEqual(newApplication.homepageUrl);
      expect(res.body.documentationUrl).toEqual(
        newApplication.documentationUrl,
      );
      expect(res.body.endpointUrl).toEqual(newApplication.endpointUrl);
      expect(res.body.minimumNumberOfPlayers).toEqual(
        newApplication.minimumNumberOfPlayers,
      );
      expect(res.body.maximumNumberOfPlayers).toEqual(
        newApplication.maximumNumberOfPlayers,
      );
      expect(res.body.minimumNumberOfScreens).toEqual(
        newApplication.minimumNumberOfScreens,
      );
      expect(res.body.maximumNumberOfScreens).toEqual(
        newApplication.maximumNumberOfScreens,
      );
      expect(res.body.role).toEqual(newApplication.role);
      expect(res.body.status).toEqual(newApplication.status);
      expect(res.body.createdAt).toBeDefined();
      expect(res.body.updatedAt).toBeDefined();
    });
  });

  it('adding a new application on /applications (POST) with the same applicationname should return a 409 error', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    await request(server)
      .post('/applications')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newApplication);

    return request(server)
      .post('/applications')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newApplication)
      .expect(409);
  });

  it('adding a new application with malformed entries should return an 400 error', async () => {
    const res = await request(server)
      .post('/applications')
      .set('Authorization', `Bearer ${jwt}`)
      .send(
        new CreateApplicationDto(
          '',
          '',
          '',
          '',
          0,
          'logo',
          'example',
          'example',
          'example',
          0,
          0,
          0,
          0,
          ApplicationRole.ACTIVE,
          ApplicationStatus.ENABLED,
        ),
      )
      .expect(400);

    expect(res.body).toEqual({
      statusCode: 400,
      message: [
        'name must be longer than or equal to 1 characters',
        'description must be longer than or equal to 1 characters',
        'version must be longer than or equal to 1 characters',
        'contact must be an email',
        'timeLimit must not be less than 1',
        'logoUrl must be an URL address',
        'homepageUrl must be an URL address',
        'documentationUrl must be an URL address',
        'endpointUrl must be an URL address',
        'minimumNumberOfPlayers must not be less than 1',
        'maximumNumberOfPlayers must not be less than 1',
        'minimumNumberOfScreens must not be less than 1',
        'maximumNumberOfScreens must not be less than 1',
      ],
      error: 'Bad Request',
    });
  });

  it('should be able to get the new application and have all the properties on /applications/{applicationId} (GET)', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const resCreatedApplication = await request(server)
      .post('/applications')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newApplication);

    const resFoundApplication = await request(server)
      .get(`/applications/${resCreatedApplication.body.id}`)
      .set('Authorization', `Bearer ${jwt}`)
      .expect(200);

    expect(resFoundApplication.body.id).toEqual(resCreatedApplication.body.id);
    expect(resFoundApplication.body).not.toHaveProperty('apiKey');
    expect(resFoundApplication.body.name).toEqual(
      resCreatedApplication.body.name,
    );
    expect(resFoundApplication.body.description).toEqual(
      resCreatedApplication.body.description,
    );
    expect(resFoundApplication.body.version).toEqual(
      resCreatedApplication.body.version,
    );
    expect(resFoundApplication.body.contact).toEqual(
      resCreatedApplication.body.contact,
    );
    expect(resFoundApplication.body.timeLimit).toEqual(
      resCreatedApplication.body.timeLimit,
    );
    expect(resFoundApplication.body.logoUrl).toEqual(
      resCreatedApplication.body.logoUrl,
    );
    expect(resFoundApplication.body.homepageUrl).toEqual(
      resCreatedApplication.body.homepageUrl,
    );
    expect(resFoundApplication.body.documentationUrl).toEqual(
      resCreatedApplication.body.documentationUrl,
    );
    expect(resFoundApplication.body.endpointUrl).toEqual(
      resCreatedApplication.body.endpointUrl,
    );
    expect(resFoundApplication.body.minimumNumberOfPlayers).toEqual(
      resCreatedApplication.body.minimumNumberOfPlayers,
    );
    expect(resFoundApplication.body.maximumNumberOfPlayers).toEqual(
      resCreatedApplication.body.maximumNumberOfPlayers,
    );
    expect(resFoundApplication.body.minimumNumberOfScreens).toEqual(
      resCreatedApplication.body.minimumNumberOfScreens,
    );
    expect(resFoundApplication.body.maximumNumberOfScreens).toEqual(
      resCreatedApplication.body.maximumNumberOfScreens,
    );
    expect(resFoundApplication.body.role).toEqual(
      resCreatedApplication.body.role,
    );
    expect(resFoundApplication.body.status).toEqual(
      resCreatedApplication.body.status,
    );
    expect(resFoundApplication.body.createdAt).toEqual(
      resCreatedApplication.body.createdAt,
    );
    expect(resFoundApplication.body.updatedAt).toEqual(
      resCreatedApplication.body.updatedAt,
    );
  });

  it('should return a 400 error when trying to get a application with a malformed UUID on /application/{applicationId} (GET)', async () => {
    return request(server)
      .get('/applications/notuuid')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('should return a 404 error when trying to get an undefined application on /applications/{applicationId} (GET)', async () => {
    return request(server)
      .get('/applications/00000000-0000-0000-0000-000000000000')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(404);
  });

  it('should be able to update the application on /applications/{applicationId} (PATCH)', async (done) => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const updateApplication = new UpdateApplicationDto(
      'name2',
      'description2',
      'version2',
      'contact2@example.com',
      180,
      'https://example2.com/logo.png',
      'https://example2.com',
      'https://example2.com/docs',
      'https://play.example2.com',
      2,
      2,
      2,
      2,
      ApplicationRole.PASSIVE,
      ApplicationStatus.DISABLED,
    );

    const socket = connect(`${serverAddress}/applications`, {
      forceNew: true,
      query: { jwt },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      const resCreatedApplication = await request(server)
        .post('/applications')
        .set('Authorization', `Bearer ${jwt}`)
        .send(newApplication);

      socket.on(
        WsApplicationEvents.APPLICATION_UPDATED,
        (updatedApplication: ApplicationUpdatedDto) => {
          expect(updatedApplication.id).toEqual(resCreatedApplication.body.id);

          socket.close();

          done();
        },
      );

      const resUpdatedApplication = await request(server)
        .patch(`/applications/${resCreatedApplication.body.id}`)
        .set('Authorization', `Bearer ${jwt}`)
        .send(updateApplication)
        .expect(200);

      expect(resUpdatedApplication.body.id).toEqual(
        resCreatedApplication.body.id,
      );
      expect(resUpdatedApplication.body).not.toHaveProperty('apiKey');
      expect(resUpdatedApplication.body.name).toEqual(updateApplication.name);
      expect(resUpdatedApplication.body.description).toEqual(
        updateApplication.description,
      );
      expect(resUpdatedApplication.body.version).toEqual(
        updateApplication.version,
      );
      expect(resUpdatedApplication.body.contact).toEqual(
        updateApplication.contact,
      );
      expect(resUpdatedApplication.body.timeLimit).toEqual(
        updateApplication.timeLimit,
      );
      expect(resUpdatedApplication.body.logoUrl).toEqual(
        updateApplication.logoUrl,
      );
      expect(resUpdatedApplication.body.homepageUrl).toEqual(
        updateApplication.homepageUrl,
      );
      expect(resUpdatedApplication.body.documentationUrl).toEqual(
        updateApplication.documentationUrl,
      );
      expect(resUpdatedApplication.body.endpointUrl).toEqual(
        updateApplication.endpointUrl,
      );
      expect(resUpdatedApplication.body.minimumNumberOfPlayers).toEqual(
        updateApplication.minimumNumberOfPlayers,
      );
      expect(resUpdatedApplication.body.maximumNumberOfPlayers).toEqual(
        updateApplication.maximumNumberOfPlayers,
      );
      expect(resUpdatedApplication.body.minimumNumberOfScreens).toEqual(
        updateApplication.minimumNumberOfScreens,
      );
      expect(resUpdatedApplication.body.maximumNumberOfScreens).toEqual(
        updateApplication.maximumNumberOfScreens,
      );
      expect(resUpdatedApplication.body.role).toEqual(updateApplication.role);
      expect(resUpdatedApplication.body.status).toEqual(
        updateApplication.status,
      );
      expect(resUpdatedApplication.body.updatedAt).toEqual(
        resCreatedApplication.body.updatedAt,
      );
      expect(resUpdatedApplication.body.updatedAt).toBeDefined();
    });
  });

  it('should return a 400 error when trying to update a application with a malformed UUID on /applications/{applicationId} (PATCH)', async () => {
    return request(server)
      .patch('/applications/notuuid')
      .set('Authorization', `Bearer ${jwt}`)
      .send(
        new UpdateApplicationDto(
          'name2',
          'description2',
          'version2',
          'contact2@example.com',
          180,
          'https://example2.com/logo.png',
          'https://example2.com',
          'https://example2.com/docs',
          'https://play.example2.com',
          2,
          2,
          2,
          2,
          ApplicationRole.PASSIVE,
          ApplicationStatus.DISABLED,
        ),
      )
      .expect(400);
  });

  it('should return a 404 error when trying to update an undefined application on /applications/{applicationId} (PATCH)', async () => {
    return request(server)
      .patch('/applications/00000000-0000-0000-0000-000000000000')
      .set('Authorization', `Bearer ${jwt}`)
      .send(
        new UpdateApplicationDto(
          'name2',
          'description2',
          'version2',
          'contact2@example.com',
          180,
          'https://example2.com/logo.png',
          'https://example2.com',
          'https://example2.com/docs',
          'https://play.example2.com',
          2,
          2,
          2,
          2,
          ApplicationRole.PASSIVE,
          ApplicationStatus.DISABLED,
        ),
      )
      .expect(404);
  });

  it('should be able to revoke the application on /applications/{applicationId}/revoke (PATCH)', async () => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const resOne = await request(server)
      .post('/applications')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newApplication);

    const resTwo = await request(server)
      .patch(`/applications/${resOne.body.id}/revoke`)
      .set('Authorization', `Bearer ${jwt}`);

    expect(resTwo.body.apiKey).not.toEqual(resOne.body.apiKey);
  });

  it('should return a 400 error when trying to revoke a application with a malformed UUID on /applications/{applicationId}/revoke (PATCH)', async () => {
    return request(server)
      .patch('/applications/notuuid/revoke')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('should return a 404 error when trying to revoke an undefined application on /applications/{applicationId}/revoke (PATCH)', async () => {
    return request(server)
      .patch('/applications/00000000-0000-0000-0000-000000000000/revoke')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(404);
  });

  it('should be able to delete the application on /applications/{applicationId} (DELETE)', async (done) => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const socket = connect(`${serverAddress}/applications`, {
      forceNew: true,
      query: { jwt },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(
      WsApplicationEvents.APPLICATION_DELETED,
      (deletedApplication: ApplicationDeletedDto) => {
        expect(deletedApplication.id).toBeDefined();

        socket.close();

        done();
      },
    );

    socket.on(WsCommonEvents.JOINED, async () => {
      const resCreatedApplication = await request(server)
        .post('/applications')
        .set('Authorization', `Bearer ${jwt}`)
        .send(newApplication);

      const resDeletedApplication = await request(server)
        .delete(`/applications/${resCreatedApplication.body.id}`)
        .set('Authorization', `Bearer ${jwt}`)
        .expect(200);

      expect(resDeletedApplication.body.id).toEqual(
        resCreatedApplication.body.id,
      );
      expect(resDeletedApplication.body).not.toHaveProperty('apiKey');
      expect(resDeletedApplication.body.name).toEqual(
        resCreatedApplication.body.name,
      );
      expect(resDeletedApplication.body.description).toEqual(
        resCreatedApplication.body.description,
      );
      expect(resDeletedApplication.body.version).toEqual(
        resCreatedApplication.body.version,
      );
      expect(resDeletedApplication.body.contact).toEqual(
        resCreatedApplication.body.contact,
      );
      expect(resDeletedApplication.body.timeLimit).toEqual(
        resCreatedApplication.body.timeLimit,
      );
      expect(resDeletedApplication.body.logoUrl).toEqual(
        resCreatedApplication.body.logoUrl,
      );
      expect(resDeletedApplication.body.homepageUrl).toEqual(
        resCreatedApplication.body.homepageUrl,
      );
      expect(resDeletedApplication.body.documentationUrl).toEqual(
        resCreatedApplication.body.documentationUrl,
      );
      expect(resDeletedApplication.body.endpointUrl).toEqual(
        resCreatedApplication.body.endpointUrl,
      );
      expect(resDeletedApplication.body.minimumNumberOfPlayers).toEqual(
        resCreatedApplication.body.minimumNumberOfPlayers,
      );
      expect(resDeletedApplication.body.maximumNumberOfPlayers).toEqual(
        resCreatedApplication.body.maximumNumberOfPlayers,
      );
      expect(resDeletedApplication.body.minimumNumberOfScreens).toEqual(
        resCreatedApplication.body.minimumNumberOfScreens,
      );
      expect(resDeletedApplication.body.maximumNumberOfScreens).toEqual(
        resCreatedApplication.body.maximumNumberOfScreens,
      );
      expect(resDeletedApplication.body.role).toEqual(
        resCreatedApplication.body.role,
      );
      expect(resDeletedApplication.body.status).toEqual(
        resCreatedApplication.body.status,
      );
      expect(resDeletedApplication.body.createdAt).toEqual(
        resCreatedApplication.body.createdAt,
      );
      expect(resDeletedApplication.body.updatedAt).toEqual(
        resCreatedApplication.body.updatedAt,
      );
    });
  });

  it('should return a 400 error when trying to delete a application with a malformed UUID on /applications/{applicationId} (DELETE)', async () => {
    return request(server)
      .delete('/applications/notuuid')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('should return a 404 error when trying to delete an undefined application on /applications/{applicationId} (DELETE)', async () => {
    return request(server)
      .delete('/applications/00000000-0000-0000-0000-000000000000')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(404);
  });

  it('should be able to get all applications on /applications (GET)', async () => {
    const page = 2;
    const limit = 10;

    const start = (page - 1) * limit;
    const end = start + limit;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const createdApplications: any[] = [];

    // eslint-disable-next-line no-plusplus
    for (let applicationNb = 0; applicationNb < 35; applicationNb++) {
      // eslint-disable-next-line no-await-in-loop
      const res = await request(server)
        .post('/applications')
        .set('Authorization', `Bearer ${jwt}`)
        .send(
          new CreateApplicationDto(
            `name${applicationNb}`,
            `description${applicationNb}`,
            `version${applicationNb}`,
            `contact${applicationNb}@example.com`,
            120,
            `https://example${applicationNb}.com/logo.png`,
            `https://example${applicationNb}.com`,
            `https://example${applicationNb}.com/docs`,
            `https://play.example${applicationNb}.com`,
            applicationNb + 1,
            applicationNb + 1,
            applicationNb + 1,
            applicationNb + 1,
            selectRandomEnum<ApplicationRole>(ApplicationRole),
            selectRandomEnum<ApplicationStatus>(ApplicationStatus),
          ),
        );

      createdApplications.push({
        id: res.body.id,
        name: res.body.name,
        description: res.body.description,
        version: res.body.version,
        contact: res.body.contact,
        timeLimit: res.body.timeLimit,
        logoUrl: res.body.logoUrl,
        homepageUrl: res.body.homepageUrl,
        documentationUrl: res.body.documentationUrl,
        endpointUrl: res.body.endpointUrl,
        minimumNumberOfPlayers: res.body.minimumNumberOfPlayers,
        maximumNumberOfPlayers: res.body.maximumNumberOfPlayers,
        minimumNumberOfScreens: res.body.minimumNumberOfScreens,
        maximumNumberOfScreens: res.body.maximumNumberOfScreens,
        tier: ApplicationTier.BRONZE,
        role: res.body.role,
        status: res.body.status,
        createdAt: res.body.createdAt,
        updatedAt: res.body.updatedAt,
      });
    }

    createdApplications.sort(
      (application1: ReadApplicationDto, application2: ReadApplicationDto) =>
        application1.name.localeCompare(application2.name),
    );

    const res = await request(server)
      .get(`/applications?page=${page}&limit=${limit}`)
      .set('Authorization', `Bearer ${jwt}`);

    const { items, meta, links } = res.body;

    const pagination = new ApplicationsPaginationDto(
      items,
      new PaginationMetaDto(
        parseInt(meta.currentPage, 10),
        parseInt(meta.itemCount, 10),
        parseInt(meta.itemsPerPage, 10),
        parseInt(meta.totalItems, 10),
        parseInt(meta.totalPages, 10),
      ),
      new PaginationLinksDto(
        links.first,
        links.last,
        links.next,
        links.previous,
      ),
    );

    const results = new ApplicationsPaginationDto(
      createdApplications.slice(start, end),
      new PaginationMetaDto(2, 10, 10, 35, 4),
      new PaginationLinksDto(
        '/applications?limit=10',
        '/applications?page=4&limit=10',
        '/applications?page=3&limit=10',
        '/applications?page=1&limit=10',
      ),
    );

    expect(pagination).toEqual(results);
  });

  it('should return a 400 error when trying to retrieve applications page under 1 on /applications (GET)', async () => {
    return request(server)
      .get('/applications?page=0')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('should return a 400 error when trying to retrieve applications limit under 1 on /applications (GET)', async () => {
    return request(server)
      .get('/applications?page=1&limit=0')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('cannot access to WS as an application without a JWT', async (done) => {
    const socket = connect(`${serverAddress}/applications`, {
      forceNew: true,
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.EXCEPTION, async () => {
      socket.close();
      done();
    });
  });

  it('cannot access to WS as an application with a wrong JWT', async (done) => {
    const socket = connect(`${serverAddress}/applications`, {
      forceNew: true,
      query: {
        jwt: 'wrongjwt',
      },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.EXCEPTION, async () => {
      socket.close();
      done();
    });
  });

  it('can access to WS as an application with a JWT', async (done) => {
    const socket = connect(`${serverAddress}/applications`, {
      forceNew: true,
      query: { jwt },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      socket.close();
      done();
    });
  });

  it('cannot access to WS as an entity without an API key', async (done) => {
    const socket = connect(`${serverAddress}/applications`, {
      forceNew: true,
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsApplicationEvents.JOIN_AS_APPLICATION);
    });

    socket.on(WsCommonEvents.EXCEPTION, async () => {
      socket.close();
      done();
    });
  });

  it('cannot access to WS as an entity with a wrong API key', async (done) => {
    const socket = connect(`${serverAddress}/applications`, {
      forceNew: true,
      query: { apiKey: 'wrongapikey' },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsApplicationEvents.JOIN_AS_APPLICATION);
    });

    socket.on(WsCommonEvents.EXCEPTION, async () => {
      socket.close();
      done();
    });
  });

  it('can access to WS as an entity with an API key', async (done) => {
    const newApplication = new CreateApplicationDto(
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const res = await request(server)
      .post('/applications')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newApplication);

    const socket = connect(`${serverAddress}/applications`, {
      forceNew: true,
      query: { apiKey: res.body.apiKey },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsApplicationEvents.JOIN_AS_APPLICATION);
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      socket.close();
      done();
    });
  });
});
