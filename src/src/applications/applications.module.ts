import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApplicationsController } from './applications.controller';
import { ApplicationsService } from './applications.service';
import { ApplicationsGateway } from './applications.gateway';
import { ApplicationEntity } from './entities';

@Module({
  imports: [TypeOrmModule.forFeature([ApplicationEntity])],
  controllers: [ApplicationsController],
  providers: [ApplicationsService, ApplicationsGateway],
  exports: [ApplicationsService, ApplicationsGateway],
})
export class ApplicationsModule {}
