import { Entity, Column, Unique } from 'typeorm';
import {
  Application,
  ApplicationTier,
  ApplicationRole,
  ApplicationStatus,
} from '@beescreens/beescreens';
import { AbstractEntity } from '../../common/abstract-classes';

@Entity({
  name: 'applications',
})
@Unique(['name'])
@Unique(['apiKey'])
export class ApplicationEntity extends AbstractEntity implements Application {
  @Column()
  apiKey: string;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  version: string;

  @Column()
  contact: string;

  @Column()
  timeLimit: number;

  @Column()
  logoUrl: string;

  @Column()
  homepageUrl: string;

  @Column()
  documentationUrl: string;

  @Column()
  endpointUrl: string;

  @Column()
  minimumNumberOfPlayers: number;

  @Column()
  maximumNumberOfPlayers: number;

  @Column()
  minimumNumberOfScreens: number;

  @Column()
  maximumNumberOfScreens: number;

  @Column()
  tier: ApplicationTier;

  @Column()
  role: ApplicationRole;

  @Column()
  status: ApplicationStatus;

  constructor(
    id: string,
    apiKey: string,
    name: string,
    description: string,
    version: string,
    contact: string,
    timeLimit: number,
    logoUrl: string,
    homepageUrl: string,
    documentationUrl: string,
    endpointUrl: string,
    minimumNumberOfPlayers: number,
    maximumNumberOfPlayers: number,
    minimumNumberOfScreens: number,
    maximumNumberOfScreens: number,
    role: ApplicationRole,
    status: ApplicationStatus,
  ) {
    super(id);
    this.apiKey = apiKey;
    this.name = name;
    this.description = description;
    this.version = version;
    this.contact = contact;
    this.timeLimit = timeLimit;
    this.logoUrl = logoUrl;
    this.homepageUrl = homepageUrl;
    this.documentationUrl = documentationUrl;
    this.endpointUrl = endpointUrl;
    this.minimumNumberOfPlayers = minimumNumberOfPlayers;
    this.maximumNumberOfPlayers = maximumNumberOfPlayers;
    this.minimumNumberOfScreens = minimumNumberOfScreens;
    this.maximumNumberOfScreens = maximumNumberOfScreens;
    this.tier = ApplicationTier.BRONZE;
    this.role = role;
    this.status = status;
  }
}
