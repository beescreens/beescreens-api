import {
  WebSocketGateway,
  ConnectedSocket,
  SubscribeMessage,
  WsResponse,
} from '@nestjs/websockets';
import { Inject, UseGuards } from '@nestjs/common';
import { Logger } from 'winston';
import { WsUserEvents, WsApplicationEvents } from '@beescreens/beescreens';
import { Socket } from 'socket.io';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { WsJwtGuard } from '../auth/guards';
import { ApplicationsApiKeyGuard } from './guards';
import { AbstractGateway } from '../common/abstract-classes';
import { ApplicationsService } from './applications.service';
import { ApplicationEntity } from './entities';
import { JwtDto } from '../auth/dto';

@WebSocketGateway({ namespace: 'applications' })
export class ApplicationsGateway extends AbstractGateway {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) readonly logger: Logger,
    readonly applicationsService: ApplicationsService,
  ) {
    super(ApplicationsGateway.name, logger, new Map<Socket, string>());
  }

  @SubscribeMessage(WsUserEvents.JOIN_AS_USER)
  @UseGuards(WsJwtGuard)
  async joinAsUser(@ConnectedSocket() client: Socket): Promise<WsResponse> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const jwt = (client.handshake as any).user as JwtDto;

    return this.join(jwt.jwtPayload.userRole, client, jwt.jwtPayload.userId);
  }

  @SubscribeMessage(WsApplicationEvents.JOIN_AS_APPLICATION)
  @UseGuards(ApplicationsApiKeyGuard)
  async joinAsApplication(
    @ConnectedSocket() client: Socket,
  ): Promise<WsResponse> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const application = (client.handshake as any).user as ApplicationEntity;

    return this.join('external', client, application.apiKey);
  }
}
