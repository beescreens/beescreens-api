import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Logger } from 'winston';
import { InjectRepository } from '@nestjs/typeorm';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { ApplicationEntity } from './entities';
import { AbstractService } from '../common/abstract-classes';

@Injectable()
export class ApplicationsService extends AbstractService<ApplicationEntity> {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) readonly logger: Logger,
    @InjectRepository(ApplicationEntity)
    readonly repository: Repository<ApplicationEntity>,
  ) {
    super(ApplicationsService.name, logger, repository);
  }
}
