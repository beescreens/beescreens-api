import { Test, TestingModule } from '@nestjs/testing';
import { v4 as uuidv4 } from 'uuid';
import { transports } from 'winston';
import { Pagination } from 'nestjs-typeorm-paginate';
import { ApplicationRole, ApplicationStatus } from '@beescreens/beescreens';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { ApplicationsService } from './applications.service';
import { ApplicationEntity } from './entities';
import { ApplicationsController } from './applications.controller';
import { ApplicationsGateway } from './applications.gateway';
import { selectRandomEnum } from '../helpers';

describe('ApplicationsService', () => {
  let app: TestingModule;
  let applicationsService: ApplicationsService;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        TypeOrmModule.forFeature([ApplicationEntity]),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
      ],
      controllers: [ApplicationsController],
      providers: [ApplicationsService, ApplicationsGateway],
    }).compile();

    applicationsService = app.get<ApplicationsService>(ApplicationsService);
  });

  afterEach(async () => {
    await app.close();
  });

  it('should be able to create the new application', async () => {
    const newApplication = new ApplicationEntity(
      uuidv4(),
      uuidv4(),
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const createdApplication = await applicationsService.createOne(
      newApplication,
    );

    expect(createdApplication.id).toEqual(newApplication.id);
    expect(createdApplication.apiKey).toEqual(newApplication.apiKey);
    expect(createdApplication.name).toEqual(newApplication.name);
    expect(createdApplication.description).toEqual(newApplication.description);
    expect(createdApplication.version).toEqual(newApplication.version);
    expect(createdApplication.contact).toEqual(newApplication.contact);
    expect(createdApplication.timeLimit).toEqual(newApplication.timeLimit);
    expect(createdApplication.logoUrl).toEqual(newApplication.logoUrl);
    expect(createdApplication.homepageUrl).toEqual(newApplication.homepageUrl);
    expect(createdApplication.documentationUrl).toEqual(
      newApplication.documentationUrl,
    );
    expect(createdApplication.endpointUrl).toEqual(newApplication.endpointUrl);
    expect(createdApplication.minimumNumberOfPlayers).toEqual(
      newApplication.minimumNumberOfPlayers,
    );
    expect(createdApplication.maximumNumberOfPlayers).toEqual(
      newApplication.maximumNumberOfPlayers,
    );
    expect(createdApplication.minimumNumberOfScreens).toEqual(
      newApplication.minimumNumberOfScreens,
    );
    expect(createdApplication.maximumNumberOfScreens).toEqual(
      newApplication.maximumNumberOfScreens,
    );
    expect(createdApplication.role).toEqual(newApplication.role);
    expect(createdApplication.status).toEqual(newApplication.status);
    expect(createdApplication.createdAt).toBeDefined();
    expect(createdApplication.updatedAt).toBeDefined();
  });

  it('creating a new application with the same name should throw an error', async () => {
    const newApplicationOne = new ApplicationEntity(
      uuidv4(),
      uuidv4(),
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const newApplicationTwo = new ApplicationEntity(
      uuidv4(),
      uuidv4(),
      newApplicationOne.name,
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    try {
      await applicationsService.createOne(newApplicationOne);
      await applicationsService.createOne(newApplicationTwo);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be able to get the new application and have all the properties', async () => {
    const newApplication = new ApplicationEntity(
      uuidv4(),
      uuidv4(),
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const createdApplication = await applicationsService.createOne(
      newApplication,
    );

    const foundApplication = await applicationsService.getOne({
      id: createdApplication.id,
    });

    expect(foundApplication).toEqual(createdApplication);
  });

  it('should throw an error when trying to get an undefined application', async () => {
    try {
      await applicationsService.getOne({ id: '404' });
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be able to update the application', async () => {
    const newApplication = new ApplicationEntity(
      uuidv4(),
      uuidv4(),
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const updateApplication = new ApplicationEntity(
      newApplication.id,
      newApplication.apiKey,
      'name2',
      'description2',
      'version2',
      'contact2@example.com',
      180,
      'https://example2.com/logo.png',
      'https://example2.com',
      'https://example2.com/docs',
      'https://play.example2.com',
      2,
      2,
      2,
      2,
      ApplicationRole.PASSIVE,
      ApplicationStatus.DISABLED,
    );

    const createdApplication = await applicationsService.createOne(
      newApplication,
    );

    const updatedApplication = await applicationsService.updateOne(
      createdApplication.id,
      updateApplication,
    );

    expect(updatedApplication.id).toEqual(createdApplication.id);
    expect(updatedApplication.apiKey).toEqual(updateApplication.apiKey);
    expect(updatedApplication.name).toEqual(updateApplication.name);
    expect(updatedApplication.description).toEqual(
      updateApplication.description,
    );
    expect(updatedApplication.version).toEqual(updateApplication.version);
    expect(updatedApplication.contact).toEqual(updateApplication.contact);
    expect(updatedApplication.timeLimit).toEqual(updateApplication.timeLimit);
    expect(updatedApplication.logoUrl).toEqual(updateApplication.logoUrl);
    expect(updatedApplication.homepageUrl).toEqual(
      updateApplication.homepageUrl,
    );
    expect(updatedApplication.documentationUrl).toEqual(
      updateApplication.documentationUrl,
    );
    expect(updatedApplication.endpointUrl).toEqual(
      updateApplication.endpointUrl,
    );
    expect(updatedApplication.minimumNumberOfPlayers).toEqual(
      updateApplication.minimumNumberOfPlayers,
    );
    expect(updatedApplication.maximumNumberOfPlayers).toEqual(
      updateApplication.maximumNumberOfPlayers,
    );
    expect(updatedApplication.minimumNumberOfScreens).toEqual(
      updateApplication.minimumNumberOfScreens,
    );
    expect(updatedApplication.maximumNumberOfScreens).toEqual(
      updateApplication.maximumNumberOfScreens,
    );
    expect(updatedApplication.role).toEqual(updateApplication.role);
    expect(updatedApplication.status).toEqual(updateApplication.status);
    expect(updatedApplication.createdAt).toEqual(createdApplication.createdAt);
    expect(updatedApplication.updatedAt).toBeDefined();
  });

  it('should throw an error when trying to update an undefined application', async () => {
    try {
      await applicationsService.updateOne(
        '404',
        new ApplicationEntity(
          uuidv4(),
          uuidv4(),
          '404',
          'description',
          'version',
          'contact@example.com',
          120,
          'https://example.com/logo.png',
          'https://example.com',
          'https://example.com/docs',
          'https://play.example.com',
          1,
          1,
          1,
          1,
          ApplicationRole.ACTIVE,
          ApplicationStatus.ENABLED,
        ),
      );
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be able to delete the application', async () => {
    const newApplication = new ApplicationEntity(
      uuidv4(),
      uuidv4(),
      'name',
      'description',
      'version',
      'contact@example.com',
      120,
      'https://example.com/logo.png',
      'https://example.com',
      'https://example.com/docs',
      'https://play.example.com',
      1,
      1,
      1,
      1,
      ApplicationRole.ACTIVE,
      ApplicationStatus.ENABLED,
    );

    const createdApplication = await applicationsService.createOne(
      newApplication,
    );

    const deletedApplication = await applicationsService.deleteOne({
      id: createdApplication.id,
    });

    expect(deletedApplication.id).not.toBeDefined();
    expect(deletedApplication.apiKey).toEqual(createdApplication.apiKey);
    expect(deletedApplication.name).toEqual(createdApplication.name);
    expect(deletedApplication.description).toEqual(
      createdApplication.description,
    );
    expect(deletedApplication.version).toEqual(createdApplication.version);
    expect(deletedApplication.contact).toEqual(createdApplication.contact);
    expect(deletedApplication.timeLimit).toEqual(createdApplication.timeLimit);
    expect(deletedApplication.logoUrl).toEqual(createdApplication.logoUrl);
    expect(deletedApplication.homepageUrl).toEqual(
      createdApplication.homepageUrl,
    );
    expect(deletedApplication.documentationUrl).toEqual(
      createdApplication.documentationUrl,
    );
    expect(deletedApplication.endpointUrl).toEqual(
      createdApplication.endpointUrl,
    );
    expect(deletedApplication.minimumNumberOfPlayers).toEqual(
      createdApplication.minimumNumberOfPlayers,
    );
    expect(deletedApplication.maximumNumberOfPlayers).toEqual(
      createdApplication.maximumNumberOfPlayers,
    );
    expect(deletedApplication.minimumNumberOfScreens).toEqual(
      createdApplication.minimumNumberOfScreens,
    );
    expect(deletedApplication.maximumNumberOfScreens).toEqual(
      createdApplication.maximumNumberOfScreens,
    );
    expect(deletedApplication.role).toEqual(createdApplication.role);
    expect(deletedApplication.status).toEqual(createdApplication.status);
    expect(deletedApplication.createdAt).toEqual(createdApplication.createdAt);
    expect(deletedApplication.updatedAt).toEqual(createdApplication.updatedAt);
  });

  it('should throw an error when trying to delete an undefined application', async () => {
    try {
      await applicationsService.deleteOne({ id: '404' });
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be able to get all applications', async () => {
    const page = 2;
    const limit = 10;

    const start = (page - 1) * limit;
    const end = start + limit;

    const createdApplications: ApplicationEntity[] = [];

    // eslint-disable-next-line no-plusplus
    for (let applicationNb = 0; applicationNb < 35; applicationNb++) {
      // eslint-disable-next-line no-await-in-loop
      const createdApplication = await applicationsService.createOne(
        new ApplicationEntity(
          uuidv4(),
          uuidv4(),
          `name${applicationNb}`,
          `description${applicationNb}`,
          `version${applicationNb}`,
          `contact${applicationNb}@example.com`,
          120,
          `https://example${applicationNb}.com/logo.png`,
          `https://example${applicationNb}.com`,
          `https://example${applicationNb}.com/docs`,
          `https://play.example${applicationNb}.com`,
          applicationNb,
          applicationNb,
          applicationNb,
          applicationNb,
          selectRandomEnum<ApplicationRole>(ApplicationRole),
          selectRandomEnum<ApplicationStatus>(ApplicationStatus),
        ),
      );

      createdApplications.push(createdApplication);
    }

    createdApplications.sort(
      (application1: ApplicationEntity, application2: ApplicationEntity) =>
        application1.name.localeCompare(application2.name),
    );

    const query = applicationsService.repository
      .createQueryBuilder('a')
      .orderBy('a.name', 'ASC');

    const pagination = await applicationsService.getPagination(
      '/applications',
      page,
      limit,
      query,
    );

    const results = new Pagination<ApplicationEntity>(
      createdApplications.slice(start, end),
      {
        currentPage: 2,
        itemCount: 10,
        itemsPerPage: 10,
        totalItems: 35,
        totalPages: 4,
      },
      {
        first: '/applications?limit=10',
        last: '/applications?page=4&limit=10',
        next: '/applications?page=3&limit=10',
        previous: '/applications?page=1&limit=10',
      },
    );

    expect(pagination).toEqual(results);
  });

  it('should throw an error when trying to retrieve applications page under 1', async () => {
    const query = applicationsService.repository
      .createQueryBuilder('a')
      .orderBy('a.name', 'ASC');

    try {
      await applicationsService.getPagination('/applications', 0, 1, query);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should throw an error when trying to retrieve applications limit under 1', async () => {
    const query = applicationsService.repository
      .createQueryBuilder('a')
      .orderBy('a.name', 'ASC');

    try {
      await applicationsService.getPagination('/applications', 1, 0, query);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });
});
