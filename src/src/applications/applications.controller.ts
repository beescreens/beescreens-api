import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Patch,
  Inject,
  ConflictException,
  NotFoundException,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  Query,
  BadRequestException,
  ParseUUIDPipe,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBody,
  ApiOperation,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiConflictResponse,
  ApiParam,
  ApiBadRequestResponse,
  ApiQuery,
} from '@nestjs/swagger';
import { Logger } from 'winston';
import { v4 as uuidv4 } from 'uuid';
import { Pagination } from 'nestjs-typeorm-paginate';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { UserRole, WsApplicationEvents } from '@beescreens/beescreens';
import { HttpJwtGuard } from '../auth/guards';
import {
  MissingOrIncorrectFieldsResponseSpecs,
  MissingOrIncorrectParamsResponseSpecs,
} from '../common/swagger/responses-specs';
import { ApplicationsService } from './applications.service';
import {
  ApplicationsRetrievedResponseSpecs,
  ApplicationCreatedResponseSpecs,
  ApplicationConflictResponseSpecs,
  ApplicationRetrievedResponseSpecs,
  ApplicationNotFoundResponseSpecs,
  ApplicationDeletedResponseSpecs,
  ApplicationUpdatedResponseSpecs,
  ApplicationApiKeyResponseSpecs,
} from './swagger/responses-specs';
import {
  GetApplicationsOperationSpecs,
  CreateApplicationOperationSpecs,
  GetApplicationOperationSpecs,
  DeleteApplicationOperationSpecs,
  UpdateApplicationOperationSpecs,
  RevokeApplicationOperationSpecs,
} from './swagger/operations-specs';
import {
  ReadApplicationDto,
  CreateApplicationDto,
  UpdateApplicationDto,
  ApplicationCreatedDto,
  ApplicationUpdatedDto,
  ApplicationDeletedDto,
  ApplicationsPaginationDto,
} from './dto';
import {
  CreateApplicationBodySpecs,
  UpdateApplicationBodySpecs,
} from './swagger/bodies-specs';
import { ApplicationIdApiParameterSpecs } from './swagger/parameters-specs';
import {
  PaginationPageQuerySpecs,
  PaginationLimitQuerySpecs,
} from '../common/swagger/queries-specs';
import { ApplicationEntity } from './entities';

import {
  PaginationMetaDto,
  PaginationLinksDto,
  EntityApiKeyDto,
  EntityIdDto,
} from '../common/dto';
import { ApplicationsGateway } from './applications.gateway';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DefaultIntValuePipe } from '../common/pipes';
import { HttpAuth } from '../common/decorators';

@ApiTags('Applications')
@Controller('applications')
export class ApplicationsController {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) readonly logger: Logger,
    readonly applicationsService: ApplicationsService,
    readonly applicationsGateway: ApplicationsGateway,
  ) {}

  @Get()
  @HttpAuth(HttpJwtGuard)
  @ApiQuery(PaginationPageQuerySpecs)
  @ApiQuery(PaginationLimitQuerySpecs)
  @ApiOkResponse(ApplicationsRetrievedResponseSpecs)
  @ApiOperation(GetApplicationsOperationSpecs)
  async getApplications(
    @Query('page', new DefaultIntValuePipe(1)) page = 10,
    @Query('limit', new DefaultIntValuePipe(10)) limit = 10,
  ): Promise<ApplicationsPaginationDto> {
    this.logger.info(`page: ${page}, limit: ${limit}`, {
      context: `${ApplicationsController.name}::${this.getApplications.name}`,
    });

    const query = this.applicationsService.repository
      .createQueryBuilder('a')
      .orderBy('a.name', 'ASC');

    let pagination: Pagination<ApplicationEntity>;

    try {
      pagination = await this.applicationsService.getPagination(
        '/applications',
        page,
        limit,
        query,
      );
    } catch (error) {
      throw new BadRequestException();
    }

    const applications = pagination.items.map(
      async (application) =>
        new ReadApplicationDto(
          application.id,
          application.name,
          application.description,
          application.version,
          application.contact,
          application.timeLimit,
          application.logoUrl,
          application.homepageUrl,
          application.documentationUrl,
          application.endpointUrl,
          application.minimumNumberOfPlayers,
          application.maximumNumberOfPlayers,
          application.minimumNumberOfScreens,
          application.maximumNumberOfScreens,
          application.tier,
          application.role,
          application.status,
          application.createdAt,
          application.updatedAt,
        ),
    );

    const { meta, links } = pagination;

    return new ApplicationsPaginationDto(
      await Promise.all(applications),
      new PaginationMetaDto(
        meta.currentPage,
        meta.itemCount,
        meta.itemsPerPage,
        meta.totalItems,
        meta.totalPages,
      ),
      new PaginationLinksDto(
        links.first || '',
        links.last || '',
        links.next || '',
        links.previous || '',
      ),
    );
  }

  @Post()
  @HttpAuth(HttpJwtGuard)
  @ApiCreatedResponse(ApplicationCreatedResponseSpecs)
  @ApiConflictResponse(ApplicationConflictResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiOperation(CreateApplicationOperationSpecs)
  @ApiBody(CreateApplicationBodySpecs)
  async createApplication(
    @Body() applicationDto: CreateApplicationDto,
  ): Promise<ApplicationCreatedDto> {
    this.logger.info(`body: ${JSON.stringify(applicationDto)}`, {
      context: `${ApplicationsController.name}::${this.createApplication.name}`,
    });

    let newApplication: ApplicationEntity;

    try {
      const application = new ApplicationEntity(
        uuidv4(),
        uuidv4(),
        applicationDto.name,
        applicationDto.description,
        applicationDto.version,
        applicationDto.contact,
        applicationDto.timeLimit,
        applicationDto.logoUrl,
        applicationDto.homepageUrl,
        applicationDto.documentationUrl,
        applicationDto.endpointUrl,
        applicationDto.minimumNumberOfPlayers,
        applicationDto.maximumNumberOfPlayers,
        applicationDto.minimumNumberOfScreens,
        applicationDto.maximumNumberOfScreens,
        applicationDto.role,
        applicationDto.status,
      );

      newApplication = await this.applicationsService.createOne(application);
    } catch (error) {
      throw new ConflictException();
    }

    const {
      id,
      apiKey,
      name,
      description,
      version,
      contact,
      timeLimit,
      logoUrl,
      homepageUrl,
      documentationUrl,
      endpointUrl,
      minimumNumberOfPlayers,
      maximumNumberOfPlayers,
      minimumNumberOfScreens,
      maximumNumberOfScreens,
      tier,
      role,
      status,
      createdAt,
      updatedAt,
    } = newApplication;

    const newApplicationDto = new ApplicationCreatedDto(
      id,
      apiKey,
      name,
      description,
      version,
      contact,
      timeLimit,
      logoUrl,
      homepageUrl,
      documentationUrl,
      endpointUrl,
      minimumNumberOfPlayers,
      maximumNumberOfPlayers,
      minimumNumberOfScreens,
      maximumNumberOfScreens,
      tier,
      role,
      status,
      createdAt,
      updatedAt,
    );

    await this.applicationsGateway.emitToAll(
      [UserRole.ADMIN, UserRole.MODERATOR, UserRole.READ_ONLY],
      WsApplicationEvents.APPLICATION_CREATED,
      new EntityIdDto(id),
    );

    return newApplicationDto;
  }

  @Get(':applicationId')
  @HttpAuth(HttpJwtGuard)
  @ApiParam(ApplicationIdApiParameterSpecs)
  @ApiOkResponse(ApplicationRetrievedResponseSpecs)
  @ApiNotFoundResponse(ApplicationNotFoundResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectParamsResponseSpecs)
  @ApiOperation(GetApplicationOperationSpecs)
  async getApplication(
    @Param('applicationId', ParseUUIDPipe) applicationId: string,
  ): Promise<ReadApplicationDto> {
    this.logger.info(`applicationId: ${applicationId}`, {
      context: `${ApplicationsController.name}::${this.getApplication.name}`,
    });

    let foundApplication: ApplicationEntity;

    try {
      foundApplication = await this.applicationsService.getOne({
        id: applicationId,
      });
    } catch (error) {
      throw new NotFoundException();
    }

    const {
      id,
      name,
      description,
      version,
      contact,
      timeLimit,
      logoUrl,
      homepageUrl,
      documentationUrl,
      endpointUrl,
      minimumNumberOfPlayers,
      maximumNumberOfPlayers,
      minimumNumberOfScreens,
      maximumNumberOfScreens,
      tier,
      role,
      status,
      createdAt,
      updatedAt,
    } = foundApplication;

    return new ReadApplicationDto(
      id,
      name,
      description,
      version,
      contact,
      timeLimit,
      logoUrl,
      homepageUrl,
      documentationUrl,
      endpointUrl,
      minimumNumberOfPlayers,
      maximumNumberOfPlayers,
      minimumNumberOfScreens,
      maximumNumberOfScreens,
      tier,
      role,
      status,
      createdAt,
      updatedAt,
    );
  }

  @Patch(':applicationId')
  @HttpAuth(HttpJwtGuard)
  @ApiOkResponse(ApplicationUpdatedResponseSpecs)
  @ApiNotFoundResponse(ApplicationNotFoundResponseSpecs)
  @ApiConflictResponse(ApplicationConflictResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiParam(ApplicationIdApiParameterSpecs)
  @ApiOperation(UpdateApplicationOperationSpecs)
  @ApiBody(UpdateApplicationBodySpecs)
  async updateApplication(
    @Param('applicationId', ParseUUIDPipe) applicationId: string,
    @Body() applicationDto: UpdateApplicationDto,
  ): Promise<ApplicationUpdatedDto> {
    this.logger.info(
      `applicationId: ${applicationId}, body: ${JSON.stringify(
        applicationDto,
      )}`,
      {
        context: `${ApplicationsController.name}::${this.updateApplication.name}`,
      },
    );

    let applicationToUpdate: ApplicationEntity;

    try {
      applicationToUpdate = await this.applicationsService.getOne({
        id: applicationId,
      });
    } catch (error) {
      throw new NotFoundException();
    }

    const {
      name,
      description,
      version,
      contact,
      timeLimit,
      logoUrl,
      homepageUrl,
      documentationUrl,
      endpointUrl,
      minimumNumberOfPlayers,
      maximumNumberOfPlayers,
      minimumNumberOfScreens,
      maximumNumberOfScreens,
      role,
      status,
    } = applicationDto;

    applicationToUpdate.name = name;
    applicationToUpdate.description = description;
    applicationToUpdate.version = version;
    applicationToUpdate.contact = contact;
    applicationToUpdate.timeLimit = timeLimit;
    applicationToUpdate.logoUrl = logoUrl;
    applicationToUpdate.homepageUrl = homepageUrl;
    applicationToUpdate.documentationUrl = documentationUrl;
    applicationToUpdate.endpointUrl = endpointUrl;
    applicationToUpdate.minimumNumberOfPlayers = minimumNumberOfPlayers;
    applicationToUpdate.maximumNumberOfPlayers = maximumNumberOfPlayers;
    applicationToUpdate.minimumNumberOfScreens = minimumNumberOfScreens;
    applicationToUpdate.maximumNumberOfScreens = maximumNumberOfScreens;
    applicationToUpdate.role = role;
    applicationToUpdate.status = status;

    const updatedApplication = await this.applicationsService.updateOne(
      applicationId,
      applicationToUpdate,
    );

    const updatedApplicationDto = new ApplicationUpdatedDto(
      updatedApplication.id,
      updatedApplication.name,
      updatedApplication.description,
      updatedApplication.version,
      updatedApplication.contact,
      updatedApplication.timeLimit,
      updatedApplication.logoUrl,
      updatedApplication.homepageUrl,
      updatedApplication.documentationUrl,
      updatedApplication.endpointUrl,
      updatedApplication.minimumNumberOfPlayers,
      updatedApplication.maximumNumberOfPlayers,
      updatedApplication.minimumNumberOfScreens,
      updatedApplication.maximumNumberOfScreens,
      updatedApplication.tier,
      updatedApplication.role,
      updatedApplication.status,
      updatedApplication.createdAt,
      updatedApplication.updatedAt,
    );

    await this.applicationsGateway.emitToAll(
      [UserRole.ADMIN, UserRole.MODERATOR, UserRole.READ_ONLY],
      WsApplicationEvents.APPLICATION_UPDATED,
      new EntityIdDto(applicationId),
    );

    return updatedApplicationDto;
  }

  @Delete(':applicationId')
  @HttpAuth(HttpJwtGuard)
  @ApiOkResponse(ApplicationDeletedResponseSpecs)
  @ApiNotFoundResponse(ApplicationNotFoundResponseSpecs)
  @ApiParam(ApplicationIdApiParameterSpecs)
  @ApiOperation(DeleteApplicationOperationSpecs)
  async deleteApplication(
    @Param('applicationId', ParseUUIDPipe) applicationId: string,
  ): Promise<ApplicationDeletedDto> {
    this.logger.info(`applicationId: ${applicationId}`, {
      context: `${ApplicationsController.name}::${this.deleteApplication.name}`,
    });

    let deletedApplication: ApplicationEntity;

    try {
      deletedApplication = await this.applicationsService.deleteOne({
        id: applicationId,
      });
      deletedApplication.id = applicationId;
    } catch (error) {
      throw new NotFoundException();
    }

    const {
      id,
      name,
      description,
      version,
      contact,
      timeLimit,
      logoUrl,
      homepageUrl,
      documentationUrl,
      endpointUrl,
      minimumNumberOfPlayers,
      maximumNumberOfPlayers,
      minimumNumberOfScreens,
      maximumNumberOfScreens,
      tier,
      role,
      status,
      createdAt,
      updatedAt,
    } = deletedApplication;

    const deletedApplicationDto = new ApplicationDeletedDto(
      id,
      name,
      description,
      version,
      contact,
      timeLimit,
      logoUrl,
      homepageUrl,
      documentationUrl,
      endpointUrl,
      minimumNumberOfPlayers,
      maximumNumberOfPlayers,
      minimumNumberOfScreens,
      maximumNumberOfScreens,
      tier,
      role,
      status,
      createdAt,
      updatedAt,
    );

    await this.applicationsGateway.emitToAll(
      [UserRole.ADMIN, UserRole.MODERATOR, UserRole.READ_ONLY],
      WsApplicationEvents.APPLICATION_DELETED,
      new EntityIdDto(id),
    );

    return deletedApplicationDto;
  }

  @Patch(':applicationId/revoke')
  @HttpAuth(HttpJwtGuard)
  @ApiOkResponse(ApplicationApiKeyResponseSpecs)
  @ApiNotFoundResponse(ApplicationNotFoundResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiParam(ApplicationIdApiParameterSpecs)
  @ApiOperation(RevokeApplicationOperationSpecs)
  async revokeApplication(
    @Param('applicationId', ParseUUIDPipe) applicationId: string,
  ): Promise<EntityApiKeyDto> {
    this.logger.info(`applicationId: ${applicationId}`, {
      context: `${ApplicationsController.name}::${this.updateApplication.name}`,
    });

    let applicationToRevoke: ApplicationEntity;

    try {
      applicationToRevoke = await this.applicationsService.getOne({
        id: applicationId,
      });
    } catch (error) {
      throw new NotFoundException();
    }

    applicationToRevoke.apiKey = uuidv4();

    const revokedApplication = await this.applicationsService.updateOne(
      applicationId,
      applicationToRevoke,
    );

    const revokedApplicationDto = new EntityApiKeyDto(
      revokedApplication.apiKey,
    );

    return revokedApplicationDto;
  }
}
