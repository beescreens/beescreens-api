import { Application } from '@beescreens/beescreens';

export type ReadApplication = Omit<Application, 'apiKey'>;
