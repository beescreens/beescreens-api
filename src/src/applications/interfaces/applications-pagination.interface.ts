import { ReadApplication } from './read-application.interface';
import { PaginationMeta, PaginationLinks } from '../../common/interfaces';

export interface ApplicationsPagination {
  readonly items: ReadApplication[];
  readonly meta: PaginationMeta;
  readonly links: PaginationLinks;
}
