import { Application } from '@beescreens/beescreens';

export type CreateApplication = Omit<
  Application,
  'id' | 'apiKey' | 'tier' | 'createdAt' | 'updatedAt'
>;
