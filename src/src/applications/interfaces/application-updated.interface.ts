import { Application } from '@beescreens/beescreens';

export type ApplicationUpdated = Omit<Application, 'apiKey'>;
