import { Application } from '@beescreens/beescreens';

export type UpdateApplication = Omit<
  Application,
  'id' | 'apiKey' | 'tier' | 'createdAt' | 'updatedAt'
>;
