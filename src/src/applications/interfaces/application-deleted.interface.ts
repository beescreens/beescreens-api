import { Application } from '@beescreens/beescreens';

export type ApplicationDeleted = Omit<Application, 'apiKey'>;
