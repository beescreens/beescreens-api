import { Application } from '@beescreens/beescreens';

export type ApplicationCreated = Application;
