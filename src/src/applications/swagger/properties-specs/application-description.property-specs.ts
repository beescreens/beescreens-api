import { ApiPropertyOptions } from '@nestjs/swagger';

export const ApplicationDescriptionPropertySpecs: ApiPropertyOptions = {
  description: 'Description of the application',
  type: 'string',
  minLength: 1,
};
