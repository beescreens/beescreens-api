import { ApiPropertyOptions } from '@nestjs/swagger';

export const ApplicationVersionPropertySpecs: ApiPropertyOptions = {
  description: 'Version of the application',
  type: 'string',
  minLength: 1,
};
