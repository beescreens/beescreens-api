import { ApiPropertyOptions } from '@nestjs/swagger';

export const ApplicationNamePropertySpecs: ApiPropertyOptions = {
  description: 'Name of the application',
  type: 'string',
  minLength: 1,
};
