import { ApiPropertyOptions } from '@nestjs/swagger';

export const ApplicationMaximumNumberOfScreensPropertySpecs: ApiPropertyOptions = {
  description: 'Maximum number of screens to play the application',
  type: 'integer',
  minimum: 1,
};
