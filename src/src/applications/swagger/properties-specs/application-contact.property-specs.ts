import { ApiPropertyOptions } from '@nestjs/swagger';

export const ApplicationContactPropertySpecs: ApiPropertyOptions = {
  description: 'How to contact the developer(s) of the application',
  type: 'string',
  format: 'email',
};
