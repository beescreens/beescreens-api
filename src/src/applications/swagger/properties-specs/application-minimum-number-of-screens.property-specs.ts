import { ApiPropertyOptions } from '@nestjs/swagger';

export const ApplicationMinimumNumberOfScreensPropertySpecs: ApiPropertyOptions = {
  description: 'Minimum number of screens to play the application',
  type: 'integer',
  minimum: 1,
};
