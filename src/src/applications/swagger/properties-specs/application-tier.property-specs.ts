import { ApiPropertyOptions } from '@nestjs/swagger';
import { ApplicationTier } from '@beescreens/beescreens';

export const ApplicationTierPropertySpecs: ApiPropertyOptions = {
  description: 'Tier of the application',
  type: 'string',
  enum: ApplicationTier,
};
