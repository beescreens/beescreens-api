import { ApiPropertyOptions } from '@nestjs/swagger';

export const ApplicationLogoUrlPropertySpecs: ApiPropertyOptions = {
  description: 'The logo URL of the application',
  type: 'string',
  format: 'uri',
  minLength: 1,
};
