import { ApiPropertyOptions } from '@nestjs/swagger';
import { ApplicationStatus } from '@beescreens/beescreens';

export const ApplicationStatusPropertySpecs: ApiPropertyOptions = {
  description: 'Status of the application',
  type: 'string',
  enum: ApplicationStatus,
};
