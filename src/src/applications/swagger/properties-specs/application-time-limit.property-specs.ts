import { ApiPropertyOptions } from '@nestjs/swagger';

export const ApplicationTimeLimitPropertySpecs: ApiPropertyOptions = {
  description: 'Time limit allowed for the application (in seconds)',
  type: 'integer',
  minimum: 1,
};
