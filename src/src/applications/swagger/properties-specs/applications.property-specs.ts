import { ApiPropertyOptions } from '@nestjs/swagger';
import { ReadApplicationDto } from '../../dto';

export const ApplicationsPropertySpecs: ApiPropertyOptions = {
  description: 'The retrieved applications.',
  type: () => [ReadApplicationDto],
};
