import { ApiPropertyOptions } from '@nestjs/swagger';

export const ApplicationDocumentationUrlPropertySpecs: ApiPropertyOptions = {
  description: 'The documentation URL of the application',
  type: 'string',
  format: 'uri',
};
