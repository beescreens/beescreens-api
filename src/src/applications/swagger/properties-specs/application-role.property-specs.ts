import { ApiPropertyOptions } from '@nestjs/swagger';
import { ApplicationRole } from '@beescreens/beescreens';

export const ApplicationRolePropertySpecs: ApiPropertyOptions = {
  description: 'Role of the application',
  type: 'string',
  enum: ApplicationRole,
};
