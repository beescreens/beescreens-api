import { ApiPropertyOptions } from '@nestjs/swagger';

export const ApplicationHomepageUrlPropertySpecs: ApiPropertyOptions = {
  description: 'The homepage URL of the application',
  type: 'string',
  format: 'uri',
  minLength: 1,
};
