import { ApiPropertyOptions } from '@nestjs/swagger';

export const ApplicationMinimumNumberOfPlayersPropertySpecs: ApiPropertyOptions = {
  description: 'Minimum number of players to play the application',
  type: 'integer',
  minimum: 1,
};
