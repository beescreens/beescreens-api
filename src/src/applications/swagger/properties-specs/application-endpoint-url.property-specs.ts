import { ApiPropertyOptions } from '@nestjs/swagger';

export const ApplicationEndpointUrlPropertySpecs: ApiPropertyOptions = {
  description: 'The endpoint URL of the application',
  type: 'string',
  format: 'uri',
};
