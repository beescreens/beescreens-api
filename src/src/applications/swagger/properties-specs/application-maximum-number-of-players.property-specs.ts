import { ApiPropertyOptions } from '@nestjs/swagger';

export const ApplicationMaximumNumberOfPlayersPropertySpecs: ApiPropertyOptions = {
  description: 'Maximum number of players to play the application',
  type: 'integer',
  minimum: 1,
};
