import { ApiResponseOptions } from '@nestjs/swagger';
import { ReadApplicationDto } from '../../dto';

export const ApplicationRetrievedResponseSpecs: ApiResponseOptions = {
  description: 'Application has been successfully retrieved.',
  type: ReadApplicationDto,
};
