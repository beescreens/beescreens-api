import { ApiResponseOptions } from '@nestjs/swagger';
import { ApplicationsPaginationDto } from '../../dto';

export const ApplicationsRetrievedResponseSpecs: ApiResponseOptions = {
  description: 'Applications have been successfully retrieved.',
  type: ApplicationsPaginationDto,
};
