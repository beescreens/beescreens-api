import { ApiResponseOptions } from '@nestjs/swagger';
import { ApplicationDeletedDto } from '../../dto';

export const ApplicationDeletedResponseSpecs: ApiResponseOptions = {
  description: 'Application has been successfully deleted.',
  type: ApplicationDeletedDto,
};
