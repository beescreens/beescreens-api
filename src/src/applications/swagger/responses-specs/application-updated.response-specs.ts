import { ApiResponseOptions } from '@nestjs/swagger';
import { ApplicationUpdatedDto } from '../../dto';

export const ApplicationUpdatedResponseSpecs: ApiResponseOptions = {
  description: 'Application has been successfully updated.',
  type: ApplicationUpdatedDto,
};
