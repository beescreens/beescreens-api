import { ApiResponseOptions } from '@nestjs/swagger';
import { CreateApplicationWithOtherNameLinkSpecs } from '../links-specs';

export const ApplicationConflictResponseSpecs: ApiResponseOptions = {
  description:
    'Another application has the same name. Please try again with another applicationname.',
  links: {
    AddApplicationWithOtherName: CreateApplicationWithOtherNameLinkSpecs,
  },
};
