import { ApiResponseOptions } from '@nestjs/swagger';
import { EntityApiKeyDto } from '../../../common/dto';
import { JoinAsApplicationLinkSpecs } from '../links-specs';

export const ApplicationApiKeyResponseSpecs: ApiResponseOptions = {
  description:
    'The Application API key has been successfully revoked. The API key is only displayed once.',
  type: EntityApiKeyDto,
  links: {
    JoinAsApplication: JoinAsApplicationLinkSpecs,
  },
};
