import { ApiResponseOptions } from '@nestjs/swagger';

export const ApplicationNotFoundResponseSpecs: ApiResponseOptions = {
  description: 'Application has not been found.',
};
