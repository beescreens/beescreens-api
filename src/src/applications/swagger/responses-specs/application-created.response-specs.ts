import { ApiResponseOptions } from '@nestjs/swagger';
import { ApplicationCreatedDto } from '../../dto';
import { ApplicationsLinksApiCommon } from '../common';

export const ApplicationCreatedResponseSpecs: ApiResponseOptions = {
  description:
    'Application has been successfully created. The `apiKey` is only displayed once.',
  type: ApplicationCreatedDto,
  links: ApplicationsLinksApiCommon,
};
