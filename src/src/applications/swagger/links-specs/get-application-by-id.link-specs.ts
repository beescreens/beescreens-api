import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const GetApplicationByIdLinkSpecs: LinkObject = {
  description:
    'The `id` value returned in the response can be used as the `applicationId` parameter in `GET /applications/{applicationId}`.',
  operationId: 'GET /applications/{applicationId}',
  parameters: { applicationId: '$response.body#/id' },
};
