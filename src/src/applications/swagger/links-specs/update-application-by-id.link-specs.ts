import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const UpdateApplicationByIdLinkSpecs: LinkObject = {
  description:
    'The `id` value returned in the response can be used as the `applicationId` parameter in `PATCH /applications/{applicationId}`.',
  operationId: 'PATCH /applications/{applicationId}',
  parameters: { applicationId: '$response.body#/id' },
};
