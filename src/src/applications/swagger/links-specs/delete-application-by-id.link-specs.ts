import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const DeleteApplicationByIdLinkSpecs: LinkObject = {
  description:
    'The `id` value returned in the response can be used as the `applicationId` parameter in `DELETE /applications/{applicationId}`.',
  operationId: 'DELETE /applications/{applicationId}',
  parameters: { applicationId: '$response.body#/id' },
};
