export * from './create-application-with-other-name.link-specs';
export * from './delete-application-by-id.link-specs';
export * from './get-application-by-id.link-specs';
export * from './join-as-application.link-specs';
export * from './revoke-application-by-id.link-specs';
export * from './update-application-by-id.link-specs';
