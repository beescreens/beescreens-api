import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const RevokeApplicationByIdLinkSpecs: LinkObject = {
  description:
    'The `id` value returned in the response can be used as the `applicationId` parameter in `PATCH /applications/{applicationId}/revoke`.',
  operationId: 'PATCH /applications/{applicationId}/revoke',
  parameters: { applicationId: '$response.body#/id' },
};
