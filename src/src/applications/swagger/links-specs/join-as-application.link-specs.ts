import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const JoinAsApplicationLinkSpecs: LinkObject = {
  description:
    'The `apiKey` value returned in the response can be used to join the WebSockets server in `emitJoinAsApplication`.',
  operationId: 'emitJoinAsApplication',
  parameters: { apiKey: '$response.body#/apiKey' },
};
