import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const CreateApplicationWithOtherNameLinkSpecs: LinkObject = {
  description: 'The `name` must be changed in `POST /applications`.',
  operationId: 'POST /applications',
  requestBody: {
    name: '$response.body#/name',
    location: '$response.body#/location',
    role: '$response.body#/role',
    status: '$response.body#/status',
  },
};
