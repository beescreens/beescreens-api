import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const OnApplicationDeletedCallbackSpecs: PathItemObject = {
  get: {
    summary: 'A deleted application has been emitted by the WebSocket server',
    description:
      'A deleted application has been emitted by the WebSocket server.',
    operationId: 'onApplicationDeleted',
    responses: {
      'N/A': {
        description:
          'A deleted application has been emitted by the WebSocket server.',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: {
                  description: 'ID of the entity',
                  type: 'string',
                  format: 'uuid',
                },
              },
            },
          },
        },
      },
    },
  },
};
