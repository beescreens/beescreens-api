import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { JoinAsUserLinkSpecs } from '../../../common/swagger/links-specs';
import { JoinAsApplicationLinkSpecs } from '../links-specs';

export const OnConnectApplicationCallbackSpecs: PathItemObject = {
  get: {
    summary: 'The connection to the server has been established',
    description: 'The connection to the server has been established.',
    operationId: 'onConnectApplication',
    responses: {
      'N/A': {
        description: 'The connection to the server has been established.',
        links: {
          joinAsUser: JoinAsUserLinkSpecs,
          joinAsApplication: JoinAsApplicationLinkSpecs,
        },
      },
    },
  },
};
