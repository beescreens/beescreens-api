import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ApplicationsLinksApiCommon } from '../common';

export const OnApplicationCreatedCallbackSpecs: PathItemObject = {
  get: {
    summary: 'A new application has been emitted by the WebSocket server',
    description: 'A new application has been emitted by the WebSocket server.',
    operationId: 'onApplicationCreated',
    responses: {
      'N/A': {
        description:
          'A new application has been emitted by the WebSocket server.',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: {
                  description: 'ID of the entity',
                  type: 'string',
                  format: 'uuid',
                },
              },
            },
          },
        },
        links: ApplicationsLinksApiCommon,
      },
    },
  },
};
