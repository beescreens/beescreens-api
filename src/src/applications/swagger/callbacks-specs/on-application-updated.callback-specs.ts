import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const OnApplicationUpdatedCallbackSpecs: PathItemObject = {
  get: {
    summary: 'An updated application has been emitted by the WebSocket server',
    description:
      'An updated application has been emitted by the WebSocket server.',
    operationId: 'onApplicationUpdated',
    responses: {
      'N/A': {
        description:
          'An updated application has been emitted by the WebSocket server.',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: {
                  description: 'ID of the entity',
                  type: 'string',
                  format: 'uuid',
                },
              },
            },
          },
        },
      },
    },
  },
};
