import { ApiBodyOptions } from '@nestjs/swagger';
import { CreateApplicationDto } from '../../dto';

export const CreateApplicationBodySpecs: ApiBodyOptions = {
  description: "The application's details.",
  type: CreateApplicationDto,
};
