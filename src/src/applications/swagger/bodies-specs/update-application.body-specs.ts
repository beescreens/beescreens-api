import { ApiBodyOptions } from '@nestjs/swagger';
import { UpdateApplicationDto } from '../../dto';

export const UpdateApplicationBodySpecs: ApiBodyOptions = {
  description: "The application's details.",
  type: UpdateApplicationDto,
};
