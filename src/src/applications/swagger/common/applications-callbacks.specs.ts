import {
  PathItemObject,
  ReferenceObject,
} from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import {
  OnJoinedCallbackSpecs,
  OnExceptionCallbackSpecs,
  OnDisconnectCallbackSpecs,
} from '../../../common/swagger/callbacks-specs';
import { OnMessageCallbackSpecs } from '../../../messages/swagger/callbacks-specs';
import {
  OnConnectApplicationCallbackSpecs,
  OnApplicationCreatedCallbackSpecs,
  OnApplicationUpdatedCallbackSpecs,
  OnApplicationDeletedCallbackSpecs,
  EmitJoinAsApplicationCallbackSpecs,
} from '../callbacks-specs';
import { EmitJoinAsUserCallbackSpecs } from '../../../users/swagger/callbacks-specs';

export const ApplicationsCallbacksApiCommon: Record<
  string,
  ReferenceObject | Record<string, PathItemObject>
> = {
  'WebSockets (common)': {
    onConnect: OnConnectApplicationCallbackSpecs,
    onJoined: OnJoinedCallbackSpecs,
    onException: OnExceptionCallbackSpecs,
    onDisconnect: OnDisconnectCallbackSpecs,
    onMessage: OnMessageCallbackSpecs,
  },
  'WebSockets (as user)': {
    emitJoinAsUser: EmitJoinAsUserCallbackSpecs,
    onApplicationCreated: OnApplicationCreatedCallbackSpecs,
    onApplicationUpdated: OnApplicationUpdatedCallbackSpecs,
    onApplicationDeleted: OnApplicationDeletedCallbackSpecs,
  },
  'WebSockets (as application)': {
    emitJoinAsApplication: EmitJoinAsApplicationCallbackSpecs,
  },
};
