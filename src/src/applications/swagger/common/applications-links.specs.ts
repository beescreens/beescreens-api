import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import {
  GetApplicationByIdLinkSpecs,
  DeleteApplicationByIdLinkSpecs,
  UpdateApplicationByIdLinkSpecs,
  RevokeApplicationByIdLinkSpecs,
} from '../links-specs';

export const ApplicationsLinksApiCommon: Record<string, LinkObject> = {
  GetApplicationById: GetApplicationByIdLinkSpecs,
  DeleteApplicationById: DeleteApplicationByIdLinkSpecs,
  UpdateApplicationById: UpdateApplicationByIdLinkSpecs,
  RevokeApplicationById: RevokeApplicationByIdLinkSpecs,
};
