import { ApiParamOptions } from '@nestjs/swagger';

export const ApplicationIdApiParameterSpecs: ApiParamOptions = {
  name: 'applicationId',
  description: 'The application ID.',
};
