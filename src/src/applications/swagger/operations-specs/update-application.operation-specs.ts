import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ApplicationsCallbacksApiCommon } from '../common';

export const UpdateApplicationOperationSpecs: Partial<OperationObject> = {
  summary: 'Update the specified application',
  description: 'Update the specified application.',
  operationId: 'updateApplication',
  callbacks: ApplicationsCallbacksApiCommon,
};
