import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ApplicationsCallbacksApiCommon } from '../common';

export const DeleteApplicationOperationSpecs: Partial<OperationObject> = {
  summary: 'Delete the specified application',
  description: 'Delete the specified application.',
  operationId: 'deleteApplication',
  callbacks: ApplicationsCallbacksApiCommon,
};
