import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ApplicationsCallbacksApiCommon } from '../common';

export const RevokeApplicationOperationSpecs: Partial<OperationObject> = {
  summary: 'Revoke the specified application',
  description: 'Revoke the specified application.',
  operationId: 'revokeApplication',
  callbacks: ApplicationsCallbacksApiCommon,
};
