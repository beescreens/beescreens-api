import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ApplicationsCallbacksApiCommon } from '../common';

export const CreateApplicationOperationSpecs: Partial<OperationObject> = {
  summary: 'Create a new application',
  description: 'Create a new application.',
  operationId: 'createApplication',
  callbacks: ApplicationsCallbacksApiCommon,
};
