import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ApplicationsCallbacksApiCommon } from '../common';

export const GetApplicationOperationSpecs: Partial<OperationObject> = {
  summary: 'Get the specified application',
  description: 'Get the specified application.',
  operationId: 'getApplication',
  callbacks: ApplicationsCallbacksApiCommon,
};
