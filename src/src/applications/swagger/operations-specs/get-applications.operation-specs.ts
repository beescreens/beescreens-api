import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ApplicationsCallbacksApiCommon } from '../common';

export const GetApplicationsOperationSpecs: Partial<OperationObject> = {
  summary: 'Get the applications',
  description: 'Get the applications.',
  operationId: 'getApplications',
  callbacks: ApplicationsCallbacksApiCommon,
};
