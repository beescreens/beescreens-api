import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsEnum, MinLength } from 'class-validator';
import { UserRole, UserStatus } from '@beescreens/beescreens';
import {
  UserUsernamePropertySpecs,
  UserPasswordPropertySpecs,
  UserRolePropertySpecs,
  UserStatusPropertySpecs,
} from '../swagger/properties-specs';
import { CreateUser } from '../interfaces';

export class CreateUserDto implements CreateUser {
  @ApiProperty(UserUsernamePropertySpecs)
  @IsString()
  @MinLength(1)
  readonly username: string;

  @ApiProperty(UserPasswordPropertySpecs)
  @IsString()
  @MinLength(8)
  readonly password: string;

  @ApiProperty(UserRolePropertySpecs)
  @IsEnum(UserRole)
  readonly role: UserRole;

  @ApiProperty(UserStatusPropertySpecs)
  @IsEnum(UserStatus)
  readonly status: UserStatus;

  constructor(
    username: string,
    password: string,
    role: UserRole,
    status: UserStatus,
  ) {
    this.username = username;
    this.password = password;
    this.role = role;
    this.status = status;
  }
}
