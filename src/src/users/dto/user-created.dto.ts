import { ApiProperty } from '@nestjs/swagger';
import { IsString, Length, IsEnum, IsDate, IsUUID } from 'class-validator';
import { UserRole, UserStatus } from '@beescreens/beescreens';
import {
  EntityIdPropertySpecs,
  EntityCreatedAtPropertySpecs,
  EntityUpdatedAtPropertySpecs,
} from '../../common/swagger/properties-specs';
import { UserCreated } from '../interfaces';
import {
  UserUsernamePropertySpecs,
  UserRolePropertySpecs,
  UserStatusPropertySpecs,
} from '../swagger/properties-specs';

export class UserCreatedDto implements UserCreated {
  @ApiProperty(EntityIdPropertySpecs)
  @IsUUID()
  readonly id: string;

  @ApiProperty(UserUsernamePropertySpecs)
  @Length(4, 12)
  @IsString()
  readonly username: string;

  @ApiProperty(UserRolePropertySpecs)
  @IsEnum(UserRole)
  readonly role: UserRole;

  @ApiProperty(UserStatusPropertySpecs)
  @IsEnum(UserStatus)
  readonly status: UserStatus;

  @ApiProperty(EntityCreatedAtPropertySpecs)
  @IsDate()
  readonly createdAt: Date;

  @ApiProperty(EntityUpdatedAtPropertySpecs)
  @IsDate()
  readonly updatedAt: Date;

  constructor(
    id: string,
    username: string,
    role: UserRole,
    status: UserStatus,
    createdAt: Date,
    updatedAt: Date,
  ) {
    this.id = id;
    this.username = username;
    this.role = role;
    this.status = status;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
