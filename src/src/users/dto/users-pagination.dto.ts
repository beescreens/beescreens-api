import { ApiProperty } from '@nestjs/swagger';
import { IsArray } from 'class-validator';
import { PaginationMetaDto, PaginationLinksDto } from '../../common/dto';
import { UsersPagination } from '../interfaces';
import { ReadUserDto } from './read-user.dto';
import { UsersPropertySpecs } from '../swagger/properties-specs';

export class UsersPaginationDto implements UsersPagination {
  @ApiProperty(UsersPropertySpecs)
  @IsArray()
  readonly items: ReadUserDto[];

  @ApiProperty()
  readonly meta: PaginationMetaDto;

  @ApiProperty()
  readonly links: PaginationLinksDto;

  constructor(
    items: ReadUserDto[],
    meta: PaginationMetaDto,
    links: PaginationLinksDto,
  ) {
    this.items = items;
    this.meta = meta;
    this.links = links;
  }
}
