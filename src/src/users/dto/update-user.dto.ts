import { ApiProperty } from '@nestjs/swagger';
import { Length, IsString, MinLength, IsEnum } from 'class-validator';
import { UserRole, UserStatus } from '@beescreens/beescreens';
import { UpdateUser } from '../interfaces';
import {
  UserUsernamePropertySpecs,
  UserPasswordPropertySpecs,
  UserRolePropertySpecs,
  UserStatusPropertySpecs,
} from '../swagger/properties-specs';

export class UpdateUserDto implements UpdateUser {
  @ApiProperty(UserUsernamePropertySpecs)
  @Length(4, 12)
  @IsString()
  readonly username: string;

  @ApiProperty(UserPasswordPropertySpecs)
  @IsString()
  @MinLength(8)
  readonly password: string;

  @ApiProperty(UserRolePropertySpecs)
  @IsEnum(UserRole)
  readonly role: UserRole;

  @ApiProperty(UserStatusPropertySpecs)
  @IsEnum(UserStatus)
  readonly status: UserStatus;

  constructor(
    username: string,
    password: string,
    role: UserRole,
    status: UserStatus,
  ) {
    this.username = username;
    this.password = password;
    this.role = role;
    this.status = status;
  }
}
