import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Patch,
  Inject,
  ConflictException,
  NotFoundException,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  Query,
  BadRequestException,
  ParseUUIDPipe,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBody,
  ApiOperation,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiConflictResponse,
  ApiParam,
  ApiBadRequestResponse,
  ApiQuery,
} from '@nestjs/swagger';
import { Logger } from 'winston';
import { v4 as uuidv4 } from 'uuid';
import { hash } from 'argon2';
import { Pagination } from 'nestjs-typeorm-paginate';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { UserRole, WsUserEvents } from '@beescreens/beescreens';
import { HttpJwtGuard } from '../auth/guards';
import {
  MissingOrIncorrectFieldsResponseSpecs,
  MissingOrIncorrectParamsResponseSpecs,
} from '../common/swagger/responses-specs';
import { UsersService } from './users.service';
import {
  UsersRetrievedResponseSpecs,
  UserCreatedResponseSpecs,
  UserConflictResponseSpecs,
  UserRetrievedResponseSpecs,
  UserNotFoundResponseSpecs,
  UserDeletedResponseSpecs,
  UserUpdatedResponseSpecs,
} from './swagger/responses-specs';
import {
  GetUsersOperationSpecs,
  CreateUserOperationSpecs,
  GetUserOperationSpecs,
  DeleteUserOperationSpecs,
  UpdateUserOperationSpecs,
} from './swagger/operations-specs';
import {
  ReadUserDto,
  CreateUserDto,
  UpdateUserDto,
  UserDeletedDto,
  UserCreatedDto,
  UserUpdatedDto,
  UsersPaginationDto,
} from './dto';
import {
  CreateUserBodySpecs,
  UpdateUserBodySpecs,
} from './swagger/bodies-specs';
import { UserIdApiParameterSpecs } from './swagger/parameters-specs';
import {
  PaginationPageQuerySpecs,
  PaginationLimitQuerySpecs,
} from '../common/swagger/queries-specs';
import { UserEntity } from './entities';

import { UsersGateway } from './users.gateway';
import {
  PaginationMetaDto,
  PaginationLinksDto,
  EntityIdDto,
} from '../common/dto';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DefaultIntValuePipe } from '../common/pipes';
import { HttpAuth } from '../common/decorators';

@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) readonly logger: Logger,
    readonly usersService: UsersService,
    readonly usersGateway: UsersGateway,
  ) {}

  @Get()
  @HttpAuth(HttpJwtGuard)
  @ApiQuery(PaginationPageQuerySpecs)
  @ApiQuery(PaginationLimitQuerySpecs)
  @ApiOkResponse(UsersRetrievedResponseSpecs)
  @ApiOperation(GetUsersOperationSpecs)
  async getUsers(
    @Query('page', new DefaultIntValuePipe(1)) page = 1,
    @Query('limit', new DefaultIntValuePipe(10)) limit = 10,
  ): Promise<UsersPaginationDto> {
    this.logger.info(`page: ${page}, limit: ${limit}`, {
      context: `${UsersController.name}::${this.getUsers.name}`,
    });

    const query = this.usersService.repository
      .createQueryBuilder('u')
      .orderBy('u.username', 'ASC');

    let pagination: Pagination<UserEntity>;

    try {
      pagination = await this.usersService.getPagination(
        '/users',
        page,
        limit,
        query,
      );
    } catch (error) {
      throw new BadRequestException();
    }

    const users = pagination.items.map(
      async (user) =>
        new ReadUserDto(
          user.id,
          user.username,
          user.role,
          user.status,
          user.createdAt,
          user.updatedAt,
        ),
    );

    const { meta, links } = pagination;

    return new UsersPaginationDto(
      await Promise.all(users),
      new PaginationMetaDto(
        meta.currentPage,
        meta.itemCount,
        meta.itemsPerPage,
        meta.totalItems,
        meta.totalPages,
      ),
      new PaginationLinksDto(
        links.first || '',
        links.last || '',
        links.next || '',
        links.previous || '',
      ),
    );
  }

  @Post()
  @HttpAuth(HttpJwtGuard)
  @ApiCreatedResponse(UserCreatedResponseSpecs)
  @ApiConflictResponse(UserConflictResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiOperation(CreateUserOperationSpecs)
  @ApiBody(CreateUserBodySpecs)
  async createUser(@Body() userDto: CreateUserDto): Promise<UserCreatedDto> {
    this.logger.info(`body: ${JSON.stringify(userDto)}`, {
      context: `${UsersController.name}::${this.createUser.name}`,
    });

    let newUser: UserEntity;

    try {
      const user = new UserEntity(
        uuidv4(),
        userDto.username,
        userDto.password,
        userDto.role,
        userDto.status,
      );

      user.password = await hash(userDto.password);

      newUser = await this.usersService.createOne(user);
    } catch (error) {
      throw new ConflictException();
    }

    const { id, username, role, status, createdAt, updatedAt } = newUser;

    const newUserDto = new UserCreatedDto(
      id,
      username,
      role,
      status,
      createdAt,
      updatedAt,
    );

    await this.usersGateway.emitToAll(
      [UserRole.ADMIN, UserRole.MODERATOR, UserRole.READ_ONLY],
      WsUserEvents.USER_CREATED,
      new EntityIdDto(id),
    );

    return newUserDto;
  }

  @Get(':userId')
  @HttpAuth(HttpJwtGuard)
  @ApiParam(UserIdApiParameterSpecs)
  @ApiOkResponse(UserRetrievedResponseSpecs)
  @ApiNotFoundResponse(UserNotFoundResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectParamsResponseSpecs)
  @ApiOperation(GetUserOperationSpecs)
  async getUser(
    @Param('userId', ParseUUIDPipe) userId: string,
  ): Promise<ReadUserDto> {
    this.logger.info(`userId: ${userId}`, {
      context: `${UsersController.name}::${this.getUser.name}`,
    });

    let foundUser: UserEntity;

    try {
      foundUser = await this.usersService.getOne({ id: userId });
    } catch (error) {
      throw new NotFoundException();
    }

    const { id, username, role, status, createdAt, updatedAt } = foundUser;

    return new ReadUserDto(id, username, role, status, createdAt, updatedAt);
  }

  @Patch(':userId')
  @HttpAuth(HttpJwtGuard)
  @ApiOkResponse(UserUpdatedResponseSpecs)
  @ApiNotFoundResponse(UserNotFoundResponseSpecs)
  @ApiConflictResponse(UserConflictResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiParam(UserIdApiParameterSpecs)
  @ApiOperation(UpdateUserOperationSpecs)
  @ApiBody(UpdateUserBodySpecs)
  async updateUser(
    @Param('userId', ParseUUIDPipe) userId: string,
    @Body() userDto: UpdateUserDto,
  ): Promise<UserUpdatedDto> {
    this.logger.info(`userId: ${userId}, body: ${JSON.stringify(userDto)}`, {
      context: `${UsersController.name}::${this.updateUser.name}`,
    });

    let userToUpdate: UserEntity;

    try {
      userToUpdate = await this.usersService.getOne({ id: userId });
    } catch (error) {
      throw new NotFoundException();
    }

    const { username, password, role, status } = userDto;

    userToUpdate.username = username;
    userToUpdate.password = await hash(password);
    userToUpdate.role = role;
    userToUpdate.status = status;

    const updatedUser = await this.usersService.updateOne(userId, userToUpdate);

    const updatedUserDto = new UserUpdatedDto(
      updatedUser.id,
      updatedUser.username,
      updatedUser.role,
      updatedUser.status,
      updatedUser.createdAt,
      updatedUser.updatedAt,
    );

    await this.usersGateway.emitToAll(
      [UserRole.ADMIN, UserRole.MODERATOR, UserRole.READ_ONLY],
      WsUserEvents.USER_UPDATED,
      new EntityIdDto(userId),
    );

    return updatedUserDto;
  }

  @Delete(':userId')
  @HttpAuth(HttpJwtGuard)
  @ApiOkResponse(UserDeletedResponseSpecs)
  @ApiNotFoundResponse(UserNotFoundResponseSpecs)
  @ApiParam(UserIdApiParameterSpecs)
  @ApiOperation(DeleteUserOperationSpecs)
  async deleteUser(
    @Param('userId', ParseUUIDPipe) userId: string,
  ): Promise<UserDeletedDto> {
    this.logger.info(`userId: ${userId}`, {
      context: `${UsersController.name}::${this.deleteUser.name}`,
    });

    let deletedUser: UserEntity;

    try {
      deletedUser = await this.usersService.deleteOne({ id: userId });
      deletedUser.id = userId;
    } catch (error) {
      throw new NotFoundException();
    }

    const { id, username, role, status, createdAt, updatedAt } = deletedUser;

    const deletedUserDto = new UserDeletedDto(
      id,
      username,
      role,
      status,
      createdAt,
      updatedAt,
    );

    try {
      await this.usersGateway.leave(deletedUser.id);
    } catch (error) {
      // Do nothing
    }

    await this.usersGateway.emitToAll(
      [UserRole.ADMIN, UserRole.MODERATOR, UserRole.READ_ONLY],
      WsUserEvents.USER_DELETED,
      new EntityIdDto(id),
    );

    return deletedUserDto;
  }
}
