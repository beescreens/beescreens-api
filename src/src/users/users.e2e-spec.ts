import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import request from 'supertest';
import { ScheduleModule } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { transports } from 'winston';
import {
  UserRole,
  UserStatus,
  WsCommonEvents,
  WsUserEvents,
} from '@beescreens/beescreens';
import { connect } from 'socket.io-client';
import compression from 'compression';
import helmet from 'helmet';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { MessagesModule } from '../messages/messages.module';
import { UsersModule } from './users.module';
import { AuthModule } from '../auth/auth.module';
import {
  ReadUserDto,
  CreateUserDto,
  UpdateUserDto,
  UsersPaginationDto,
  UserCreatedDto,
  UserUpdatedDto,
  UserDeletedDto,
} from './dto';
import { PaginationMetaDto, PaginationLinksDto } from '../common/dto';
import { DisplayersModule } from '../displayers/displayers.module';
import { ApplicationsModule } from '../applications/applications.module';
import { selectRandomEnum } from '../helpers';

describe('Users (e2e)', () => {
  let app: INestApplication;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let server: any;
  let serverAddress: string;
  let configService: ConfigService;
  let jwt: string;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ScheduleModule.forRoot(),
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
        AuthModule,
        MessagesModule,
        UsersModule,
        ApplicationsModule,
        DisplayersModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    app.use(compression());

    app.use(helmet());

    app.enableCors();

    app.useGlobalPipes(
      new ValidationPipe({
        transform: true,
      }),
    );

    app.enableShutdownHooks();

    server = app.getHttpServer();
    configService = app.get<ConfigService>(ConfigService);

    const { address, port } = server.listen().address();

    serverAddress = `http://[${address}]:${port}`;

    await app.init();

    const res = await request(server)
      .post('/login')
      .send({
        username: configService.get<string>('DEFAULT_USERNAME'),
        password: configService.get<string>('DEFAULT_PASSWORD'),
      });

    jwt = res.body.jwt;
  });

  afterEach(async () => {
    await app.close();
  });

  it('cannot access to /users (GET) without a JWT', () => {
    return request(server).get('/users').expect(401);
  });

  it('cannot access to /users (POST) without a JWT', () => {
    return request(server).post('/users').expect(401);
  });

  it('should be able to add the new user on /users (POST)', async (done) => {
    const newUser = new CreateUserDto(
      'newUser',
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const socket = connect(`${serverAddress}/users`, {
      forceNew: true,
      query: { jwt },
    });

    socket.on(WsUserEvents.USER_CREATED, (createdUser: UserCreatedDto) => {
      expect(createdUser.id).toBeDefined();

      socket.close();

      done();
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      const res = await request(server)
        .post('/users')
        .set('Authorization', `Bearer ${jwt}`)
        .send(newUser);

      expect(res.status).toEqual(201);

      expect(res.body.id).toBeDefined();
      expect(res.body.username).toEqual(newUser.username);
      expect(res.body).not.toHaveProperty('password');
      expect(res.body.role).toEqual(newUser.role);
      expect(res.body.status).toEqual(newUser.status);
      expect(res.body.createdAt).toBeDefined();
      expect(res.body.updatedAt).toBeDefined();
    });
  });

  it('adding a new user on /users (POST) with the same username should return a 409 error', async () => {
    const newUser = new CreateUserDto(
      'newUser',
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    await request(server)
      .post('/users')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newUser);

    return request(server)
      .post('/users')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newUser)
      .expect(409);
  });

  it('adding a new user with malformed entries should return an 400 error', async () => {
    const res = await request(server)
      .post('/users')
      .set('Authorization', `Bearer ${jwt}`)
      .send(new CreateUserDto('', '', UserRole.DEVELOPER, UserStatus.ENABLED))
      .expect(400);

    expect(res.body).toEqual({
      statusCode: 400,
      message: [
        'username must be longer than or equal to 1 characters',
        'password must be longer than or equal to 8 characters',
      ],
      error: 'Bad Request',
    });
  });

  it('should be able to get the new user and have all the properties on /users/{userId} (GET)', async () => {
    const newUser = new CreateUserDto(
      'newUser',
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const resCreatedUser = await request(server)
      .post('/users')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newUser);

    const resFoundUser = await request(server)
      .get(`/users/${resCreatedUser.body.id}`)
      .set('Authorization', `Bearer ${jwt}`)
      .expect(200);

    expect(resFoundUser.body.id).toEqual(resCreatedUser.body.id);
    expect(resFoundUser.body.username).toEqual(resCreatedUser.body.username);
    expect(resFoundUser.body).not.toHaveProperty('password');
    expect(resFoundUser.body.role).toEqual(resCreatedUser.body.role);
    expect(resFoundUser.body.status).toEqual(resCreatedUser.body.status);
    expect(resFoundUser.body.createdAt).toEqual(resCreatedUser.body.createdAt);
    expect(resFoundUser.body.updatedAt).toEqual(resCreatedUser.body.updatedAt);
  });

  it('should return a 400 error when trying to get an user with a malformed UUID on /users/{userId} (GET)', async () => {
    return request(server)
      .get('/users/notuuid')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('should return a 404 error when trying to get an undefined user on /users/{userId} (GET)', async () => {
    return request(server)
      .get('/users/00000000-0000-0000-0000-000000000000')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(404);
  });

  it('should be able to update the user on /users/{userId} (PATCH)', async (done) => {
    const newUser = new CreateUserDto(
      'newUser',
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const updateUser = new UpdateUserDto(
      'updatedUser',
      'passwordForUpdatedUser',
      UserRole.DEVELOPER,
      UserStatus.DISABLED,
    );

    const socket = connect(`${serverAddress}/users`, {
      forceNew: true,
      query: { jwt },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      const resCreatedUser = await request(server)
        .post('/users')
        .set('Authorization', `Bearer ${jwt}`)
        .send(newUser);

      socket.on(WsUserEvents.USER_UPDATED, (updatedUser: UserUpdatedDto) => {
        expect(updatedUser.id).toEqual(resCreatedUser.body.id);

        socket.close();

        done();
      });

      const resUpdatedUser = await request(server)
        .patch(`/users/${resCreatedUser.body.id}`)
        .set('Authorization', `Bearer ${jwt}`)
        .send(updateUser)
        .expect(200);

      expect(resUpdatedUser.body.id).toEqual(resCreatedUser.body.id);
      expect(resUpdatedUser.body.username).toEqual(updateUser.username);
      expect(resUpdatedUser.body).not.toHaveProperty('password');
      expect(resUpdatedUser.body.role).toEqual(updateUser.role);
      expect(resUpdatedUser.body.status).toEqual(updateUser.status);
      expect(resUpdatedUser.body.createdAt).toEqual(
        resCreatedUser.body.createdAt,
      );
      expect(resUpdatedUser.body.updatedAt).toBeDefined();
    });
  });

  it('should return a 400 error when trying to update an user with a malformed UUID on /users/{userId} (PATCH)', async () => {
    return request(server)
      .patch('/users/notuuid')
      .set('Authorization', `Bearer ${jwt}`)
      .send(
        new UpdateUserDto(
          'updatedUser',
          'passwordForUpdatedUser',
          UserRole.DEVELOPER,
          UserStatus.DISABLED,
        ),
      )
      .expect(400);
  });

  it('should return a 404 error when trying to update an undefined user on /users/{userId} (PATCH)', async () => {
    return request(server)
      .patch('/users/00000000-0000-0000-0000-000000000000')
      .set('Authorization', `Bearer ${jwt}`)
      .send(
        new UpdateUserDto(
          'updatedUser',
          'passwordForUpdatedUser',
          UserRole.DEVELOPER,
          UserStatus.DISABLED,
        ),
      )
      .expect(404);
  });

  it('should be able to delete the user on /users/{userId} (DELETE)', async (done) => {
    const newUser = new CreateUserDto(
      'newUser',
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const socket = connect(`${serverAddress}/users`, {
      forceNew: true,
      query: { jwt },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      const resCreatedUser = await request(server)
        .post('/users')
        .set('Authorization', `Bearer ${jwt}`)
        .send(newUser);

      socket.on(WsUserEvents.USER_DELETED, (deletedUser: UserDeletedDto) => {
        expect(deletedUser.id).toEqual(resCreatedUser.body.id);

        socket.close();

        done();
      });

      const resDeletedUser = await request(server)
        .delete(`/users/${resCreatedUser.body.id}`)
        .set('Authorization', `Bearer ${jwt}`)
        .expect(200);

      expect(resDeletedUser.body.id).toEqual(resCreatedUser.body.id);
      expect(resDeletedUser.body.username).toEqual(
        resCreatedUser.body.username,
      );
      expect(resDeletedUser.body).not.toHaveProperty('password');
      expect(resDeletedUser.body.role).toEqual(resCreatedUser.body.role);
      expect(resDeletedUser.body.status).toEqual(resCreatedUser.body.status);
      expect(resDeletedUser.body.createdAt).toEqual(
        resCreatedUser.body.createdAt,
      );
      expect(resDeletedUser.body.updatedAt).toEqual(
        resCreatedUser.body.updatedAt,
      );
    });
  });

  it('should return a 400 error when trying to delete an user with a malformed UUID on /users/{userId} (DELETE)', async () => {
    return request(server)
      .delete('/users/notuuid')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('should return a 404 error when trying to delete an undefined user on /users/{userId} (DELETE)', async () => {
    return request(server)
      .delete('/users/00000000-0000-0000-0000-000000000000')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(404);
  });

  it('should be able to get all users on /users (GET)', async () => {
    const page = 2;
    const limit = 10;

    const start = (page - 1) * limit;
    const end = start + limit;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const createdUsers: any[] = [];

    // eslint-disable-next-line no-plusplus
    for (let userNb = 0; userNb < 35; userNb++) {
      // eslint-disable-next-line no-await-in-loop
      const res = await request(server)
        .post('/users')
        .set('Authorization', `Bearer ${jwt}`)
        .send(
          new CreateUserDto(
            `user${userNb}`,
            `userpass${userNb}`,
            selectRandomEnum(UserRole),
            selectRandomEnum(UserStatus),
          ),
        );

      createdUsers.push({
        id: res.body.id,
        username: res.body.username,
        role: res.body.role,
        status: res.body.status,
        createdAt: res.body.createdAt,
        updatedAt: res.body.updatedAt,
      });
    }

    createdUsers.sort((user1: ReadUserDto, user2: ReadUserDto) =>
      user1.username.localeCompare(user2.username),
    );

    const res = await request(server)
      .get(`/users?page=${page}&limit=${limit}`)
      .set('Authorization', `Bearer ${jwt}`);

    const { items, meta, links } = res.body;

    const pagination = new UsersPaginationDto(
      items,
      new PaginationMetaDto(
        parseInt(meta.currentPage, 10),
        parseInt(meta.itemCount, 10),
        parseInt(meta.itemsPerPage, 10),
        parseInt(meta.totalItems, 10),
        parseInt(meta.totalPages, 10),
      ),
      new PaginationLinksDto(
        links.first,
        links.last,
        links.next,
        links.previous,
      ),
    );

    const results = new UsersPaginationDto(
      createdUsers.slice(start - 1, end - 1),
      new PaginationMetaDto(2, 10, 10, 36, 4),
      new PaginationLinksDto(
        '/users?limit=10',
        '/users?page=4&limit=10',
        '/users?page=3&limit=10',
        '/users?page=1&limit=10',
      ),
    );

    expect(pagination).toEqual(results);
  });

  it('should return a 400 error when trying to retrieve users page under 1 on /users (GET)', async () => {
    return request(server)
      .get('/users?page=0')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('should return a 400 error when trying to retrieve users limit under 1 on /users (GET)', async () => {
    return request(server)
      .get('/users?page=1&limit=0')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('cannot access to WS as an user without a JWT', async (done) => {
    const socket = connect(`${serverAddress}/users`, {
      forceNew: true,
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.EXCEPTION, async () => {
      socket.close();
      done();
    });
  });

  it('cannot access to WS as an user with a wrong JWT', async (done) => {
    const socket = connect(`${serverAddress}/users`, {
      forceNew: true,
      query: {
        jwt: 'wrongjwt',
      },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.EXCEPTION, async () => {
      socket.close();
      done();
    });
  });

  it('can access to WS as an user with a JWT', async (done) => {
    const socket = connect(`${serverAddress}/users`, {
      forceNew: true,
      query: { jwt },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      socket.close();
      done();
    });
  });
});
