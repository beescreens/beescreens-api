import { User } from '@beescreens/beescreens';

export type ReadUser = Omit<User, 'password'>;
