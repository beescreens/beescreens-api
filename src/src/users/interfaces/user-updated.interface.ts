import { User } from '@beescreens/beescreens';

export type UserUpdated = Omit<User, 'password'>;
