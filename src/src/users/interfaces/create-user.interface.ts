import { User } from '@beescreens/beescreens';

export type CreateUser = Omit<User, 'id' | 'createdAt' | 'updatedAt'>;
