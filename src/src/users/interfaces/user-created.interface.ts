import { User } from '@beescreens/beescreens';

export type UserCreated = Omit<User, 'password'>;
