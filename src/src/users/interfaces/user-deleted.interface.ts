import { User } from '@beescreens/beescreens';

export type UserDeleted = Omit<User, 'password'>;
