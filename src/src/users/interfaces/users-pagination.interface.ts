import { ReadUser } from './read-user.interface';
import { PaginationMeta, PaginationLinks } from '../../common/interfaces';

export interface UsersPagination {
  readonly items: ReadUser[];
  readonly meta: PaginationMeta;
  readonly links: PaginationLinks;
}
