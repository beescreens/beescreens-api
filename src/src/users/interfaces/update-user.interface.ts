import { User } from '@beescreens/beescreens';

export type UpdateUser = Omit<User, 'id' | 'createdAt' | 'updatedAt'>;
