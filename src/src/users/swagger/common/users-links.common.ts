import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import {
  GetUserByIdLinkSpecs,
  DeleteUserByIdLinkSpecs,
  UpdateUserByIdLinkSpecs,
} from '../links-specs';

export const UsersLinksApiCommon: Record<string, LinkObject> = {
  GetUserById: GetUserByIdLinkSpecs,
  DeleteUserById: DeleteUserByIdLinkSpecs,
  UpdateUserById: UpdateUserByIdLinkSpecs,
};
