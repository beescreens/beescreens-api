import {
  ReferenceObject,
  PathItemObject,
} from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import {
  OnJoinedCallbackSpecs,
  OnExceptionCallbackSpecs,
  OnDisconnectCallbackSpecs,
} from '../../../common/swagger/callbacks-specs';
import { OnMessageCallbackSpecs } from '../../../messages/swagger/callbacks-specs';
import {
  OnUserCreatedCallbackSpecs,
  OnUserUpdatedCallbackSpecs,
  OnUserDeletedCallbackSpecs,
  OnConnectUserCallbackSpecs,
  EmitJoinAsUserCallbackSpecs,
} from '../callbacks-specs';

export const UsersCallbacksApiCommon: Record<
  string,
  ReferenceObject | Record<string, PathItemObject>
> = {
  'WebSockets (common)': {
    onConnect: OnConnectUserCallbackSpecs,
    onJoined: OnJoinedCallbackSpecs,
    onException: OnExceptionCallbackSpecs,
    onDisconnect: OnDisconnectCallbackSpecs,
    onMessage: OnMessageCallbackSpecs,
  },
  'WebSockets (as user)': {
    emitJoinAsUser: EmitJoinAsUserCallbackSpecs,
    onUserCreated: OnUserCreatedCallbackSpecs,
    onUserUpdated: OnUserUpdatedCallbackSpecs,
    onUserDeleted: OnUserDeletedCallbackSpecs,
  },
};
