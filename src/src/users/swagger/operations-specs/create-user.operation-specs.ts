import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { UsersCallbacksApiCommon } from '../common';

export const CreateUserOperationSpecs: Partial<OperationObject> = {
  summary: 'Create a new user',
  description: 'Create a new user.',
  operationId: 'createUser',
  callbacks: UsersCallbacksApiCommon,
};
