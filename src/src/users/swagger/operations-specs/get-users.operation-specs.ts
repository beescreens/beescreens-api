import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { UsersCallbacksApiCommon } from '../common';

export const GetUsersOperationSpecs: Partial<OperationObject> = {
  summary: 'Get the users',
  description: 'Get the users.',
  operationId: 'getUsers',
  callbacks: UsersCallbacksApiCommon,
};
