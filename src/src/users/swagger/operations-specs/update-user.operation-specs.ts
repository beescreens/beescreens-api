import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { UsersCallbacksApiCommon } from '../common';

export const UpdateUserOperationSpecs: Partial<OperationObject> = {
  summary: 'Update the specified user',
  description: 'Update the specified user.',
  operationId: 'updateUser',
  callbacks: UsersCallbacksApiCommon,
};
