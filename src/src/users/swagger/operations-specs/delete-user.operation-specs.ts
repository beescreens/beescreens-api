import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { UsersCallbacksApiCommon } from '../common';

export const DeleteUserOperationSpecs: Partial<OperationObject> = {
  summary: 'Delete the specified user',
  description: 'Delete the specified user.',
  operationId: 'deleteUser',
  callbacks: UsersCallbacksApiCommon,
};
