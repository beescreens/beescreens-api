import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { UsersCallbacksApiCommon } from '../common';

export const GetUserOperationSpecs: Partial<OperationObject> = {
  summary: 'Get the specified user',
  description: 'Get the specified user.',
  operationId: 'getUser',
  callbacks: UsersCallbacksApiCommon,
};
