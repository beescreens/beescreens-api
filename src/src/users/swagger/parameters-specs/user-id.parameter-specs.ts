import { ApiParamOptions } from '@nestjs/swagger';

export const UserIdApiParameterSpecs: ApiParamOptions = {
  name: 'userId',
  description: 'The user ID.',
};
