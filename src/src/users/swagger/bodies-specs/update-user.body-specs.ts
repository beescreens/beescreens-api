import { ApiBodyOptions } from '@nestjs/swagger';
import { UpdateUserDto } from '../../dto';

export const UpdateUserBodySpecs: ApiBodyOptions = {
  description: "The user's details.",
  type: UpdateUserDto,
};
