import { ApiBodyOptions } from '@nestjs/swagger';
import { CreateUserDto } from '../../dto';

export const CreateUserBodySpecs: ApiBodyOptions = {
  description: "The user's details.",
  type: CreateUserDto,
};
