import { ApiPropertyOptions } from '@nestjs/swagger';
import { ReadUserDto } from '../../dto';

export const UsersPropertySpecs: ApiPropertyOptions = {
  description: 'The retrieved users.',
  type: () => [ReadUserDto],
};
