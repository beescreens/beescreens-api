import { ApiPropertyOptions } from '@nestjs/swagger';
import { UserStatus } from '@beescreens/beescreens';

export const UserStatusPropertySpecs: ApiPropertyOptions = {
  description: 'Status of the user',
  type: 'string',
  enum: UserStatus,
};
