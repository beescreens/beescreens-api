import { ApiPropertyOptions } from '@nestjs/swagger';

export const UserPasswordPropertySpecs: ApiPropertyOptions = {
  description: 'Password of the user',
  type: 'string',
  format: 'password',
  minLength: 8,
};
