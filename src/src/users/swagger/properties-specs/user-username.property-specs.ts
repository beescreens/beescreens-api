import { ApiPropertyOptions } from '@nestjs/swagger';

export const UserUsernamePropertySpecs: ApiPropertyOptions = {
  description: 'Username of the user',
  type: 'string',
  minLength: 1,
};
