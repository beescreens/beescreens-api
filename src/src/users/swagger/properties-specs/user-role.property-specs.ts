import { ApiPropertyOptions } from '@nestjs/swagger';
import { UserRole } from '@beescreens/beescreens';

export const UserRolePropertySpecs: ApiPropertyOptions = {
  description: 'Role of the user',
  type: 'string',
  enum: UserRole,
};
