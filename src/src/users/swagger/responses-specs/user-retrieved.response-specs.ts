import { ApiResponseOptions } from '@nestjs/swagger';
import { ReadUserDto } from '../../dto';

export const UserRetrievedResponseSpecs: ApiResponseOptions = {
  description: 'User has been successfully retrieved.',
  type: ReadUserDto,
};
