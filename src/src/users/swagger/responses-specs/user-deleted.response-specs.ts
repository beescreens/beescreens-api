import { ApiResponseOptions } from '@nestjs/swagger';
import { UserDeletedDto } from '../../dto';

export const UserDeletedResponseSpecs: ApiResponseOptions = {
  description: 'User has been successfully deleted.',
  type: UserDeletedDto,
};
