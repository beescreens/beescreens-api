import { ApiResponseOptions } from '@nestjs/swagger';
import { UsersPaginationDto } from '../../dto';

export const UsersRetrievedResponseSpecs: ApiResponseOptions = {
  description: 'Users have been successfully retrieved.',
  type: UsersPaginationDto,
};
