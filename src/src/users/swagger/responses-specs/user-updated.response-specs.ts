import { ApiResponseOptions } from '@nestjs/swagger';
import { UserUpdatedDto } from '../../dto';

export const UserUpdatedResponseSpecs: ApiResponseOptions = {
  description: 'User has been successfully updated.',
  type: UserUpdatedDto,
};
