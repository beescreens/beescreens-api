import { ApiResponseOptions } from '@nestjs/swagger';
import { UserCreatedDto } from '../../dto';
import { UsersLinksApiCommon } from '../common';

export const UserCreatedResponseSpecs: ApiResponseOptions = {
  description: 'User has been successfully created.',
  type: UserCreatedDto,
  links: UsersLinksApiCommon,
};
