import { ApiResponseOptions } from '@nestjs/swagger';
import { CreateUserWithOtherUsernameLinkSpecs } from '../links-specs';

export const UserConflictResponseSpecs: ApiResponseOptions = {
  description:
    'Another user has the same username. Please try again with another username.',
  links: {
    AddUserWithOtherUsername: CreateUserWithOtherUsernameLinkSpecs,
  },
};
