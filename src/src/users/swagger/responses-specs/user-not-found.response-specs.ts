import { ApiResponseOptions } from '@nestjs/swagger';

export const UserNotFoundResponseSpecs: ApiResponseOptions = {
  description: 'User has not been found.',
};
