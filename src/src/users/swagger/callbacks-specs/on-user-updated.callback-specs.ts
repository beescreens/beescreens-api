import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const OnUserUpdatedCallbackSpecs: PathItemObject = {
  get: {
    summary: 'An updated user has been emitted by the WebSocket server',
    description: 'An updated user has been emitted by the WebSocket server.',
    operationId: 'onUserUpdated',
    responses: {
      'N/A': {
        description:
          'An updated user has been emitted by the WebSocket server.',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: {
                  description: 'ID of the entity',
                  type: 'string',
                  format: 'uuid',
                },
              },
            },
          },
        },
      },
    },
  },
};
