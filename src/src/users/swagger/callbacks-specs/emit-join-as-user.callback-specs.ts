import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import {
  JoinedLinkSpecs,
  ExceptionLinkSpecs,
} from '../../../common/swagger/links-specs';

export const EmitJoinAsUserCallbackSpecs: PathItemObject = {
  post: {
    summary: 'Join the WebSocket server as an user with an API key',
    description: 'Join the WebSocket server as an user with an API key.',
    operationId: 'emitJoinAsUser',
    parameters: [
      {
        name: 'apiKey',
        description: 'API key to access protected endpoint.',
        in: 'query',
      },
    ],
    responses: {
      'N/A': {
        description: 'Join the WebSocket server.',
        links: {
          Joined: JoinedLinkSpecs,
        },
      },
      exception: {
        description:
          'Check if an exception has been emitted by the WebSocket server',
        links: {
          Exception: ExceptionLinkSpecs,
        },
      },
    },
  },
};
