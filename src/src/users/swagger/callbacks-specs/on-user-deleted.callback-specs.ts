import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const OnUserDeletedCallbackSpecs: PathItemObject = {
  get: {
    summary: 'A deleted user has been emitted by the WebSocket server',
    description: 'A deleted user has been emitted by the WebSocket server.',
    operationId: 'onUserDeleted',
    responses: {
      'N/A': {
        description: 'A deleted user has been emitted by the WebSocket server.',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: {
                  description: 'ID of the entity',
                  type: 'string',
                  format: 'uuid',
                },
              },
            },
          },
        },
      },
    },
  },
};
