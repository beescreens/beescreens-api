import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { UsersLinksApiCommon } from '../common';

export const OnUserCreatedCallbackSpecs: PathItemObject = {
  get: {
    summary: 'A new user has been emitted by the WebSocket server',
    description: 'A new user has been emitted by the WebSocket server.',
    operationId: 'onUserCreated',
    responses: {
      'N/A': {
        description: 'A new user has been emitted by the WebSocket server.',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: {
                  description: 'ID of the entity',
                  type: 'string',
                  format: 'uuid',
                },
              },
            },
          },
        },
        links: UsersLinksApiCommon,
      },
    },
  },
};
