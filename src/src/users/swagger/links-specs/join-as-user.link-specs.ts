import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const JoinAsUserLinkSpecs: LinkObject = {
  description: 'Join the WebSocket server as an user with the JWT.',
  operationId: 'emitJoinAsInternalEntity',
  parameters: { query: 'jwt' },
};
