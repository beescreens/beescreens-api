import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const DeleteUserByIdLinkSpecs: LinkObject = {
  description:
    'The `id` value returned in the response can be used as the `userId` parameter in `DELETE /users/{userId}`.',
  operationId: 'DELETE /users/{userId}',
  parameters: { userId: '$response.body#/id' },
};
