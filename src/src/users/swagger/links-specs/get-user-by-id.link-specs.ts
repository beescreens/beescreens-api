import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const GetUserByIdLinkSpecs: LinkObject = {
  description:
    'The `id` value returned in the response can be used as the `userId` parameter in `GET /users/{userId}`.',
  operationId: 'GET /users/{userId}',
  parameters: { userId: '$response.body#/id' },
};
