import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const CreateUserWithOtherUsernameLinkSpecs: LinkObject = {
  description: 'The `username` must be changed in `POST /users`.',
  operationId: 'POST /users',
  requestBody: {
    username: '$response.body#/username',
    role: '$response.body#/role',
    password: '$response.body#/password',
  },
};
