import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const UpdateUserByIdLinkSpecs: LinkObject = {
  description:
    'The `id` value returned in the response can be used as the `userId` parameter in `PATCH /users/{userId}`.',
  operationId: 'PATCH /users/{userId}',
  parameters: { userId: '$response.body#/id' },
};
