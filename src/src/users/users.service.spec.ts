import { Test, TestingModule } from '@nestjs/testing';
import { v4 as uuidv4 } from 'uuid';
import { transports } from 'winston';
import { Pagination } from 'nestjs-typeorm-paginate';
import { UserRole, UserStatus } from '@beescreens/beescreens';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { UsersService } from './users.service';
import { UserEntity } from './entities';
import { UsersController } from './users.controller';
import { UsersGateway } from './users.gateway';
import { selectRandomEnum } from '../helpers';

describe('UsersService', () => {
  let app: TestingModule;
  let usersService: UsersService;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        TypeOrmModule.forFeature([UserEntity]),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
      ],
      controllers: [UsersController],
      providers: [UsersService, UsersGateway],
    }).compile();

    usersService = app.get<UsersService>(UsersService);
  });

  afterEach(async () => {
    await app.close();
  });

  it('should be able to create the new user', async () => {
    const newUser = new UserEntity(
      uuidv4(),
      'newUser',
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser = await usersService.createOne(newUser);

    expect(createdUser.id).toEqual(newUser.id);
    expect(createdUser.username).toEqual(newUser.username);
    expect(createdUser.password).toEqual(newUser.password);
    expect(createdUser.role).toEqual(newUser.role);
    expect(createdUser.status).toEqual(newUser.status);
    expect(createdUser.createdAt).toBeDefined();
    expect(createdUser.updatedAt).toBeDefined();
  });

  it('creating a new user with the same username should throw an error', async () => {
    const newUserOne = new UserEntity(
      uuidv4(),
      'newUser',
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const newUserTwo = new UserEntity(
      uuidv4(),
      newUserOne.username,
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    try {
      await usersService.createOne(newUserOne);
      await usersService.createOne(newUserTwo);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be able to get the new user and have all the properties', async () => {
    const newUser = new UserEntity(
      uuidv4(),
      'newUser',
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser = await usersService.createOne(newUser);

    const foundUser = await usersService.getOne({ id: createdUser.id });

    expect(foundUser).toEqual(createdUser);
  });

  it('should throw an error when trying to get an undefined user', async () => {
    try {
      await usersService.getOne({ id: '404' });
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be able to update the user', async () => {
    const newUser = new UserEntity(
      uuidv4(),
      'newUser',
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const updateUser = new UserEntity(
      newUser.id,
      'updatedUser',
      'passwordForUpdatedUser',
      UserRole.DEVELOPER,
      UserStatus.DISABLED,
    );

    const createdUser = await usersService.createOne(newUser);

    const updatedUser = await usersService.updateOne(
      createdUser.id,
      updateUser,
    );

    expect(updatedUser.id).toEqual(createdUser.id);
    expect(updatedUser.username).toEqual(updateUser.username);
    expect(updatedUser.password).toEqual(updateUser.password);
    expect(updatedUser.role).toEqual(updateUser.role);
    expect(updatedUser.status).toEqual(updateUser.status);
    expect(updatedUser.createdAt).toEqual(createdUser.createdAt);
    expect(updatedUser.updatedAt).toBeDefined();
  });

  it('should throw an error when trying to update an undefined user', async () => {
    try {
      await usersService.updateOne(
        '404',
        new UserEntity(
          '404',
          'updatedUser',
          'passwordForUpdatedUser',
          UserRole.DEVELOPER,
          UserStatus.DISABLED,
        ),
      );
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be able to delete the user', async () => {
    const newUser = new UserEntity(
      uuidv4(),
      'newUser',
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser = await usersService.createOne(newUser);

    const deletedUser = await usersService.deleteOne({
      id: createdUser.id,
    });

    expect(deletedUser.id).not.toBeDefined();
    expect(deletedUser.username).toEqual(createdUser.username);
    expect(deletedUser.password).toEqual(createdUser.password);
    expect(deletedUser.role).toEqual(createdUser.role);
    expect(deletedUser.status).toEqual(createdUser.status);
    expect(deletedUser.createdAt).toEqual(createdUser.createdAt);
    expect(deletedUser.updatedAt).toEqual(createdUser.updatedAt);
  });

  it('should throw an error when trying to delete an undefined user', async () => {
    try {
      await usersService.deleteOne({ id: '404' });
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be able to get all users', async () => {
    const page = 2;
    const limit = 10;

    const start = (page - 1) * limit;
    const end = start + limit;

    const createdUsers: UserEntity[] = [];

    // eslint-disable-next-line no-plusplus
    for (let userNb = 0; userNb < 35; userNb++) {
      // eslint-disable-next-line no-await-in-loop
      const createdUser = await usersService.createOne(
        new UserEntity(
          uuidv4(),
          `user${userNb}`,
          `user${userNb}`,
          selectRandomEnum<UserRole>(UserRole),
          selectRandomEnum<UserStatus>(UserStatus),
        ),
      );

      createdUsers.push(createdUser);
    }

    createdUsers.sort((user1: UserEntity, user2: UserEntity) =>
      user1.username.localeCompare(user2.username),
    );

    const query = usersService.repository
      .createQueryBuilder('u')
      .orderBy('u.username', 'ASC');

    const pagination = await usersService.getPagination(
      '/users',
      page,
      limit,
      query,
    );

    const results = new Pagination<UserEntity>(
      createdUsers.slice(start, end),
      {
        currentPage: 2,
        itemCount: 10,
        itemsPerPage: 10,
        totalItems: 35,
        totalPages: 4,
      },
      {
        first: '/users?limit=10',
        last: '/users?page=4&limit=10',
        next: '/users?page=3&limit=10',
        previous: '/users?page=1&limit=10',
      },
    );

    expect(pagination).toEqual(results);
  });

  it('should throw an error when trying to retrieve users page under 1', async () => {
    const query = usersService.repository
      .createQueryBuilder('u')
      .orderBy('u.username', 'ASC');

    try {
      await usersService.getPagination('/users', 0, 1, query);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should throw an error when trying to retrieve users limit under 1', async () => {
    const query = usersService.repository
      .createQueryBuilder('u')
      .orderBy('u.username', 'ASC');

    try {
      await usersService.getPagination('/users', 1, 0, query);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });
});
