import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Logger } from 'winston';
import { InjectRepository } from '@nestjs/typeorm';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { AbstractService } from '../common/abstract-classes';
import { UserEntity } from './entities';

@Injectable()
export class UsersService extends AbstractService<UserEntity> {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) readonly logger: Logger,
    @InjectRepository(UserEntity)
    readonly repository: Repository<UserEntity>,
  ) {
    super(UsersService.name, logger, repository);
  }
}
