import { Entity, Column, Unique } from 'typeorm';
import { User, UserRole, UserStatus } from '@beescreens/beescreens';
import { AbstractEntity } from '../../common/abstract-classes';

@Entity({
  name: 'users',
})
@Unique(['username'])
export class UserEntity extends AbstractEntity implements User {
  @Column()
  username: string;

  @Column()
  password: string;

  @Column()
  role: UserRole;

  @Column()
  status: UserStatus;

  constructor(
    id: string,
    username: string,
    password: string,
    role: UserRole,
    status: UserStatus,
  ) {
    super(id);
    this.username = username;
    this.password = password;
    this.role = role;
    this.status = status;
  }
}
