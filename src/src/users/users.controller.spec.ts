import { Test, TestingModule } from '@nestjs/testing';
import {
  ConflictException,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { transports } from 'winston';
import { UserRole, UserStatus } from '@beescreens/beescreens';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { UsersController } from './users.controller';
import {
  ReadUserDto,
  CreateUserDto,
  UpdateUserDto,
  UsersPaginationDto,
} from './dto';
import { UsersService } from './users.service';
import { PaginationMetaDto, PaginationLinksDto } from '../common/dto';
import { UsersGateway } from './users.gateway';
import { UserEntity } from './entities';
import { selectRandomEnum } from '../helpers';

describe('UsersController', () => {
  let app: TestingModule;
  let usersController: UsersController;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        TypeOrmModule.forFeature([UserEntity]),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
      ],
      controllers: [UsersController],
      providers: [UsersService, UsersGateway],
    }).compile();

    usersController = app.get<UsersController>(UsersController);
  });

  afterEach(async () => {
    await app.close();
  });

  it('should be able to add the new user', async () => {
    const newUser = new CreateUserDto(
      'newUser',
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser = await usersController.createUser(newUser);

    expect(createdUser.id).toBeDefined();
    expect(createdUser.username).toEqual(newUser.username);
    expect(createdUser).not.toHaveProperty('password');
    expect(createdUser.role).toEqual(newUser.role);
    expect(createdUser.status).toEqual(newUser.status);
    expect(createdUser.createdAt).toBeDefined();
    expect(createdUser.updatedAt).toBeDefined();
  });

  it('adding a new user with the same username should throw an error', async () => {
    const newUser = new CreateUserDto(
      'newUser',
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    try {
      await usersController.createUser(newUser);
      await usersController.createUser(newUser);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(ConflictException);
    }
  });

  it('should be able to get the new user and have all the properties', async () => {
    const newUser = new CreateUserDto(
      'newUser',
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser = await usersController.createUser(newUser);

    const foundUser = await usersController.getUser(createdUser.id);

    expect(foundUser).toEqual(createdUser);
  });

  it('should throw an error when trying to get an undefined user', async () => {
    try {
      await usersController.getUser('404');
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to update the user', async () => {
    const newUser = new CreateUserDto(
      'newUser',
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const updateUser = new UpdateUserDto(
      'updatedUser',
      'passwordForUpdatedUser',
      UserRole.DEVELOPER,
      UserStatus.DISABLED,
    );

    const createdUser = await usersController.createUser(newUser);

    const updatedUser = await usersController.updateUser(
      createdUser.id,
      updateUser,
    );

    expect(updatedUser.id).toEqual(createdUser.id);
    expect(updatedUser.username).toEqual(updateUser.username);
    expect(updatedUser).not.toHaveProperty('password');
    expect(updatedUser.role).toEqual(updateUser.role);
    expect(updatedUser.status).toEqual(updateUser.status);
    expect(updatedUser.createdAt).toEqual(createdUser.createdAt);
    expect(updatedUser.updatedAt).toBeDefined();
  });

  it('should throw an error when trying to update an undefined user', async () => {
    try {
      await usersController.updateUser(
        '404',
        new UpdateUserDto(
          'updatedUser',
          'passwordForUpdatedUser',
          UserRole.DEVELOPER,
          UserStatus.DISABLED,
        ),
      );
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to delete the user', async () => {
    const newUser = new CreateUserDto(
      'newUser',
      'passwordForNewUser',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser = await usersController.createUser(newUser);

    const deletedUser = await usersController.deleteUser(createdUser.id);

    expect(deletedUser.id).toEqual(createdUser.id);
    expect(deletedUser.username).toEqual(createdUser.username);
    expect(deletedUser).not.toHaveProperty('password');
    expect(deletedUser.role).toEqual(createdUser.role);
    expect(deletedUser.status).toEqual(createdUser.status);
    expect(deletedUser.createdAt).toEqual(createdUser.createdAt);
    expect(deletedUser.updatedAt).toEqual(createdUser.updatedAt);
  });

  it('should throw an error when trying to delete an undefined user', async () => {
    try {
      await usersController.deleteUser('404');
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to get all users', async () => {
    const page = 2;
    const limit = 10;

    const start = (page - 1) * limit;
    const end = start + limit;

    const createdUsers: ReadUserDto[] = [];

    // eslint-disable-next-line no-plusplus
    for (let userNb = 0; userNb < 35; userNb++) {
      // eslint-disable-next-line no-await-in-loop
      const createdUser = await usersController.createUser(
        new CreateUserDto(
          `user${userNb}`,
          `user${userNb}`,
          selectRandomEnum<UserRole>(UserRole),
          selectRandomEnum<UserStatus>(UserStatus),
        ),
      );

      createdUsers.push(
        new ReadUserDto(
          createdUser.id,
          createdUser.username,
          createdUser.role,
          createdUser.status,
          createdUser.createdAt,
          createdUser.updatedAt,
        ),
      );
    }

    createdUsers.sort((user1: ReadUserDto, user2: ReadUserDto) =>
      user1.username.localeCompare(user2.username),
    );

    const pagination = await usersController.getUsers(page, limit);

    const results = new UsersPaginationDto(
      createdUsers.slice(start, end),
      new PaginationMetaDto(2, 10, 10, 35, 4),
      new PaginationLinksDto(
        '/users?limit=10',
        '/users?page=4&limit=10',
        '/users?page=3&limit=10',
        '/users?page=1&limit=10',
      ),
    );

    expect(pagination).toEqual(results);
  });

  it('should throw an error when trying to retrieve users page under 1', async () => {
    try {
      await usersController.getUsers(0);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(BadRequestException);
    }
  });

  it('should throw an error when trying to retrieve users limit under 1', async () => {
    try {
      await usersController.getUsers(1, 0);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(BadRequestException);
    }
  });
});
