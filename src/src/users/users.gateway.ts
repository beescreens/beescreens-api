import {
  WebSocketGateway,
  ConnectedSocket,
  SubscribeMessage,
  WsResponse,
} from '@nestjs/websockets';
import { Inject, UseGuards } from '@nestjs/common';
import { Logger } from 'winston';
import { WsUserEvents } from '@beescreens/beescreens';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Socket } from 'socket.io';
import { WsJwtGuard } from '../auth/guards';
import { AbstractGateway } from '../common/abstract-classes';
import { JwtDto } from '../auth/dto';

@WebSocketGateway({ namespace: '/users' })
export class UsersGateway extends AbstractGateway {
  constructor(@Inject(WINSTON_MODULE_PROVIDER) logger: Logger) {
    super(UsersGateway.name, logger, new Map<Socket, string>());
  }

  @SubscribeMessage(WsUserEvents.JOIN_AS_USER)
  @UseGuards(WsJwtGuard)
  async joinAsUser(@ConnectedSocket() client: Socket): Promise<WsResponse> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const jwt = (client.handshake as any).user as JwtDto;

    return this.join(jwt.jwtPayload.userRole, client, jwt.jwtPayload.userId);
  }
}
