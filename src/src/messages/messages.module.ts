import { Module } from '@nestjs/common';
import { MessagesController } from './messages.controller';
import { UsersModule } from '../users/users.module';
import { DisplayersModule } from '../displayers/displayers.module';
import { ApplicationsModule } from '../applications/applications.module';
import { PlayersModule } from '../players/players.module';

@Module({
  imports: [UsersModule, ApplicationsModule, DisplayersModule, PlayersModule],
  controllers: [MessagesController],
})
export class MessagesModule {}
