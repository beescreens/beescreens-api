import { IsString, IsEnum } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Message, MessageLevel, MessageType } from '@beescreens/beescreens';
import {
  MessageContentPropertySpecs,
  MessageLevelPropertySpecs,
  MessageTitlePropertySpecs,
  MessageTypePropertySpecs,
} from '../swagger/properties-specs';

export class MessageDto implements Message {
  @ApiProperty(MessageTitlePropertySpecs)
  @IsString()
  readonly title: string;

  @ApiProperty(MessageContentPropertySpecs)
  @IsString()
  readonly content: string;

  @ApiProperty(MessageLevelPropertySpecs)
  @IsEnum(MessageLevel)
  readonly level: MessageLevel;

  @ApiProperty(MessageTypePropertySpecs)
  @IsEnum(MessageType)
  readonly type: MessageType;

  constructor(
    title: string,
    content: string,
    level: MessageLevel,
    type: MessageType,
  ) {
    this.title = title;
    this.content = content;
    this.level = level;
    this.type = type;
  }
}
