import { ApiBodyOptions } from '@nestjs/swagger';
import { MessageDto } from '../../dto';

export const MessageBodySpecs: ApiBodyOptions = {
  description: "The message's details.",
  type: MessageDto,
};
