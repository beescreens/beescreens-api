import { ApiPropertyOptions } from '@nestjs/swagger';
import { MessageType } from '@beescreens/beescreens';

export const MessageTypePropertySpecs: ApiPropertyOptions = {
  description: 'Type of the message',
  type: 'string',
  enum: MessageType,
};
