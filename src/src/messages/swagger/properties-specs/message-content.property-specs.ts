import { ApiPropertyOptions } from '@nestjs/swagger';

export const MessageContentPropertySpecs: ApiPropertyOptions = {
  description: 'Content of the message',
  type: 'string',
};
