import { ApiPropertyOptions } from '@nestjs/swagger';

export const MessageTitlePropertySpecs: ApiPropertyOptions = {
  description: 'Title of the message',
  type: 'string',
};
