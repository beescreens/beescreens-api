export * from './message-content.property-specs';
export * from './message-level.property-specs';
export * from './message-title.property-specs';
export * from './message-type.property-specs';
