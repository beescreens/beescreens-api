import { ApiPropertyOptions } from '@nestjs/swagger';
import { MessageLevel } from '@beescreens/beescreens';

export const MessageLevelPropertySpecs: ApiPropertyOptions = {
  description: 'Level of the message',
  type: 'string',
  enum: MessageLevel,
};
