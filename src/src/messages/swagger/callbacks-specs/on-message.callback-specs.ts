import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { getSchemaPath } from '@nestjs/swagger';
import { MessageDto } from '../../dto';

export const OnMessageCallbackSpecs: PathItemObject = {
  get: {
    summary: 'A message has been emitted by the WebSocket server',
    description: 'A message has been emitted by the WebSocket server.',
    operationId: 'onMessage',
    responses: {
      'N/A': {
        description: 'A message has been emitted by the WebSocket server.',
        content: {
          'application/json': {
            schema: {
              $ref: getSchemaPath(MessageDto),
            },
          },
        },
      },
    },
  },
};
