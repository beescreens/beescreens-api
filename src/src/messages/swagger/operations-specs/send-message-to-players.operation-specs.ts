import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const SendMessageToPlayersOperationSpecs: Partial<OperationObject> = {
  summary: 'Send a message to players',
  description: 'Send a message to players.',
  operationId: 'sendMessageToPlayers',
};
