import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const SendMessageToDisplayersOperationSpecs: Partial<OperationObject> = {
  summary: 'Send a message to displayers',
  description: 'Send a message to displayers.',
  operationId: 'sendMessageToDisplayers',
};
