import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const SendMessageToApplicationsOperationSpecs: Partial<OperationObject> = {
  summary: 'Send a message to applications',
  description: 'Send a message to applications.',
  operationId: 'sendMessageToApplications',
};
