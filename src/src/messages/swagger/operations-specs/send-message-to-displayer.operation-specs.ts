import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const SendMessageToDisplayerOperationSpecs: Partial<OperationObject> = {
  summary: 'Send a message to a specific displayer',
  description: 'Send a message to a specific displayer.',
  operationId: 'sendMessageToDisplayer',
};
