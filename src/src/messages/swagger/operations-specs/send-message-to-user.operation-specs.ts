import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const SendMessageToUserOperationSpecs: Partial<OperationObject> = {
  summary: 'Send a message to a specific user',
  description: 'Send a message to a specific user.',
  operationId: 'sendMessageToUser',
};
