import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const SendMessageToApplicationOperationSpecs: Partial<OperationObject> = {
  summary: 'Send a message to a specific application',
  description: 'Send a message to a specific application.',
  operationId: 'sendMessageToApplication',
};
