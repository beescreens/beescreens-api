import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const SendMessageToUsersOperationSpecs: Partial<OperationObject> = {
  summary: 'Send a message to users',
  description: 'Send a message to users.',
  operationId: 'sendMessageToUsers',
};
