import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const SendMessageToPlayerOperationSpecs: Partial<OperationObject> = {
  summary: 'Send a message to a specific player',
  description: 'Send a message to a specific player.',
  operationId: 'sendMessageToPlayer',
};
