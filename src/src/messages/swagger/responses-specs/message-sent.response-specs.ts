import { ApiResponseOptions } from '@nestjs/swagger';

export const MessageSentResponseSpecs: ApiResponseOptions = {
  description: 'The message has been sent to users.',
};
