import {
  Controller,
  Post,
  Body,
  Inject,
  HttpCode,
  Param,
  ParseUUIDPipe,
  NotFoundException,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBody,
  ApiOperation,
  ApiBadRequestResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiParam,
} from '@nestjs/swagger';
import { Logger } from 'winston';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { UserRole } from '@beescreens/beescreens';
import { HttpJwtGuard } from '../auth/guards';
import { MissingOrIncorrectFieldsResponseSpecs } from '../common/swagger/responses-specs';
import { MessageDto } from './dto';
import { MessageBodySpecs } from './swagger/bodies-specs';
import {
  SendMessageToUsersOperationSpecs,
  SendMessageToDisplayersOperationSpecs,
  SendMessageToDisplayerOperationSpecs,
  SendMessageToUserOperationSpecs,
  SendMessageToApplicationsOperationSpecs,
  SendMessageToApplicationOperationSpecs,
  SendMessageToPlayersOperationSpecs,
  SendMessageToPlayerOperationSpecs,
} from './swagger/operations-specs';
import { MessageSentResponseSpecs } from './swagger/responses-specs';
import { UsersGateway } from '../users/users.gateway';
import { DisplayersGateway } from '../displayers/displayers.gateway';
import { HttpAuth } from '../common/decorators';
import { DisplayerNotFoundResponseSpecs } from '../displayers/swagger/responses-specs';
import { DisplayerIdApiParameterSpecs } from '../displayers/swagger/parameters-specs';
import { DisplayersService } from '../displayers/displayers.service';
import { UsersService } from '../users/users.service';
import { ApplicationsGateway } from '../applications/applications.gateway';
import { ApplicationsService } from '../applications/applications.service';
import { ApplicationNotFoundResponseSpecs } from '../applications/swagger/responses-specs';
import { ApplicationIdApiParameterSpecs } from '../applications/swagger/parameters-specs';
import { PlayerIdApiParameterSpecs } from '../players/swagger/parameters-specs';
import { PlayerNotFoundResponseSpecs } from '../players/swagger/responses-specs';
import { PlayersGateway } from '../players/players.gateway';
import { PlayersService } from '../players/players.service';

@ApiTags('Messages')
@Controller('message')
export class MessagesController {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) readonly logger: Logger,
    readonly usersGateway: UsersGateway,
    readonly usersService: UsersService,
    readonly applicationsGateway: ApplicationsGateway,
    readonly applicationsService: ApplicationsService,
    readonly displayersGateway: DisplayersGateway,
    readonly displayersService: DisplayersService,
    readonly playersGateway: PlayersGateway,
    readonly playersService: PlayersService,
  ) {}

  @Post('users')
  @HttpAuth(HttpJwtGuard)
  @HttpCode(200)
  @ApiNoContentResponse(MessageSentResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiOperation(SendMessageToUsersOperationSpecs)
  @ApiBody(MessageBodySpecs)
  async sendMessageToUsers(@Body() messageDto: MessageDto): Promise<void> {
    this.logger.info(`messageDto: ${JSON.stringify(messageDto)}`, {
      context: `${MessagesController.name}::${this.sendMessageToUsers.name}`,
    });

    this.usersGateway.emitMessageToAll(
      [UserRole.ADMIN, UserRole.MODERATOR, UserRole.READ_ONLY],
      messageDto,
    );
  }

  @Post('users/:userId')
  @HttpAuth(HttpJwtGuard)
  @HttpCode(200)
  @ApiNoContentResponse(MessageSentResponseSpecs)
  @ApiNotFoundResponse(DisplayerNotFoundResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiParam(DisplayerIdApiParameterSpecs)
  @ApiOperation(SendMessageToUserOperationSpecs)
  @ApiBody(MessageBodySpecs)
  async sendMessageToUser(
    @Param('userId', ParseUUIDPipe) userId: string,
    @Body() messageDto: MessageDto,
  ): Promise<void> {
    this.logger.info(`userId: ${userId}`, {
      context: `${MessagesController.name}::${this.sendMessageToUser.name}`,
    });

    try {
      const foundUser = await this.usersService.getOne({
        id: userId,
      });

      this.usersGateway.emitMessageToOne(foundUser.id, messageDto);
    } catch (error) {
      throw new NotFoundException();
    }
  }

  @Post('applications')
  @HttpAuth(HttpJwtGuard)
  @HttpCode(200)
  @ApiNoContentResponse(MessageSentResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiOperation(SendMessageToApplicationsOperationSpecs)
  @ApiBody(MessageBodySpecs)
  async sendMessageToApplications(
    @Body() messageDto: MessageDto,
  ): Promise<void> {
    this.logger.info(`messageDto: ${JSON.stringify(messageDto)}`, {
      context: `${MessagesController.name}::${this.sendMessageToApplications.name}`,
    });

    this.applicationsGateway.emitMessageToAll(['external'], messageDto);
  }

  @Post('applications/:applicationId')
  @HttpAuth(HttpJwtGuard)
  @HttpCode(200)
  @ApiNoContentResponse(MessageSentResponseSpecs)
  @ApiNotFoundResponse(ApplicationNotFoundResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiParam(ApplicationIdApiParameterSpecs)
  @ApiOperation(SendMessageToApplicationOperationSpecs)
  @ApiBody(MessageBodySpecs)
  async sendMessageToApplication(
    @Param('applicationId', ParseUUIDPipe) applicationId: string,
    @Body() messageDto: MessageDto,
  ): Promise<void> {
    this.logger.info(`applicationId: ${applicationId}`, {
      context: `${MessagesController.name}::${this.sendMessageToApplication.name}`,
    });

    try {
      const foundApplication = await this.applicationsService.getOne({
        id: applicationId,
      });

      this.applicationsGateway.emitMessageToOne(
        foundApplication.apiKey,
        messageDto,
      );
    } catch (error) {
      throw new NotFoundException();
    }
  }

  @Post('displayers')
  @HttpAuth(HttpJwtGuard)
  @HttpCode(200)
  @ApiNoContentResponse(MessageSentResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiOperation(SendMessageToDisplayersOperationSpecs)
  @ApiBody(MessageBodySpecs)
  async sendMessageToDisplayers(@Body() messageDto: MessageDto): Promise<void> {
    this.logger.info(`messageDto: ${JSON.stringify(messageDto)}`, {
      context: `${MessagesController.name}::${this.sendMessageToDisplayers.name}`,
    });

    this.displayersGateway.emitMessageToAll(['external'], messageDto);
  }

  @Post('displayers/:displayerId')
  @HttpAuth(HttpJwtGuard)
  @HttpCode(200)
  @ApiNoContentResponse(MessageSentResponseSpecs)
  @ApiNotFoundResponse(DisplayerNotFoundResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiParam(DisplayerIdApiParameterSpecs)
  @ApiOperation(SendMessageToDisplayerOperationSpecs)
  @ApiBody(MessageBodySpecs)
  async sendMessageToDisplayer(
    @Param('displayerId', ParseUUIDPipe) displayerId: string,
    @Body() messageDto: MessageDto,
  ): Promise<void> {
    this.logger.info(`displayerId: ${displayerId}`, {
      context: `${MessagesController.name}::${this.sendMessageToDisplayer.name}`,
    });

    try {
      const foundDisplayer = await this.displayersService.getOne({
        id: displayerId,
      });

      this.displayersGateway.emitMessageToOne(
        foundDisplayer.apiKey,
        messageDto,
      );
    } catch (error) {
      throw new NotFoundException();
    }
  }

  @Post('players')
  @HttpAuth(HttpJwtGuard)
  @HttpCode(200)
  @ApiNoContentResponse(MessageSentResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiOperation(SendMessageToPlayersOperationSpecs)
  @ApiBody(MessageBodySpecs)
  async sendMessageToPlayers(@Body() messageDto: MessageDto): Promise<void> {
    this.logger.info(`messageDto: ${JSON.stringify(messageDto)}`, {
      context: `${MessagesController.name}::${this.sendMessageToPlayers.name}`,
    });

    this.playersGateway.emitMessageToAll(['external'], messageDto);
  }

  @Post('players/:playerId')
  @HttpAuth(HttpJwtGuard)
  @HttpCode(200)
  @ApiNoContentResponse(MessageSentResponseSpecs)
  @ApiNotFoundResponse(PlayerNotFoundResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiParam(PlayerIdApiParameterSpecs)
  @ApiOperation(SendMessageToPlayerOperationSpecs)
  @ApiBody(MessageBodySpecs)
  async sendMessageToPlayer(
    @Param('playerId', ParseUUIDPipe) playerId: string,
    @Body() messageDto: MessageDto,
  ): Promise<void> {
    this.logger.info(`playerId: ${playerId}`, {
      context: `${MessagesController.name}::${this.sendMessageToPlayer.name}`,
    });

    try {
      const foundPlayer = await this.playersService.getOne({
        id: playerId,
      });

      this.playersGateway.emitMessageToOne(foundPlayer.id, messageDto);
    } catch (error) {
      throw new NotFoundException();
    }
  }
}
