import { Test, TestingModule } from '@nestjs/testing';
import { transports } from 'winston';
import { MessageLevel, MessageType } from '@beescreens/beescreens';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { NotFoundException } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { MessagesController } from './messages.controller';
import { MessageDto } from './dto';
import { DisplayersModule } from '../displayers/displayers.module';
import { ApplicationsModule } from '../applications/applications.module';
import { PlayersModule } from '../players/players.module';

describe('MessagesController', () => {
  let app: TestingModule;
  let messagesController: MessagesController;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
        UsersModule,
        ApplicationsModule,
        DisplayersModule,
        PlayersModule,
      ],
      controllers: [MessagesController],
    }).compile();

    messagesController = app.get<MessagesController>(MessagesController);
  });

  afterEach(async () => {
    await app.close();
  });

  it('should be able to send a message to all users', async () => {
    await messagesController.sendMessageToUsers(
      new MessageDto(
        'title',
        'content',
        MessageLevel.INFO,
        MessageType.DEVELOPER,
      ),
    );
  });

  it("should throw an error if user doesn't exist", async () => {
    try {
      await messagesController.sendMessageToUser(
        '404',
        new MessageDto(
          'title',
          'content',
          MessageLevel.INFO,
          MessageType.DEVELOPER,
        ),
      );
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to send a message to all players', async () => {
    await messagesController.sendMessageToDisplayers(
      new MessageDto(
        'title',
        'content',
        MessageLevel.INFO,
        MessageType.DEVELOPER,
      ),
    );
  });

  it("should throw an error if player doesn't exist", async () => {
    try {
      await messagesController.sendMessageToDisplayer(
        '404',
        new MessageDto(
          'title',
          'content',
          MessageLevel.INFO,
          MessageType.DEVELOPER,
        ),
      );
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to send a message to all players', async () => {
    await messagesController.sendMessageToPlayers(
      new MessageDto(
        'title',
        'content',
        MessageLevel.INFO,
        MessageType.DEVELOPER,
      ),
    );
  });

  it("should throw an error if player doesn't exist", async () => {
    try {
      await messagesController.sendMessageToPlayer(
        '404',
        new MessageDto(
          'title',
          'content',
          MessageLevel.INFO,
          MessageType.DEVELOPER,
        ),
      );
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });
});
