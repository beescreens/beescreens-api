import { Test, TestingModule } from '@nestjs/testing';
import { v4 as uuidv4 } from 'uuid';
import { INestApplication } from '@nestjs/common';
import request from 'supertest';
import { ScheduleModule } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { transports } from 'winston';
import { connect } from 'socket.io-client';
import {
  WsCommonEvents,
  MessageLevel,
  MessageType,
  DisplayerRole,
  DisplayerStatus,
  UserRole,
  UserStatus,
  ApplicationRole,
  ApplicationStatus,
  WsUserEvents,
  WsMessageEvents,
  WsApplicationEvents,
  WsDisplayerEvents,
  WsPlayerEvents,
} from '@beescreens/beescreens';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { PlayerEntity } from '../players/entities';
import { CreateUserDto } from '../users/dto';
import { CreateApplicationDto } from '../applications/dto';
import { MessagesModule } from './messages.module';
import { AuthModule } from '../auth/auth.module';
import { UsersModule } from '../users/users.module';
import { MessageDto } from './dto';
import { DisplayersModule } from '../displayers/displayers.module';
import { CreateDisplayerDto } from '../displayers/dto';
import { ApplicationsModule } from '../applications/applications.module';
import { waitForJoined } from '../helpers';
import { PlayersService } from '../players/players.service';

describe('Messages (e2e)', () => {
  let app: INestApplication;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let server: any;
  let serverAddress: string;
  let configService: ConfigService;
  let playersService: PlayersService;
  let jwt: string;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ScheduleModule.forRoot(),
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
        AuthModule,
        MessagesModule,
        UsersModule,
        ApplicationsModule,
        DisplayersModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    server = app.getHttpServer();
    configService = app.get<ConfigService>(ConfigService);
    playersService = app.get<PlayersService>(PlayersService);

    const { address, port } = server.listen().address();

    serverAddress = `http://[${address}]:${port}`;

    await app.init();

    const res = await request(server)
      .post('/login')
      .send({
        username: configService.get<string>('DEFAULT_USERNAME'),
        password: configService.get<string>('DEFAULT_PASSWORD'),
      });

    jwt = res.body.jwt;
  });

  afterAll(async () => {
    await app.close();
  });

  it('should be able to send a message to all users', async (done) => {
    const messageDto = new MessageDto(
      'title',
      'content',
      MessageLevel.INFO,
      MessageType.USER,
    );

    const socket = connect(`${serverAddress}/users`, {
      forceNew: true,
      query: { jwt },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsMessageEvents.MESSAGE, async (message: MessageDto) => {
      expect(message).toEqual(messageDto);

      socket.close();
      done();
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      await request(server)
        .post('/message/users')
        .set('Authorization', `Bearer ${jwt}`)
        .send(messageDto);
    });
  });

  it('should be able to send a message to one user', async (done) => {
    const messageDto = new MessageDto(
      'title',
      'content',
      MessageLevel.INFO,
      MessageType.USER,
    );

    const user1 = new CreateUserDto(
      'user1',
      'passwordForUser1',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const user2 = new CreateUserDto(
      'user2',
      'passwordForUser2',
      UserRole.ADMIN,
      UserStatus.ENABLED,
    );

    const createdUser1 = await request(server)
      .post('/users')
      .set('Authorization', `Bearer ${jwt}`)
      .send(user1);

    await request(server)
      .post('/users')
      .set('Authorization', `Bearer ${jwt}`)
      .send(user2);

    const login1 = await request(server).post('/login').send({
      username: user1.username,
      password: user1.password,
    });

    const login2 = await request(server).post('/login').send({
      username: user2.username,
      password: user2.password,
    });

    const socketUser1 = connect(`${serverAddress}/users`, {
      forceNew: true,
      query: { jwt: login1.body.jwt },
    });

    const socketUser2 = connect(`${serverAddress}/users`, {
      forceNew: true,
      query: { jwt: login2.body.jwt },
    });

    socketUser1.on(WsCommonEvents.CONNECT, async () => {
      socketUser1.emit(WsUserEvents.JOIN_AS_USER);
    });

    socketUser2.on(WsCommonEvents.CONNECT, async () => {
      socketUser2.emit(WsUserEvents.JOIN_AS_USER);
    });

    socketUser1.on(WsMessageEvents.MESSAGE, async (message: MessageDto) => {
      expect(message).toEqual(messageDto);

      socketUser1.close();
      socketUser2.close();
      done();
    });

    socketUser2.on(WsMessageEvents.MESSAGE, async () => {
      socketUser1.close();
      socketUser2.close();
      throw new Error();
    });

    await Promise.all([waitForJoined(socketUser1), waitForJoined(socketUser2)]);

    await request(server)
      .post(`/message/users/${createdUser1.body.id}`)
      .set('Authorization', `Bearer ${jwt}`)
      .send(messageDto);
  });

  it('should be able to send a message to all applications', async (done) => {
    const messageDto = new MessageDto(
      'title',
      'content',
      MessageLevel.INFO,
      MessageType.USER,
    );

    const res = await request(server)
      .post('/applications')
      .set('Authorization', `Bearer ${jwt}`)
      .send(
        new CreateApplicationDto(
          'app',
          'description',
          'version',
          'contact@example.com',
          120,
          'https://example.com/logo.png',
          'https://example.com',
          'https://example.com/docs',
          'https://play.example.com',
          1,
          1,
          1,
          1,
          ApplicationRole.ACTIVE,
          ApplicationStatus.ENABLED,
        ),
      );

    const socket = connect(`${serverAddress}/applications`, {
      forceNew: true,
      query: { apiKey: res.body.apiKey },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsApplicationEvents.JOIN_AS_APPLICATION);
    });

    socket.on(WsMessageEvents.MESSAGE, async (message: MessageDto) => {
      expect(message).toEqual(messageDto);

      socket.close();
      done();
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      await request(server)
        .post('/message/applications')
        .set('Authorization', `Bearer ${jwt}`)
        .send(messageDto);
    });
  });

  it('should be able to send a message to one application', async (done) => {
    const messageDto = new MessageDto(
      'title',
      'content',
      MessageLevel.INFO,
      MessageType.USER,
    );

    const application1 = await request(server)
      .post('/applications')
      .set('Authorization', `Bearer ${jwt}`)
      .send(
        new CreateApplicationDto(
          'app1',
          'description',
          'version',
          'contact@example.com',
          120,
          'https://example.com/logo.png',
          'https://example.com',
          'https://example.com/docs',
          'https://play.example.com',
          1,
          1,
          1,
          1,
          ApplicationRole.ACTIVE,
          ApplicationStatus.ENABLED,
        ),
      );

    const application2 = await request(server)
      .post('/applications')
      .set('Authorization', `Bearer ${jwt}`)
      .send(
        new CreateApplicationDto(
          'app2',
          'description',
          'version',
          'contact@example.com',
          120,
          'https://example.com/logo.png',
          'https://example.com',
          'https://example.com/docs',
          'https://play.example.com',
          1,
          1,
          1,
          1,
          ApplicationRole.ACTIVE,
          ApplicationStatus.ENABLED,
        ),
      );

    const socketApplication1 = connect(`${serverAddress}/applications`, {
      forceNew: true,
      query: { apiKey: application1.body.apiKey },
    });

    const socketApplication2 = connect(`${serverAddress}/applications`, {
      forceNew: true,
      query: { apiKey: application2.body.apiKey },
    });

    socketApplication1.on(WsCommonEvents.CONNECT, async () => {
      socketApplication1.emit(WsApplicationEvents.JOIN_AS_APPLICATION);
    });

    socketApplication2.on(WsCommonEvents.CONNECT, async () => {
      socketApplication2.emit(WsApplicationEvents.JOIN_AS_APPLICATION);
    });

    socketApplication1.on(
      WsMessageEvents.MESSAGE,
      async (message: MessageDto) => {
        expect(message).toEqual(messageDto);

        socketApplication1.close();
        socketApplication2.close();
        done();
      },
    );

    socketApplication2.on(WsMessageEvents.MESSAGE, async () => {
      socketApplication1.close();
      socketApplication2.close();
      throw new Error();
    });

    await Promise.all([
      waitForJoined(socketApplication1),
      waitForJoined(socketApplication2),
    ]);

    await request(server)
      .post(`/message/applications/${application1.body.id}`)
      .set('Authorization', `Bearer ${jwt}`)
      .send(messageDto);
  });

  it('should be able to send a message to all displayers', async (done) => {
    const messageDto = new MessageDto(
      'title',
      'content',
      MessageLevel.INFO,
      MessageType.USER,
    );

    const res = await request(server)
      .post('/displayers')
      .set('Authorization', `Bearer ${jwt}`)
      .send(
        new CreateDisplayerDto(
          'name',
          'location',
          DisplayerRole.ACTIVE,
          DisplayerStatus.ENABLED,
        ),
      );

    const socket = connect(`${serverAddress}/displayers`, {
      forceNew: true,
      query: { apiKey: res.body.apiKey },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsDisplayerEvents.JOIN_AS_DISPLAYER);
    });

    socket.on(WsMessageEvents.MESSAGE, async (message: MessageDto) => {
      expect(message).toEqual(messageDto);

      socket.close();
      done();
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      await request(server)
        .post('/message/displayers')
        .set('Authorization', `Bearer ${jwt}`)
        .send(messageDto);
    });
  });

  it('should be able to send a message to one displayer', async (done) => {
    const messageDto = new MessageDto(
      'title',
      'content',
      MessageLevel.INFO,
      MessageType.USER,
    );

    const displayer1 = await request(server)
      .post('/displayers')
      .set('Authorization', `Bearer ${jwt}`)
      .send(
        new CreateDisplayerDto(
          'displayer1',
          'location',
          DisplayerRole.ACTIVE,
          DisplayerStatus.ENABLED,
        ),
      );

    const displayer2 = await request(server)
      .post('/displayers')
      .set('Authorization', `Bearer ${jwt}`)
      .send(
        new CreateDisplayerDto(
          'displayer2',
          'location',
          DisplayerRole.ACTIVE,
          DisplayerStatus.ENABLED,
        ),
      );

    const socketDisplayer1 = connect(`${serverAddress}/displayers`, {
      forceNew: true,
      query: { apiKey: displayer1.body.apiKey },
    });

    const socketDisplayer2 = connect(`${serverAddress}/displayers`, {
      forceNew: true,
      query: { apiKey: displayer2.body.apiKey },
    });

    socketDisplayer1.on(WsCommonEvents.CONNECT, async () => {
      socketDisplayer1.emit(WsDisplayerEvents.JOIN_AS_DISPLAYER);
    });

    socketDisplayer2.on(WsCommonEvents.CONNECT, async () => {
      socketDisplayer2.emit(WsDisplayerEvents.JOIN_AS_DISPLAYER);
    });

    socketDisplayer1.on(
      WsMessageEvents.MESSAGE,
      async (message: MessageDto) => {
        expect(message).toEqual(messageDto);

        socketDisplayer1.close();
        socketDisplayer2.close();
        done();
      },
    );

    socketDisplayer2.on(WsMessageEvents.MESSAGE, async () => {
      socketDisplayer1.close();
      socketDisplayer2.close();
      throw new Error();
    });

    await Promise.all([
      waitForJoined(socketDisplayer1),
      waitForJoined(socketDisplayer2),
    ]);

    await request(server)
      .post(`/message/displayers/${displayer1.body.id}`)
      .set('Authorization', `Bearer ${jwt}`)
      .send(messageDto);
  });

  it('should be able to send a message to all players', async (done) => {
    const messageDto = new MessageDto(
      'title',
      'content',
      MessageLevel.INFO,
      MessageType.USER,
    );

    const newPlayer = new PlayerEntity(uuidv4());

    const createdPlayer = await playersService.createOne(newPlayer);

    const socket = connect(`${serverAddress}/players`, {
      forceNew: true,
      query: { id: createdPlayer.id },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsPlayerEvents.JOIN_AS_PLAYER);
    });

    socket.on(WsMessageEvents.MESSAGE, async (message: MessageDto) => {
      expect(message).toEqual(messageDto);

      socket.close();
      done();
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      await request(server)
        .post('/message/players')
        .set('Authorization', `Bearer ${jwt}`)
        .send(messageDto);
    });
  });

  it('should be able to send a message to one player', async (done) => {
    const messageDto = new MessageDto(
      'title',
      'content',
      MessageLevel.INFO,
      MessageType.USER,
    );

    const player1 = await playersService.createOne(new PlayerEntity(uuidv4()));

    const player2 = await playersService.createOne(new PlayerEntity(uuidv4()));

    const socketPlayer1 = connect(`${serverAddress}/players`, {
      forceNew: true,
      query: { id: player1.id },
    });

    const socketPlayer2 = connect(`${serverAddress}/players`, {
      forceNew: true,
      query: { id: player2.id },
    });

    socketPlayer1.on(WsCommonEvents.CONNECT, async () => {
      socketPlayer1.emit(WsPlayerEvents.JOIN_AS_PLAYER);
    });

    socketPlayer2.on(WsCommonEvents.CONNECT, async () => {
      socketPlayer2.emit(WsPlayerEvents.JOIN_AS_PLAYER);
    });

    socketPlayer1.on(WsMessageEvents.MESSAGE, async (message: MessageDto) => {
      expect(message).toEqual(messageDto);

      socketPlayer1.close();
      socketPlayer2.close();
      done();
    });

    socketPlayer2.on(WsMessageEvents.MESSAGE, async () => {
      socketPlayer1.close();
      socketPlayer2.close();
      throw new Error();
    });

    await Promise.all([
      waitForJoined(socketPlayer1),
      waitForJoined(socketPlayer2),
    ]);

    await request(server)
      .post(`/message/players/${player1.id}`)
      .set('Authorization', `Bearer ${jwt}`)
      .send(messageDto);
  });
});
