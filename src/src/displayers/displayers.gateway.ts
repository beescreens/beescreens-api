import {
  WebSocketGateway,
  ConnectedSocket,
  SubscribeMessage,
  WsResponse,
  MessageBody,
} from '@nestjs/websockets';
import { Inject, UseGuards } from '@nestjs/common';
import { Logger } from 'winston';
import {
  WsDisplayerEvents,
  WsUserEvents,
  UserRole,
} from '@beescreens/beescreens';
import { Socket } from 'socket.io';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { EntityIdDto } from '../common/dto';
import { WsJwtGuard } from '../auth/guards';
import { UpdatedDisplayerResolutionDto } from './dto';
import { DisplayersApiKeyGuard } from './guards';
import { AbstractGateway } from '../common/abstract-classes';
import { DisplayersService } from './displayers.service';
import { DisplayerEntity } from './entities';
import { JwtDto } from '../auth/dto';

@WebSocketGateway({ namespace: 'displayers' })
export class DisplayersGateway extends AbstractGateway {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) readonly logger: Logger,
    readonly displayersService: DisplayersService,
  ) {
    super(DisplayersGateway.name, logger, new Map<Socket, string>());
  }

  @SubscribeMessage(WsUserEvents.JOIN_AS_USER)
  @UseGuards(WsJwtGuard)
  async joinAsUser(@ConnectedSocket() client: Socket): Promise<WsResponse> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const jwt = (client.handshake as any).user as JwtDto;

    return this.join(jwt.jwtPayload.userRole, client, jwt.jwtPayload.userId);
  }

  @SubscribeMessage(WsDisplayerEvents.JOIN_AS_DISPLAYER)
  @UseGuards(DisplayersApiKeyGuard)
  async joinAsDisplayer(
    @ConnectedSocket() client: Socket,
  ): Promise<WsResponse> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const displayer = (client.handshake as any).user as DisplayerEntity;

    return this.join('external', client, displayer.apiKey);
  }

  @SubscribeMessage(WsDisplayerEvents.UPDATED_DISPLAYER_RESOLUTION)
  @UseGuards(DisplayersApiKeyGuard)
  async updatedDisplayerResolution(
    @ConnectedSocket() client: Socket,
    @MessageBody() updatedDisplayerResolution: UpdatedDisplayerResolutionDto,
  ): Promise<void> {
    this.logger.info(`client: ${client.id}`, {
      context: `${this.className}::${this.updatedDisplayerResolution.name}`,
    });

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const displayer = (client.handshake as any).user as DisplayerEntity;

    displayer.width = updatedDisplayerResolution.width;
    displayer.height = updatedDisplayerResolution.height;

    const updatedDisplayer = await this.displayersService.updateOne(
      displayer.id,
      displayer,
    );

    this.emitToAll(
      [UserRole.ADMIN, UserRole.MODERATOR, UserRole.READ_ONLY],
      WsDisplayerEvents.DISPLAYER_UPDATED,
      new EntityIdDto(updatedDisplayer.id),
    );
  }
}
