import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength, IsEnum } from 'class-validator';
import { DisplayerRole, DisplayerStatus } from '@beescreens/beescreens';
import { UpdateDisplayer } from '../interfaces';
import {
  DisplayerNamePropertySpecs,
  DisplayerLocationPropertySpecs,
  DisplayerRolePropertySpecs,
  DisplayerStatusPropertySpecs,
} from '../swagger/properties-specs';

export class UpdateDisplayerDto implements UpdateDisplayer {
  @ApiProperty(DisplayerNamePropertySpecs)
  @IsString()
  @MinLength(1)
  readonly name: string;

  @ApiProperty(DisplayerLocationPropertySpecs)
  @IsString()
  @MinLength(1)
  readonly location: string;

  @ApiProperty(DisplayerRolePropertySpecs)
  @IsEnum(DisplayerRole)
  readonly role: DisplayerRole;

  @ApiProperty(DisplayerStatusPropertySpecs)
  @IsEnum(DisplayerStatus)
  readonly status: DisplayerStatus;

  constructor(
    name: string,
    location: string,
    role: DisplayerRole,
    status: DisplayerStatus,
  ) {
    this.name = name;
    this.location = location;
    this.role = role;
    this.status = status;
  }
}
