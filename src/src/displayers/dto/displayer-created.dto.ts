import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsEnum,
  IsDate,
  MinLength,
  IsInt,
  IsUUID,
} from 'class-validator';
import { DisplayerRole, DisplayerStatus } from '@beescreens/beescreens';
import {
  EntityIdPropertySpecs,
  EntityCreatedAtPropertySpecs,
  EntityUpdatedAtPropertySpecs,
  EntityApiKeyPropertySpecs,
} from '../../common/swagger/properties-specs';
import { DisplayerCreated } from '../interfaces';
import {
  DisplayerNamePropertySpecs,
  DisplayerLocationPropertySpecs,
  DisplayerRolePropertySpecs,
  DisplayerStatusPropertySpecs,
  DisplayerWidthPropertySpecs,
  DisplayerHeightPropertySpecs,
} from '../swagger/properties-specs';

export class DisplayerCreatedDto implements DisplayerCreated {
  @ApiProperty(EntityIdPropertySpecs)
  @IsUUID()
  readonly id: string;

  @ApiProperty(EntityApiKeyPropertySpecs)
  @IsString()
  readonly apiKey: string;

  @ApiProperty(DisplayerNamePropertySpecs)
  @IsString()
  @MinLength(1)
  readonly name: string;

  @ApiProperty(DisplayerLocationPropertySpecs)
  @IsString()
  @MinLength(1)
  readonly location: string;

  @ApiProperty(DisplayerWidthPropertySpecs)
  @IsInt()
  readonly width: number;

  @ApiProperty(DisplayerHeightPropertySpecs)
  @IsInt()
  readonly height: number;

  @ApiProperty(DisplayerRolePropertySpecs)
  @IsEnum(DisplayerRole)
  readonly role: DisplayerRole;

  @ApiProperty(DisplayerStatusPropertySpecs)
  @IsEnum(DisplayerStatus)
  readonly status: DisplayerStatus;

  @ApiProperty(EntityCreatedAtPropertySpecs)
  @IsDate()
  readonly createdAt: Date;

  @ApiProperty(EntityUpdatedAtPropertySpecs)
  @IsDate()
  readonly updatedAt: Date;

  constructor(
    id: string,
    apiKey: string,
    name: string,
    location: string,
    width: number,
    height: number,
    role: DisplayerRole,
    status: DisplayerStatus,
    createdAt: Date,
    updatedAt: Date,
  ) {
    this.id = id;
    this.apiKey = apiKey;
    this.name = name;
    this.location = location;
    this.width = width;
    this.height = height;
    this.role = role;
    this.status = status;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
