import { ApiProperty } from '@nestjs/swagger';
import { IsArray } from 'class-validator';
import { PaginationMetaDto, PaginationLinksDto } from '../../common/dto';
import { DisplayersPagination } from '../interfaces';
import { ReadDisplayerDto } from './read-displayer.dto';
import { DisplayersPropertySpecs } from '../swagger/properties-specs';

export class DisplayersPaginationDto implements DisplayersPagination {
  @ApiProperty(DisplayersPropertySpecs)
  @IsArray()
  readonly items: ReadDisplayerDto[];

  @ApiProperty()
  readonly meta: PaginationMetaDto;

  @ApiProperty()
  readonly links: PaginationLinksDto;

  constructor(
    items: ReadDisplayerDto[],
    meta: PaginationMetaDto,
    links: PaginationLinksDto,
  ) {
    this.items = items;
    this.meta = meta;
    this.links = links;
  }
}
