import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { UpdatedDisplayerResolution } from '../interfaces';
import {
  DisplayerHeightPropertySpecs,
  DisplayerWidthPropertySpecs,
} from '../swagger/properties-specs';

export class UpdatedDisplayerResolutionDto
  implements UpdatedDisplayerResolution {
  @ApiProperty(DisplayerWidthPropertySpecs)
  @IsInt()
  readonly width: number;

  @ApiProperty(DisplayerHeightPropertySpecs)
  @IsInt()
  readonly height: number;

  constructor(width: number, height: number) {
    this.width = width;
    this.height = height;
  }
}
