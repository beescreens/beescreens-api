import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Patch,
  Inject,
  ConflictException,
  NotFoundException,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  Query,
  BadRequestException,
  ParseUUIDPipe,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBody,
  ApiOperation,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiConflictResponse,
  ApiParam,
  ApiBadRequestResponse,
  ApiQuery,
} from '@nestjs/swagger';
import { Logger } from 'winston';
import { v4 as uuidv4 } from 'uuid';
import { Pagination } from 'nestjs-typeorm-paginate';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { UserRole, WsDisplayerEvents } from '@beescreens/beescreens';
import { HttpJwtGuard } from '../auth/guards';
import {
  MissingOrIncorrectFieldsResponseSpecs,
  MissingOrIncorrectParamsResponseSpecs,
} from '../common/swagger/responses-specs';
import { DisplayersService } from './displayers.service';
import {
  DisplayersRetrievedResponseSpecs,
  DisplayerCreatedResponseSpecs,
  DisplayerConflictResponseSpecs,
  DisplayerRetrievedResponseSpecs,
  DisplayerNotFoundResponseSpecs,
  DisplayerDeletedResponseSpecs,
  DisplayerUpdatedResponseSpecs,
  DisplayerApiKeyResponseSpecs,
} from './swagger/responses-specs';
import {
  GetDisplayersOperationSpecs,
  CreateDisplayerOperationSpecs,
  GetDisplayerOperationSpecs,
  DeleteDisplayerOperationSpecs,
  UpdateDisplayerOperationSpecs,
  RevokeDisplayerOperationSpecs,
} from './swagger/operations-specs';
import {
  ReadDisplayerDto,
  CreateDisplayerDto,
  UpdateDisplayerDto,
  DisplayerCreatedDto,
  DisplayerUpdatedDto,
  DisplayerDeletedDto,
  DisplayersPaginationDto,
} from './dto';
import {
  CreateDisplayerBodySpecs,
  UpdateDisplayerBodySpecs,
} from './swagger/bodies-specs';
import { DisplayerIdApiParameterSpecs } from './swagger/parameters-specs';
import {
  PaginationPageQuerySpecs,
  PaginationLimitQuerySpecs,
} from '../common/swagger/queries-specs';
import { DisplayerEntity } from './entities';

import {
  PaginationMetaDto,
  PaginationLinksDto,
  EntityApiKeyDto,
  EntityIdDto,
} from '../common/dto';
import { DisplayersGateway } from './displayers.gateway';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DefaultIntValuePipe } from '../common/pipes';
import { HttpAuth } from '../common/decorators';

@ApiTags('Displayers')
@Controller('displayers')
export class DisplayersController {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) readonly logger: Logger,
    readonly displayersService: DisplayersService,
    readonly displayersGateway: DisplayersGateway,
  ) {}

  @Get()
  @HttpAuth(HttpJwtGuard)
  @ApiQuery(PaginationPageQuerySpecs)
  @ApiQuery(PaginationLimitQuerySpecs)
  @ApiOkResponse(DisplayersRetrievedResponseSpecs)
  @ApiOperation(GetDisplayersOperationSpecs)
  async getDisplayers(
    @Query('page', new DefaultIntValuePipe(1)) page = 10,
    @Query('limit', new DefaultIntValuePipe(10)) limit = 10,
  ): Promise<DisplayersPaginationDto> {
    this.logger.info(`page: ${page}, limit: ${limit}`, {
      context: `${DisplayersController.name}::${this.getDisplayers.name}`,
    });

    const query = this.displayersService.repository
      .createQueryBuilder('d')
      .orderBy('d.name', 'ASC');

    let pagination: Pagination<DisplayerEntity>;

    try {
      pagination = await this.displayersService.getPagination(
        '/displayers',
        page,
        limit,
        query,
      );
    } catch (error) {
      throw new BadRequestException();
    }

    const displayers = pagination.items.map(
      async (displayer) =>
        new ReadDisplayerDto(
          displayer.id,
          displayer.name,
          displayer.location,
          displayer.width,
          displayer.height,
          displayer.role,
          displayer.status,
          displayer.createdAt,
          displayer.updatedAt,
        ),
    );

    const { meta, links } = pagination;

    return new DisplayersPaginationDto(
      await Promise.all(displayers),
      new PaginationMetaDto(
        meta.currentPage,
        meta.itemCount,
        meta.itemsPerPage,
        meta.totalItems,
        meta.totalPages,
      ),
      new PaginationLinksDto(
        links.first || '',
        links.last || '',
        links.next || '',
        links.previous || '',
      ),
    );
  }

  @Post()
  @HttpAuth(HttpJwtGuard)
  @ApiCreatedResponse(DisplayerCreatedResponseSpecs)
  @ApiConflictResponse(DisplayerConflictResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiOperation(CreateDisplayerOperationSpecs)
  @ApiBody(CreateDisplayerBodySpecs)
  async createDisplayer(
    @Body() displayerDto: CreateDisplayerDto,
  ): Promise<DisplayerCreatedDto> {
    this.logger.info(`body: ${JSON.stringify(displayerDto)}`, {
      context: `${DisplayersController.name}::${this.createDisplayer.name}`,
    });

    let newDisplayer: DisplayerEntity;

    try {
      const displayer = new DisplayerEntity(
        uuidv4(),
        uuidv4(),
        displayerDto.name,
        displayerDto.location,
        displayerDto.role,
        displayerDto.status,
      );

      newDisplayer = await this.displayersService.createOne(displayer);
    } catch (error) {
      throw new ConflictException();
    }

    const {
      id,
      apiKey,
      name,
      location,
      width,
      height,
      role,
      status,
      createdAt,
      updatedAt,
    } = newDisplayer;

    const newDisplayerDto = new DisplayerCreatedDto(
      id,
      apiKey,
      name,
      location,
      width,
      height,
      role,
      status,
      createdAt,
      updatedAt,
    );

    await this.displayersGateway.emitToAll(
      [UserRole.ADMIN, UserRole.MODERATOR, UserRole.READ_ONLY],
      WsDisplayerEvents.DISPLAYER_CREATED,
      new EntityIdDto(id),
    );

    return newDisplayerDto;
  }

  @Get(':displayerId')
  @HttpAuth(HttpJwtGuard)
  @ApiParam(DisplayerIdApiParameterSpecs)
  @ApiOkResponse(DisplayerRetrievedResponseSpecs)
  @ApiNotFoundResponse(DisplayerNotFoundResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectParamsResponseSpecs)
  @ApiOperation(GetDisplayerOperationSpecs)
  async getDisplayer(
    @Param('displayerId', ParseUUIDPipe) displayerId: string,
  ): Promise<ReadDisplayerDto> {
    this.logger.info(`displayerId: ${displayerId}`, {
      context: `${DisplayersController.name}::${this.getDisplayer.name}`,
    });

    let foundDisplayer: DisplayerEntity;

    try {
      foundDisplayer = await this.displayersService.getOne({
        id: displayerId,
      });
    } catch (error) {
      throw new NotFoundException();
    }

    const {
      id,
      name,
      location,
      width,
      height,
      role,
      status,
      createdAt,
      updatedAt,
    } = foundDisplayer;

    return new ReadDisplayerDto(
      id,
      name,
      location,
      width,
      height,
      role,
      status,
      createdAt,
      updatedAt,
    );
  }

  @Patch(':displayerId')
  @HttpAuth(HttpJwtGuard)
  @ApiOkResponse(DisplayerUpdatedResponseSpecs)
  @ApiNotFoundResponse(DisplayerNotFoundResponseSpecs)
  @ApiConflictResponse(DisplayerConflictResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiParam(DisplayerIdApiParameterSpecs)
  @ApiOperation(UpdateDisplayerOperationSpecs)
  @ApiBody(UpdateDisplayerBodySpecs)
  async updateDisplayer(
    @Param('displayerId', ParseUUIDPipe) displayerId: string,
    @Body() displayerDto: UpdateDisplayerDto,
  ): Promise<DisplayerUpdatedDto> {
    this.logger.info(
      `displayerId: ${displayerId}, body: ${JSON.stringify(displayerDto)}`,
      {
        context: `${DisplayersController.name}::${this.updateDisplayer.name}`,
      },
    );

    let displayerToUpdate: DisplayerEntity;

    try {
      displayerToUpdate = await this.displayersService.getOne({
        id: displayerId,
      });
    } catch (error) {
      throw new NotFoundException();
    }

    const { name, location, role, status } = displayerDto;

    displayerToUpdate.name = name;
    displayerToUpdate.location = location;
    displayerToUpdate.role = role;
    displayerToUpdate.status = status;

    const updatedDisplayer = await this.displayersService.updateOne(
      displayerId,
      displayerToUpdate,
    );

    const updatedDisplayerDto = new DisplayerUpdatedDto(
      updatedDisplayer.id,
      updatedDisplayer.name,
      updatedDisplayer.location,
      updatedDisplayer.width,
      updatedDisplayer.height,
      updatedDisplayer.role,
      updatedDisplayer.status,
      updatedDisplayer.createdAt,
      updatedDisplayer.updatedAt,
    );

    await this.displayersGateway.emitToAll(
      [UserRole.ADMIN, UserRole.MODERATOR, UserRole.READ_ONLY],
      WsDisplayerEvents.DISPLAYER_UPDATED,
      new EntityIdDto(displayerId),
    );

    return updatedDisplayerDto;
  }

  @Delete(':displayerId')
  @HttpAuth(HttpJwtGuard)
  @ApiOkResponse(DisplayerDeletedResponseSpecs)
  @ApiNotFoundResponse(DisplayerNotFoundResponseSpecs)
  @ApiParam(DisplayerIdApiParameterSpecs)
  @ApiOperation(DeleteDisplayerOperationSpecs)
  async deleteDisplayer(
    @Param('displayerId', ParseUUIDPipe) displayerId: string,
  ): Promise<DisplayerDeletedDto> {
    this.logger.info(`displayerId: ${displayerId}`, {
      context: `${DisplayersController.name}::${this.deleteDisplayer.name}`,
    });

    let deletedDisplayer: DisplayerEntity;

    try {
      deletedDisplayer = await this.displayersService.deleteOne({
        id: displayerId,
      });
      deletedDisplayer.id = displayerId;
    } catch (error) {
      throw new NotFoundException();
    }

    const {
      id,
      name,
      location,
      width,
      height,
      role,
      status,
      createdAt,
      updatedAt,
    } = deletedDisplayer;

    const deletedDisplayerDto = new DisplayerDeletedDto(
      id,
      name,
      location,
      width,
      height,
      role,
      status,
      createdAt,
      updatedAt,
    );

    await this.displayersGateway.emitToAll(
      [UserRole.ADMIN, UserRole.MODERATOR, UserRole.READ_ONLY],
      WsDisplayerEvents.DISPLAYER_DELETED,
      new EntityIdDto(id),
    );

    return deletedDisplayerDto;
  }

  @Patch(':displayerId/revoke')
  @HttpAuth(HttpJwtGuard)
  @ApiOkResponse(DisplayerApiKeyResponseSpecs)
  @ApiNotFoundResponse(DisplayerNotFoundResponseSpecs)
  @ApiBadRequestResponse(MissingOrIncorrectFieldsResponseSpecs)
  @ApiParam(DisplayerIdApiParameterSpecs)
  @ApiOperation(RevokeDisplayerOperationSpecs)
  async revokeDisplayer(
    @Param('displayerId', ParseUUIDPipe) displayerId: string,
  ): Promise<EntityApiKeyDto> {
    this.logger.info(`displayerId: ${displayerId}`, {
      context: `${DisplayersController.name}::${this.updateDisplayer.name}`,
    });

    let displayerToRevoke: DisplayerEntity;

    try {
      displayerToRevoke = await this.displayersService.getOne({
        id: displayerId,
      });
    } catch (error) {
      throw new NotFoundException();
    }

    displayerToRevoke.apiKey = uuidv4();

    const revokedDisplayer = await this.displayersService.updateOne(
      displayerId,
      displayerToRevoke,
    );

    const revokedDisplayerDto = new EntityApiKeyDto(revokedDisplayer.apiKey);

    return revokedDisplayerDto;
  }
}
