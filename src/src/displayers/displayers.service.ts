import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Logger } from 'winston';
import { InjectRepository } from '@nestjs/typeorm';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { DisplayerEntity } from './entities';
import { AbstractService } from '../common/abstract-classes';

@Injectable()
export class DisplayersService extends AbstractService<DisplayerEntity> {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) readonly logger: Logger,
    @InjectRepository(DisplayerEntity)
    readonly repository: Repository<DisplayerEntity>,
  ) {
    super(DisplayersService.name, logger, repository);
  }
}
