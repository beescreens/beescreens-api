import { Entity, Column, Unique } from 'typeorm';
import {
  Displayer,
  DisplayerRole,
  DisplayerStatus,
} from '@beescreens/beescreens';
import { AbstractEntity } from '../../common/abstract-classes';

@Entity({
  name: 'displayers',
})
@Unique(['name'])
@Unique(['apiKey'])
export class DisplayerEntity extends AbstractEntity implements Displayer {
  @Column()
  apiKey: string;

  @Column()
  name: string;

  @Column()
  location: string;

  @Column()
  width: number;

  @Column()
  height: number;

  @Column()
  role: DisplayerRole;

  @Column()
  status: DisplayerStatus;

  constructor(
    id: string,
    apiKey: string,
    name: string,
    location: string,
    role: DisplayerRole,
    status: DisplayerStatus,
  ) {
    super(id);
    this.apiKey = apiKey;
    this.name = name;
    this.location = location;
    this.width = -1;
    this.height = -1;
    this.role = role;
    this.status = status;
  }
}
