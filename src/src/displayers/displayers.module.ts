import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DisplayersController } from './displayers.controller';
import { DisplayersService } from './displayers.service';
import { DisplayersGateway } from './displayers.gateway';
import { DisplayerEntity } from './entities';

@Module({
  imports: [TypeOrmModule.forFeature([DisplayerEntity])],
  controllers: [DisplayersController],
  providers: [DisplayersService, DisplayersGateway],
  exports: [DisplayersService, DisplayersGateway],
})
export class DisplayersModule {}
