import { Injectable, ExecutionContext, CanActivate } from '@nestjs/common';
import { DisplayerStatus } from '@beescreens/beescreens';
import { DisplayersService } from '../displayers.service';
import { DisplayerEntity } from '../entities';

@Injectable()
export class DisplayersApiKeyGuard implements CanActivate {
  constructor(readonly displayersService: DisplayersService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const client = context.switchToWs().getClient();

    let displayer: DisplayerEntity;

    try {
      displayer = await this.displayersService.getOne({
        apiKey: client.handshake.query.apiKey,
      });
    } catch (error) {
      return false;
    }

    if (displayer.status !== DisplayerStatus.DISABLED) {
      client.handshake.user = displayer;
      return true;
    }

    return false;
  }
}
