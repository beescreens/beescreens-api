import { Test, TestingModule } from '@nestjs/testing';
import { v4 as uuidv4 } from 'uuid';
import { transports } from 'winston';
import { Pagination } from 'nestjs-typeorm-paginate';
import { DisplayerRole, DisplayerStatus } from '@beescreens/beescreens';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { DisplayersService } from './displayers.service';
import { DisplayerEntity } from './entities';
import { DisplayersController } from './displayers.controller';
import { DisplayersGateway } from './displayers.gateway';
import { selectRandomEnum } from '../helpers';

describe('DisplayersService', () => {
  let app: TestingModule;
  let displayersService: DisplayersService;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        TypeOrmModule.forFeature([DisplayerEntity]),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
      ],
      controllers: [DisplayersController],
      providers: [DisplayersService, DisplayersGateway],
    }).compile();

    displayersService = app.get<DisplayersService>(DisplayersService);
  });

  afterEach(async () => {
    await app.close();
  });

  it('should be able to create the new displayer', async () => {
    const newDisplayer = new DisplayerEntity(
      uuidv4(),
      uuidv4(),
      'newDisplayer',
      'locationNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const createdDisplayer = await displayersService.createOne(newDisplayer);

    expect(createdDisplayer.id).toEqual(newDisplayer.id);
    expect(createdDisplayer.apiKey).toEqual(newDisplayer.apiKey);
    expect(createdDisplayer.name).toEqual(newDisplayer.name);
    expect(createdDisplayer.location).toEqual(newDisplayer.location);
    expect(createdDisplayer.width).toEqual(-1);
    expect(createdDisplayer.height).toEqual(-1);
    expect(createdDisplayer.role).toEqual(newDisplayer.role);
    expect(createdDisplayer.status).toEqual(newDisplayer.status);
    expect(createdDisplayer.createdAt).toBeDefined();
    expect(createdDisplayer.updatedAt).toBeDefined();
  });

  it('creating a new displayer with the same displayername should throw an error', async () => {
    const newDisplayerOne = new DisplayerEntity(
      uuidv4(),
      uuidv4(),
      'newDisplayer',
      'locationNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const newDisplayerTwo = new DisplayerEntity(
      uuidv4(),
      uuidv4(),
      newDisplayerOne.name,
      'locationNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    try {
      await displayersService.createOne(newDisplayerOne);
      await displayersService.createOne(newDisplayerTwo);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be able to get the new displayer and have all the properties', async () => {
    const newDisplayer = new DisplayerEntity(
      uuidv4(),
      uuidv4(),
      'newDisplayer',
      'locationNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const createdDisplayer = await displayersService.createOne(newDisplayer);

    const foundDisplayer = await displayersService.getOne({
      id: createdDisplayer.id,
    });

    expect(foundDisplayer).toEqual(createdDisplayer);
  });

  it('should throw an error when trying to get an undefined displayer', async () => {
    try {
      await displayersService.getOne({ id: '404' });
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be able to update the displayer', async () => {
    const newDisplayer = new DisplayerEntity(
      uuidv4(),
      uuidv4(),
      'newDisplayer',
      'locationNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const updateDisplayer = new DisplayerEntity(
      newDisplayer.id,
      newDisplayer.apiKey,
      'updatedDisplayer',
      'locationUpdatedDisplayer',
      DisplayerRole.PASSIVE,
      DisplayerStatus.DISABLED,
    );

    const createdDisplayer = await displayersService.createOne(newDisplayer);

    const updatedDisplayer = await displayersService.updateOne(
      createdDisplayer.id,
      updateDisplayer,
    );

    expect(updatedDisplayer.id).toEqual(createdDisplayer.id);
    expect(updatedDisplayer.apiKey).toEqual(createdDisplayer.apiKey);
    expect(updatedDisplayer.name).toEqual(updateDisplayer.name);
    expect(updatedDisplayer.location).toEqual(updateDisplayer.location);
    expect(updatedDisplayer.width).toEqual(createdDisplayer.width);
    expect(updatedDisplayer.height).toEqual(createdDisplayer.width);
    expect(updatedDisplayer.role).toEqual(updateDisplayer.role);
    expect(updatedDisplayer.status).toEqual(updateDisplayer.status);
    expect(updatedDisplayer.createdAt).toEqual(createdDisplayer.createdAt);
    expect(updatedDisplayer.updatedAt).toBeDefined();
  });

  it('should throw an error when trying to update an undefined displayer', async () => {
    try {
      await displayersService.updateOne(
        '404',
        new DisplayerEntity(
          '404',
          uuidv4(),
          'updatedDisplayer',
          'locationUpdatedDisplayer',
          DisplayerRole.PASSIVE,
          DisplayerStatus.ENABLED,
        ),
      );
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be able to delete the displayer', async () => {
    const newDisplayer = new DisplayerEntity(
      uuidv4(),
      uuidv4(),
      'newDisplayer',
      'locationNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const createdDisplayer = await displayersService.createOne(newDisplayer);

    const deletedDisplayer = await displayersService.deleteOne({
      id: createdDisplayer.id,
    });

    expect(deletedDisplayer.id).not.toBeDefined();
    expect(deletedDisplayer.apiKey).toEqual(createdDisplayer.apiKey);
    expect(deletedDisplayer.name).toEqual(createdDisplayer.name);
    expect(deletedDisplayer.location).toEqual(createdDisplayer.location);
    expect(deletedDisplayer.width).toEqual(createdDisplayer.width);
    expect(deletedDisplayer.height).toEqual(createdDisplayer.width);
    expect(deletedDisplayer.role).toEqual(createdDisplayer.role);
    expect(deletedDisplayer.status).toEqual(createdDisplayer.status);
    expect(deletedDisplayer.createdAt).toEqual(createdDisplayer.createdAt);
    expect(deletedDisplayer.updatedAt).toEqual(createdDisplayer.updatedAt);
  });

  it('should throw an error when trying to delete an undefined displayer', async () => {
    try {
      await displayersService.deleteOne({ id: '404' });
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be able to get all displayers', async () => {
    const page = 2;
    const limit = 10;

    const start = (page - 1) * limit;
    const end = start + limit;

    const createdDisplayers: DisplayerEntity[] = [];

    // eslint-disable-next-line no-plusplus
    for (let displayerNb = 0; displayerNb < 35; displayerNb++) {
      // eslint-disable-next-line no-await-in-loop
      const createdDisplayer = await displayersService.createOne(
        new DisplayerEntity(
          uuidv4(),
          uuidv4(),
          `displayer${displayerNb}`,
          `location${displayerNb}`,
          selectRandomEnum<DisplayerRole>(DisplayerRole),
          selectRandomEnum<DisplayerStatus>(DisplayerStatus),
        ),
      );

      createdDisplayers.push(createdDisplayer);
    }

    createdDisplayers.sort(
      (displayer1: DisplayerEntity, displayer2: DisplayerEntity) =>
        displayer1.name.localeCompare(displayer2.name),
    );

    const query = displayersService.repository
      .createQueryBuilder('d')
      .orderBy('d.name', 'ASC');

    const pagination = await displayersService.getPagination(
      '/displayers',
      page,
      limit,
      query,
    );

    const results = new Pagination<DisplayerEntity>(
      createdDisplayers.slice(start, end),
      {
        currentPage: 2,
        itemCount: 10,
        itemsPerPage: 10,
        totalItems: 35,
        totalPages: 4,
      },
      {
        first: '/displayers?limit=10',
        last: '/displayers?page=4&limit=10',
        next: '/displayers?page=3&limit=10',
        previous: '/displayers?page=1&limit=10',
      },
    );

    expect(pagination).toEqual(results);
  });

  it('should throw an error when trying to retrieve displayers page under 1', async () => {
    const query = displayersService.repository
      .createQueryBuilder('d')
      .orderBy('d.name', 'ASC');

    try {
      await displayersService.getPagination('/displayers', 0, 1, query);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should throw an error when trying to retrieve displayers limit under 1', async () => {
    const query = displayersService.repository
      .createQueryBuilder('d')
      .orderBy('d.name', 'ASC');

    try {
      await displayersService.getPagination('/displayers', 1, 0, query);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });
});
