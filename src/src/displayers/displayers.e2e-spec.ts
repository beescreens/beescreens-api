import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import request from 'supertest';
import { ScheduleModule } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { transports } from 'winston';
import {
  DisplayerRole,
  DisplayerStatus,
  WsCommonEvents,
  WsUserEvents,
  WsDisplayerEvents,
} from '@beescreens/beescreens';
import { connect } from 'socket.io-client';
import compression from 'compression';
import helmet from 'helmet';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { MessagesModule } from '../messages/messages.module';
import { DisplayersModule } from './displayers.module';
import { AuthModule } from '../auth/auth.module';
import {
  ReadDisplayerDto,
  CreateDisplayerDto,
  UpdateDisplayerDto,
  DisplayersPaginationDto,
  DisplayerCreatedDto,
  DisplayerUpdatedDto,
  DisplayerDeletedDto,
  UpdatedDisplayerResolutionDto,
} from './dto';
import { PaginationMetaDto, PaginationLinksDto } from '../common/dto';
import { UsersModule } from '../users/users.module';
import { ApplicationsModule } from '../applications/applications.module';
import { waitForJoined, selectRandomEnum } from '../helpers';

describe('Displayers (e2e)', () => {
  let app: INestApplication;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let server: any;
  let serverAddress: string;
  let configService: ConfigService;
  let jwt: string;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ScheduleModule.forRoot(),
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
        AuthModule,
        MessagesModule,
        UsersModule,
        ApplicationsModule,
        DisplayersModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    app.use(compression());

    app.use(helmet());

    app.enableCors();

    app.useGlobalPipes(
      new ValidationPipe({
        transform: true,
      }),
    );

    app.enableShutdownHooks();

    server = app.getHttpServer();
    configService = app.get<ConfigService>(ConfigService);

    const { address, port } = server.listen().address();

    serverAddress = `http://[${address}]:${port}`;

    await app.init();

    const res = await request(server)
      .post('/login')
      .send({
        username: configService.get<string>('DEFAULT_USERNAME'),
        password: configService.get<string>('DEFAULT_PASSWORD'),
      });

    jwt = res.body.jwt;
  });

  afterEach(async () => {
    await app.close();
  });

  it('cannot access to /displayers (GET) without a JWT', () => {
    return request(server).get('/displayers').expect(401);
  });

  it('cannot access to /displayers (POST) without a JWT', () => {
    return request(server).post('/displayers').expect(401);
  });

  it('should be able to add the new displayer on /displayers (POST)', async (done) => {
    const newDisplayer = new CreateDisplayerDto(
      'newDisplayer',
      'locationForNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const socket = connect(`${serverAddress}/displayers`, {
      forceNew: true,
      query: { jwt },
    });

    socket.on(
      WsDisplayerEvents.DISPLAYER_CREATED,
      (createdDisplayer: DisplayerCreatedDto) => {
        expect(createdDisplayer.id).toBeDefined();

        socket.close();

        done();
      },
    );

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      const res = await request(server)
        .post('/displayers')
        .set('Authorization', `Bearer ${jwt}`)
        .send(newDisplayer);

      expect(res.status).toEqual(201);

      expect(res.body.id).toBeDefined();
      expect(res.body.apiKey).toBeDefined();
      expect(res.body.name).toEqual(newDisplayer.name);
      expect(res.body.location).toEqual(newDisplayer.location);
      expect(res.body.width).toEqual(-1);
      expect(res.body.height).toEqual(-1);
      expect(res.body.role).toEqual(newDisplayer.role);
      expect(res.body.status).toEqual(newDisplayer.status);
      expect(res.body.createdAt).toBeDefined();
      expect(res.body.updatedAt).toBeDefined();
    });
  });

  it('adding a new displayer on /displayers (POST) with the same displayername should return a 409 error', async () => {
    const newDisplayer = new CreateDisplayerDto(
      'newDisplayer',
      'locationForNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    await request(server)
      .post('/displayers')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newDisplayer);

    return request(server)
      .post('/displayers')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newDisplayer)
      .expect(409);
  });

  it('adding a new displayer with malformed entries should return an 400 error', async () => {
    const res = await request(server)
      .post('/displayers')
      .set('Authorization', `Bearer ${jwt}`)
      .send(
        new CreateDisplayerDto(
          '',
          '',
          DisplayerRole.ACTIVE,
          DisplayerStatus.ENABLED,
        ),
      )
      .expect(400);

    expect(res.body).toEqual({
      statusCode: 400,
      message: [
        'name must be longer than or equal to 1 characters',
        'location must be longer than or equal to 1 characters',
      ],
      error: 'Bad Request',
    });
  });

  it('should be able to get the new displayer and have all the properties on /displayers/{displayerId} (GET)', async () => {
    const newDisplayer = new CreateDisplayerDto(
      'newDisplayer',
      'locationForNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const resCreatedDisplayer = await request(server)
      .post('/displayers')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newDisplayer);

    const resFoundDisplayer = await request(server)
      .get(`/displayers/${resCreatedDisplayer.body.id}`)
      .set('Authorization', `Bearer ${jwt}`)
      .expect(200);

    expect(resFoundDisplayer.body.id).toEqual(resCreatedDisplayer.body.id);
    expect(resFoundDisplayer.body).not.toHaveProperty('apiKey');
    expect(resFoundDisplayer.body.name).toEqual(resCreatedDisplayer.body.name);
    expect(resFoundDisplayer.body.location).toEqual(
      resCreatedDisplayer.body.location,
    );
    expect(resFoundDisplayer.body.width).toEqual(
      resCreatedDisplayer.body.width,
    );
    expect(resFoundDisplayer.body.height).toEqual(
      resCreatedDisplayer.body.height,
    );
    expect(resFoundDisplayer.body.role).toEqual(resCreatedDisplayer.body.role);
    expect(resFoundDisplayer.body.status).toEqual(
      resCreatedDisplayer.body.status,
    );
    expect(resFoundDisplayer.body.createdAt).toEqual(
      resCreatedDisplayer.body.createdAt,
    );
    expect(resFoundDisplayer.body.updatedAt).toEqual(
      resCreatedDisplayer.body.updatedAt,
    );
  });

  it('should return a 400 error when trying to get a displayer with a malformed UUID on /displayers/{displayerId} (GET)', async () => {
    return request(server)
      .get('/displayers/notuuid')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('should return a 404 error when trying to get an undefined displayer on /displayers/{displayerId} (GET)', async () => {
    return request(server)
      .get('/displayers/00000000-0000-0000-0000-000000000000')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(404);
  });

  it('should be able to update the displayer on /displayers/{displayerId} (PATCH)', async (done) => {
    const newDisplayer = new CreateDisplayerDto(
      'newDisplayer',
      'locationForNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const updateDisplayer = new UpdateDisplayerDto(
      'updatedDisplayer',
      'locationForUpdatedDisplayer',
      DisplayerRole.PASSIVE,
      DisplayerStatus.DISABLED,
    );

    const socket = connect(`${serverAddress}/displayers`, {
      forceNew: true,
      query: { jwt },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      const resCreatedDisplayer = await request(server)
        .post('/displayers')
        .set('Authorization', `Bearer ${jwt}`)
        .send(newDisplayer);

      socket.on(
        WsDisplayerEvents.DISPLAYER_UPDATED,
        (updatedDisplayer: DisplayerUpdatedDto) => {
          expect(updatedDisplayer.id).toEqual(resCreatedDisplayer.body.id);

          socket.close();

          done();
        },
      );

      const resUpdatedDisplayer = await request(server)
        .patch(`/displayers/${resCreatedDisplayer.body.id}`)
        .set('Authorization', `Bearer ${jwt}`)
        .send(updateDisplayer)
        .expect(200);

      expect(resUpdatedDisplayer.body.id).toEqual(resCreatedDisplayer.body.id);
      expect(resUpdatedDisplayer.body).not.toHaveProperty('apiKey');
      expect(resUpdatedDisplayer.body.name).toEqual(updateDisplayer.name);
      expect(resUpdatedDisplayer.body.location).toEqual(
        updateDisplayer.location,
      );
      expect(resUpdatedDisplayer.body.width).toEqual(
        resCreatedDisplayer.body.width,
      );
      expect(resUpdatedDisplayer.body.height).toEqual(
        resCreatedDisplayer.body.height,
      );
      expect(resUpdatedDisplayer.body.role).toEqual(updateDisplayer.role);
      expect(resUpdatedDisplayer.body.status).toEqual(updateDisplayer.status);
      expect(resUpdatedDisplayer.body.createdAt).toEqual(
        resCreatedDisplayer.body.createdAt,
      );
      expect(resUpdatedDisplayer.body.updatedAt).toBeDefined();
    });
  });

  it('should return a 400 error when trying to update a displayer with a malformed UUID on /displayers/{displayerId} (PATCH)', async () => {
    return request(server)
      .patch('/displayers/notuuid')
      .set('Authorization', `Bearer ${jwt}`)
      .send(
        new UpdateDisplayerDto(
          'updatedDisplayer',
          'locationForUpdatedDisplayer',
          DisplayerRole.PASSIVE,
          DisplayerStatus.DISABLED,
        ),
      )
      .expect(400);
  });

  it('should return a 404 error when trying to update an undefined displayer on /displayers/{displayerId} (PATCH)', async () => {
    return request(server)
      .patch('/displayers/00000000-0000-0000-0000-000000000000')
      .set('Authorization', `Bearer ${jwt}`)
      .send(
        new UpdateDisplayerDto(
          'updatedDisplayer',
          'locationForUpdatedDisplayer',
          DisplayerRole.PASSIVE,
          DisplayerStatus.DISABLED,
        ),
      )
      .expect(404);
  });

  it('should be able to revoke the displayer on /displayers/{displayerId}/revoke (PATCH)', async () => {
    const newDisplayer = new CreateDisplayerDto(
      'newDisplayer',
      'locationForNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const resOne = await request(server)
      .post('/displayers')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newDisplayer);

    const resTwo = await request(server)
      .patch(`/displayers/${resOne.body.id}/revoke`)
      .set('Authorization', `Bearer ${jwt}`);

    expect(resTwo.body.apiKey).not.toEqual(resOne.body.apiKey);
  });

  it('should return a 400 error when trying to update a displayer with a malformed UUID on /displayers/{displayerId}/revoke (PATCH)', async () => {
    return request(server)
      .patch('/displayers/notuuid/revoke')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('should return a 404 error when trying to update an undefined displayer on /displayers/{displayerId}/revoke (PATCH)', async () => {
    return request(server)
      .patch('/displayers/00000000-0000-0000-0000-000000000000/revoke')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(404);
  });

  it('should be able to delete the displayer on /displayers/{displayerId} (DELETE)', async (done) => {
    const newDisplayer = new CreateDisplayerDto(
      'newDisplayer',
      'locationForNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const socket = connect(`${serverAddress}/displayers`, {
      forceNew: true,
      query: { jwt },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      const resCreatedDisplayer = await request(server)
        .post('/displayers')
        .set('Authorization', `Bearer ${jwt}`)
        .send(newDisplayer);

      socket.on(
        WsDisplayerEvents.DISPLAYER_DELETED,
        (deletedDisplayer: DisplayerDeletedDto) => {
          expect(deletedDisplayer.id).toEqual(resCreatedDisplayer.body.id);

          socket.close();

          done();
        },
      );

      const resDeletedApplication = await request(server)
        .delete(`/displayers/${resCreatedDisplayer.body.id}`)
        .set('Authorization', `Bearer ${jwt}`)
        .expect(200);

      expect(resDeletedApplication.body.id).toEqual(
        resCreatedDisplayer.body.id,
      );
      expect(resDeletedApplication.body).not.toHaveProperty('apiKey');
      expect(resDeletedApplication.body.name).toEqual(
        resCreatedDisplayer.body.name,
      );
      expect(resDeletedApplication.body.location).toEqual(
        resCreatedDisplayer.body.location,
      );
      expect(resDeletedApplication.body.width).toEqual(
        resCreatedDisplayer.body.width,
      );
      expect(resDeletedApplication.body.height).toEqual(
        resCreatedDisplayer.body.height,
      );
      expect(resDeletedApplication.body.role).toEqual(
        resCreatedDisplayer.body.role,
      );
      expect(resDeletedApplication.body.status).toEqual(
        resCreatedDisplayer.body.status,
      );
      expect(resDeletedApplication.body.createdAt).toEqual(
        resCreatedDisplayer.body.createdAt,
      );
      expect(resDeletedApplication.body.updatedAt).toEqual(
        resCreatedDisplayer.body.updatedAt,
      );
    });
  });

  it('should return a 400 error when trying to delete a displayer with a malformed UUID on /displayers/{displayerId} (DELETE)', async () => {
    return request(server)
      .delete('/displayers/notuuid')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('should return a 404 error when trying to delete an undefined displayer on /displayers/{displayerId} (DELETE)', async () => {
    return request(server)
      .delete('/displayers/00000000-0000-0000-0000-000000000000')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(404);
  });

  it('should be able to get all displayers on /displayers (GET)', async () => {
    const page = 2;
    const limit = 10;

    const start = (page - 1) * limit;
    const end = start + limit;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const createdDisplayers: any[] = [];

    // eslint-disable-next-line no-plusplus
    for (let displayerNb = 0; displayerNb < 35; displayerNb++) {
      // eslint-disable-next-line no-await-in-loop
      const res = await request(server)
        .post('/displayers')
        .set('Authorization', `Bearer ${jwt}`)
        .send(
          new CreateDisplayerDto(
            `displayer${displayerNb}`,
            `displayer${displayerNb}`,
            selectRandomEnum(DisplayerRole),
            selectRandomEnum(DisplayerStatus),
          ),
        );

      createdDisplayers.push({
        id: res.body.id,
        name: res.body.name,
        location: res.body.location,
        width: -1,
        height: -1,
        role: res.body.role,
        status: res.body.status,
        updatedAt: res.body.updatedAt,
        createdAt: res.body.createdAt,
      });
    }

    createdDisplayers.sort(
      (displayer1: ReadDisplayerDto, displayer2: ReadDisplayerDto) =>
        displayer1.name.localeCompare(displayer2.name),
    );

    const res = await request(server)
      .get(`/displayers?page=${page}&limit=${limit}`)
      .set('Authorization', `Bearer ${jwt}`);

    const { items, meta, links } = res.body;

    const pagination = new DisplayersPaginationDto(
      items,
      new PaginationMetaDto(
        parseInt(meta.currentPage, 10),
        parseInt(meta.itemCount, 10),
        parseInt(meta.itemsPerPage, 10),
        parseInt(meta.totalItems, 10),
        parseInt(meta.totalPages, 10),
      ),
      new PaginationLinksDto(
        links.first,
        links.last,
        links.next,
        links.previous,
      ),
    );

    const results = new DisplayersPaginationDto(
      createdDisplayers.slice(start, end),
      new PaginationMetaDto(2, 10, 10, 35, 4),
      new PaginationLinksDto(
        '/displayers?limit=10',
        '/displayers?page=4&limit=10',
        '/displayers?page=3&limit=10',
        '/displayers?page=1&limit=10',
      ),
    );

    expect(pagination).toEqual(results);
  });

  it('should return a 400 error when trying to retrieve displayers page under 1 on /displayers (GET)', async () => {
    return request(server)
      .get('/displayers?page=0')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('should return a 400 error when trying to retrieve displayers limit under 1 on /displayers (GET)', async () => {
    return request(server)
      .get('/displayers?page=1&limit=0')
      .set('Authorization', `Bearer ${jwt}`)
      .expect(400);
  });

  it('cannot access to WS as an displayer without a JWT', async (done) => {
    const socket = connect(`${serverAddress}/displayers`, {
      forceNew: true,
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.EXCEPTION, async () => {
      socket.close();
      done();
    });
  });

  it('cannot access to WS as an displayer with a wrong JWT', async (done) => {
    const socket = connect(`${serverAddress}/displayers`, {
      forceNew: true,
      query: {
        jwt: 'wrongjwt',
      },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.EXCEPTION, async () => {
      socket.close();
      done();
    });
  });

  it('can access to WS as an displayer with a JWT', async (done) => {
    const socket = connect(`${serverAddress}/displayers`, {
      forceNew: true,
      query: { jwt },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsUserEvents.JOIN_AS_USER);
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      socket.close();
      done();
    });
  });

  it('cannot access to WS as an entity without an API key', async (done) => {
    const socket = connect(`${serverAddress}/displayers`, {
      forceNew: true,
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsDisplayerEvents.JOIN_AS_DISPLAYER);
    });

    socket.on(WsCommonEvents.EXCEPTION, async () => {
      socket.close();
      done();
    });
  });

  it('cannot access to WS as an entity with a wrong API key', async (done) => {
    const socket = connect(`${serverAddress}/displayers`, {
      forceNew: true,
      query: { apiKey: 'wrongapikey' },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsDisplayerEvents.JOIN_AS_DISPLAYER);
    });

    socket.on(WsCommonEvents.EXCEPTION, async () => {
      socket.close();
      done();
    });
  });

  it('can access to WS as an entity with an API key', async (done) => {
    const newDisplayer = new CreateDisplayerDto(
      'newDisplayer',
      'locationForNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const res = await request(server)
      .post('/displayers')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newDisplayer);

    const socket = connect(`${serverAddress}/displayers`, {
      forceNew: true,
      query: { apiKey: res.body.apiKey },
    });

    socket.on(WsCommonEvents.CONNECT, async () => {
      socket.emit(WsDisplayerEvents.JOIN_AS_DISPLAYER);
    });

    socket.on(WsCommonEvents.JOINED, async () => {
      socket.close();
      done();
    });
  });

  it('can update the resolution of the displayer through WebSockets', async (done) => {
    const newDisplayer = new CreateDisplayerDto(
      'newDisplayer',
      'locationForNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const resCreatedDisplayer = await request(server)
      .post('/displayers')
      .set('Authorization', `Bearer ${jwt}`)
      .send(newDisplayer);

    const adminSocket = connect(`${serverAddress}/displayers`, {
      forceNew: true,
      query: { jwt },
    });

    const displayerSocket = connect(`${serverAddress}/displayers`, {
      forceNew: true,
      query: { apiKey: resCreatedDisplayer.body.apiKey },
    });

    adminSocket.on(WsCommonEvents.CONNECT, async () => {
      adminSocket.emit(WsUserEvents.JOIN_AS_USER);
    });

    displayerSocket.on(WsCommonEvents.CONNECT, async () => {
      displayerSocket.emit(WsDisplayerEvents.JOIN_AS_DISPLAYER);
    });

    adminSocket.on(
      WsDisplayerEvents.DISPLAYER_UPDATED,
      (updatedDisplayer: DisplayerUpdatedDto) => {
        expect(updatedDisplayer.id).toEqual(resCreatedDisplayer.body.id);

        adminSocket.close();
        displayerSocket.close();

        done();
      },
    );

    await Promise.all([
      waitForJoined(adminSocket),
      waitForJoined(displayerSocket),
    ]);

    displayerSocket.emit(
      WsDisplayerEvents.UPDATED_DISPLAYER_RESOLUTION,
      new UpdatedDisplayerResolutionDto(1920, 1080),
    );
  });
});
