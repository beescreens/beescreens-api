import { Displayer } from '@beescreens/beescreens';

export type UpdateDisplayer = Omit<
  Displayer,
  'id' | 'apiKey' | 'width' | 'height' | 'createdAt' | 'updatedAt'
>;
