import { Displayer } from '@beescreens/beescreens';

export type ReadDisplayer = Omit<Displayer, 'apiKey'>;
