import { Displayer } from '@beescreens/beescreens';

export type UpdatedDisplayerResolution = Pick<Displayer, 'width' | 'height'>;
