import { Displayer } from '@beescreens/beescreens';

export type DisplayerUpdated = Omit<Displayer, 'apiKey'>;
