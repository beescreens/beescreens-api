import { Displayer } from '@beescreens/beescreens';

export type DisplayerCreated = Displayer;
