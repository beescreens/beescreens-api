import { Displayer } from '@beescreens/beescreens';

export type CreateDisplayer = Omit<
  Displayer,
  'id' | 'apiKey' | 'width' | 'height' | 'createdAt' | 'updatedAt'
>;
