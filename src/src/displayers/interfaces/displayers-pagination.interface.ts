import { ReadDisplayer } from './read-displayer.interface';
import { PaginationMeta, PaginationLinks } from '../../common/interfaces';

export interface DisplayersPagination {
  readonly items: ReadDisplayer[];
  readonly meta: PaginationMeta;
  readonly links: PaginationLinks;
}
