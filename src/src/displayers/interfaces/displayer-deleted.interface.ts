import { Displayer } from '@beescreens/beescreens';

export type DisplayerDeleted = Omit<Displayer, 'apiKey'>;
