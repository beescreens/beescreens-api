import { ApiParamOptions } from '@nestjs/swagger';

export const DisplayerIdApiParameterSpecs: ApiParamOptions = {
  name: 'displayerId',
  description: 'The displayer ID.',
};
