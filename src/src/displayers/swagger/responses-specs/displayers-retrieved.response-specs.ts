import { ApiResponseOptions } from '@nestjs/swagger';
import { DisplayersPaginationDto } from '../../dto';

export const DisplayersRetrievedResponseSpecs: ApiResponseOptions = {
  description: 'Displayers have been successfully retrieved.',
  type: DisplayersPaginationDto,
};
