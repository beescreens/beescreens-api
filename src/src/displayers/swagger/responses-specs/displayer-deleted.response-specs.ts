import { ApiResponseOptions } from '@nestjs/swagger';
import { DisplayerDeletedDto } from '../../dto';

export const DisplayerDeletedResponseSpecs: ApiResponseOptions = {
  description: 'Displayer has been successfully deleted.',
  type: DisplayerDeletedDto,
};
