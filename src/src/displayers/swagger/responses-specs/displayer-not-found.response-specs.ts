import { ApiResponseOptions } from '@nestjs/swagger';

export const DisplayerNotFoundResponseSpecs: ApiResponseOptions = {
  description: 'Displayer has not been found.',
};
