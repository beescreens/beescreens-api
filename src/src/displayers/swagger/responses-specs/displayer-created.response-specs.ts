import { ApiResponseOptions } from '@nestjs/swagger';
import { DisplayerCreatedDto } from '../../dto';
import { DisplayersLinksApiCommon } from '../common';

export const DisplayerCreatedResponseSpecs: ApiResponseOptions = {
  description:
    'Displayer has been successfully created. The `apiKey` is only displayed once.',
  type: DisplayerCreatedDto,
  links: DisplayersLinksApiCommon,
};
