import { ApiResponseOptions } from '@nestjs/swagger';
import { EntityApiKeyDto } from '../../../common/dto';
import { JoinAsDisplayerLinkSpecs } from '../links-specs';

export const DisplayerApiKeyResponseSpecs: ApiResponseOptions = {
  description:
    'The Displayer API key has been successfully revoked. The API key is only displayed once.',
  type: EntityApiKeyDto,
  links: {
    JoinAsDisplayer: JoinAsDisplayerLinkSpecs,
  },
};
