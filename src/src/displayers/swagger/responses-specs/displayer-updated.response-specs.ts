import { ApiResponseOptions } from '@nestjs/swagger';
import { DisplayerUpdatedDto } from '../../dto';

export const DisplayerUpdatedResponseSpecs: ApiResponseOptions = {
  description: 'Displayer has been successfully updated.',
  type: DisplayerUpdatedDto,
};
