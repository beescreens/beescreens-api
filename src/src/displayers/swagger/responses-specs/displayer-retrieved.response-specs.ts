import { ApiResponseOptions } from '@nestjs/swagger';
import { ReadDisplayerDto } from '../../dto';

export const DisplayerRetrievedResponseSpecs: ApiResponseOptions = {
  description: 'Displayer has been successfully retrieved.',
  type: ReadDisplayerDto,
};
