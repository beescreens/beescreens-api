import { ApiResponseOptions } from '@nestjs/swagger';
import { CreateDisplayerWithOtherNameLinkSpecs } from '../links-specs';

export const DisplayerConflictResponseSpecs: ApiResponseOptions = {
  description:
    'Another displayer has the same name. Please try again with another displayername.',
  links: {
    AddDisplayerWithOtherName: CreateDisplayerWithOtherNameLinkSpecs,
  },
};
