import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import {
  GetDisplayerByIdLinkSpecs,
  DeleteDisplayerByIdLinkSpecs,
  UpdateDisplayerByIdLinkSpecs,
  RevokeDisplayerByIdLinkSpecs,
} from '../links-specs';

export const DisplayersLinksApiCommon: Record<string, LinkObject> = {
  GetDisplayerById: GetDisplayerByIdLinkSpecs,
  DeleteDisplayerById: DeleteDisplayerByIdLinkSpecs,
  UpdateDisplayerById: UpdateDisplayerByIdLinkSpecs,
  RevokeDisplayerById: RevokeDisplayerByIdLinkSpecs,
};
