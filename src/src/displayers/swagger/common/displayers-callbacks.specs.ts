import {
  PathItemObject,
  ReferenceObject,
} from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import {
  OnJoinedCallbackSpecs,
  OnExceptionCallbackSpecs,
  OnDisconnectCallbackSpecs,
} from '../../../common/swagger/callbacks-specs';
import { OnMessageCallbackSpecs } from '../../../messages/swagger/callbacks-specs';
import {
  OnDisplayerCreatedCallbackSpecs,
  OnDisplayerUpdatedCallbackSpecs,
  OnDisplayerDeletedCallbackSpecs,
  EmitUpdatedDisplayerResolutionCallbackSpecs,
  OnConnectDisplayerCallbackSpecs,
  EmitJoinAsDisplayerCallbackSpecs,
} from '../callbacks-specs';
import { EmitJoinAsUserCallbackSpecs } from '../../../users/swagger/callbacks-specs';

export const DisplayersCallbacksApiCommon: Record<
  string,
  ReferenceObject | Record<string, PathItemObject>
> = {
  'WebSockets (common)': {
    onConnect: OnConnectDisplayerCallbackSpecs,
    onJoined: OnJoinedCallbackSpecs,
    onException: OnExceptionCallbackSpecs,
    onDisconnect: OnDisconnectCallbackSpecs,
    onMessage: OnMessageCallbackSpecs,
  },
  'WebSockets (as user)': {
    emitJoinAsUser: EmitJoinAsUserCallbackSpecs,
    onDisplayerCreated: OnDisplayerCreatedCallbackSpecs,
    onDisplayerUpdated: OnDisplayerUpdatedCallbackSpecs,
    onDisplayerDeleted: OnDisplayerDeletedCallbackSpecs,
  },
  'WebSockets (as displayer)': {
    emitJoinAsDisplayer: EmitJoinAsDisplayerCallbackSpecs,
    emitUpdatedDisplayerResolution: EmitUpdatedDisplayerResolutionCallbackSpecs,
  },
};
