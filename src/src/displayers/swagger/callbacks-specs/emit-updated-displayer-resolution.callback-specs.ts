import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const EmitUpdatedDisplayerResolutionCallbackSpecs: PathItemObject = {
  post: {
    summary:
      "The displayer's resolution has changed and can notify the WebSocket server",
    description:
      "The displayer's resolution has changed and can notify the WebSocket server.",
    operationId: 'emitUpdatedDisplayerResolution',
    responses: {
      'N/A': {
        description: "The displayer's resolution has been updated.",
      },
    },
  },
};
