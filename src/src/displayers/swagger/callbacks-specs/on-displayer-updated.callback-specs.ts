import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const OnDisplayerUpdatedCallbackSpecs: PathItemObject = {
  get: {
    summary: 'An updated displayer has been emitted by the WebSocket server',
    description:
      'An updated displayer has been emitted by the WebSocket server.',
    operationId: 'onDisplayerUpdated',
    responses: {
      'N/A': {
        description:
          'An updated displayer has been emitted by the WebSocket server.',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: {
                  description: 'ID of the entity',
                  type: 'string',
                  format: 'uuid',
                },
              },
            },
          },
        },
      },
    },
  },
};
