import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { DisplayersLinksApiCommon } from '../common';

export const OnDisplayerCreatedCallbackSpecs: PathItemObject = {
  get: {
    summary: 'A new displayer has been emitted by the WebSocket server',
    description: 'A new displayer has been emitted by the WebSocket server.',
    operationId: 'onDisplayerCreated',
    responses: {
      'N/A': {
        description:
          'A new displayer has been emitted by the WebSocket server.',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: {
                  description: 'ID of the entity',
                  type: 'string',
                  format: 'uuid',
                },
              },
            },
          },
        },
        links: DisplayersLinksApiCommon,
      },
    },
  },
};
