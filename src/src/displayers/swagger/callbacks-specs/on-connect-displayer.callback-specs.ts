import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { JoinAsUserLinkSpecs } from '../../../common/swagger/links-specs';
import { JoinAsDisplayerLinkSpecs } from '../links-specs';

export const OnConnectDisplayerCallbackSpecs: PathItemObject = {
  get: {
    summary: 'The connection to the server has been established',
    description: 'The connection to the server has been established.',
    operationId: 'onConnectDisplayer',
    responses: {
      'N/A': {
        description: 'The connection to the server has been established.',
        links: {
          joinAsUser: JoinAsUserLinkSpecs,
          joinAsDisplayer: JoinAsDisplayerLinkSpecs,
        },
      },
    },
  },
};
