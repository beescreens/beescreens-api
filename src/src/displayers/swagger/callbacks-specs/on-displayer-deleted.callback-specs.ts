import { PathItemObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const OnDisplayerDeletedCallbackSpecs: PathItemObject = {
  get: {
    summary: 'A deleted displayer has been emitted by the WebSocket server',
    description:
      'A deleted displayer has been emitted by the WebSocket server.',
    operationId: 'onDisplayerDeleted',
    responses: {
      'N/A': {
        description:
          'A deleted displayer has been emitted by the WebSocket server.',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: {
                  description: 'ID of the entity',
                  type: 'string',
                  format: 'uuid',
                },
              },
            },
          },
        },
      },
    },
  },
};
