import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { DisplayersCallbacksApiCommon } from '../common';

export const GetDisplayerOperationSpecs: Partial<OperationObject> = {
  summary: 'Get the specified displayer',
  description: 'Get the specified displayer.',
  operationId: 'getDisplayer',
  callbacks: DisplayersCallbacksApiCommon,
};
