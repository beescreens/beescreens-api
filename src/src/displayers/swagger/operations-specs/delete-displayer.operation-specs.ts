import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { DisplayersCallbacksApiCommon } from '../common';

export const DeleteDisplayerOperationSpecs: Partial<OperationObject> = {
  summary: 'Delete the specified displayer',
  description: 'Delete the specified displayer.',
  operationId: 'deleteDisplayer',
  callbacks: DisplayersCallbacksApiCommon,
};
