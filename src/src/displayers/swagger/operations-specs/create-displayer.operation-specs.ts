import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { DisplayersCallbacksApiCommon } from '../common';

export const CreateDisplayerOperationSpecs: Partial<OperationObject> = {
  summary: 'Create a new displayer',
  description: 'Create a new displayer.',
  operationId: 'createDisplayer',
  callbacks: DisplayersCallbacksApiCommon,
};
