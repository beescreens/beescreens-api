import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { DisplayersCallbacksApiCommon } from '../common';

export const RevokeDisplayerOperationSpecs: Partial<OperationObject> = {
  summary: 'Revoke the specified displayer',
  description: 'Revoke the specified displayer.',
  operationId: 'revokeDisplayer',
  callbacks: DisplayersCallbacksApiCommon,
};
