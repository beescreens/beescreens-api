import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { DisplayersCallbacksApiCommon } from '../common';

export const UpdateDisplayerOperationSpecs: Partial<OperationObject> = {
  summary: 'Update the specified displayer',
  description: 'Update the specified displayer.',
  operationId: 'updateDisplayer',
  callbacks: DisplayersCallbacksApiCommon,
};
