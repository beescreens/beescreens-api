import { OperationObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { DisplayersCallbacksApiCommon } from '../common';

export const GetDisplayersOperationSpecs: Partial<OperationObject> = {
  summary: 'Get the displayers',
  description: 'Get the displayers.',
  operationId: 'getDisplayers',
  callbacks: DisplayersCallbacksApiCommon,
};
