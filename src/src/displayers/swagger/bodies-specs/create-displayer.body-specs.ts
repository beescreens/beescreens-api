import { ApiBodyOptions } from '@nestjs/swagger';
import { CreateDisplayerDto } from '../../dto';

export const CreateDisplayerBodySpecs: ApiBodyOptions = {
  description: "The displayer's details.",
  type: CreateDisplayerDto,
};
