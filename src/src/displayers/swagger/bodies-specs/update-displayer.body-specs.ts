import { ApiBodyOptions } from '@nestjs/swagger';
import { UpdateDisplayerDto } from '../../dto';

export const UpdateDisplayerBodySpecs: ApiBodyOptions = {
  description: "The displayer's details.",
  type: UpdateDisplayerDto,
};
