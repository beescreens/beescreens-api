import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const DeleteDisplayerByIdLinkSpecs: LinkObject = {
  description:
    'The `id` value returned in the response can be used as the `displayerId` parameter in `DELETE /displayers/{displayerId}`.',
  operationId: 'DELETE /displayers/{displayerId}',
  parameters: { displayerId: '$response.body#/id' },
};
