export * from './create-displayer-with-other-name.link-specs';
export * from './delete-displayer-by-id.link-specs';
export * from './get-displayer-by-id.link-specs';
export * from './join-as-displayer.link-specs';
export * from './revoke-displayer-by-id.link-specs';
export * from './update-displayer-by-id.link-specs';
