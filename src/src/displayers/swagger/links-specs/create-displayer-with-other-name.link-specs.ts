import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const CreateDisplayerWithOtherNameLinkSpecs: LinkObject = {
  description: 'The `name` must be changed in `POST /displayers`.',
  operationId: 'POST /displayers',
  requestBody: {
    name: '$response.body#/name',
    location: '$response.body#/location',
    role: '$response.body#/role',
    status: '$response.body#/status',
  },
};
