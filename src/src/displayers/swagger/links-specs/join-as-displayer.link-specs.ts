import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const JoinAsDisplayerLinkSpecs: LinkObject = {
  description:
    'The `apiKey` value returned in the response can be used to join the WebSockets server in `emitJoinAsDisplayer`.',
  operationId: 'emitJoinAsDisplayer',
  parameters: { apiKey: '$response.body#/apiKey' },
};
