import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const UpdateDisplayerByIdLinkSpecs: LinkObject = {
  description:
    'The `id` value returned in the response can be used as the `displayerId` parameter in `PATCH /displayers/{displayerId}`.',
  operationId: 'PATCH /displayers/{displayerId}',
  parameters: { displayerId: '$response.body#/id' },
};
