import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const RevokeDisplayerByIdLinkSpecs: LinkObject = {
  description:
    'The `id` value returned in the response can be used as the `displayerId` parameter in `PATCH /displayers/{displayerId}/revoke`.',
  operationId: 'PATCH /displayers/{displayerId}/revoke',
  parameters: { displayerId: '$response.body#/id' },
};
