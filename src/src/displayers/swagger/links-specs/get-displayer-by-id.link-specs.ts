import { LinkObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export const GetDisplayerByIdLinkSpecs: LinkObject = {
  description:
    'The `id` value returned in the response can be used as the `displayerId` parameter in `GET /displayers/{displayerId}`.',
  operationId: 'GET /displayers/{displayerId}',
  parameters: { displayerId: '$response.body#/id' },
};
