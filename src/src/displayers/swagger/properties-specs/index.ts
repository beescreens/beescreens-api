export * from './displayer-height.property-specs';
export * from './displayer-location.property-specs';
export * from './displayers.property-specs';
export * from './displayer-role.property-specs';
export * from './displayer-status.property-specs';
export * from './displayer-width.property-specs';
export * from './displayer-name.property-specs';
