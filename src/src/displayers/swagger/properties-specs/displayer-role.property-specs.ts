import { ApiPropertyOptions } from '@nestjs/swagger';
import { DisplayerRole } from '@beescreens/beescreens';

export const DisplayerRolePropertySpecs: ApiPropertyOptions = {
  description: 'Role of the displayer',
  type: 'string',
  enum: DisplayerRole,
};
