import { ApiPropertyOptions } from '@nestjs/swagger';

export const DisplayerHeightPropertySpecs: ApiPropertyOptions = {
  description: 'Height of the displayer (`-1` meaning unknown)',
  type: 'integer',
};
