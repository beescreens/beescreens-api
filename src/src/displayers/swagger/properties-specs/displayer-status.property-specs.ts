import { ApiPropertyOptions } from '@nestjs/swagger';
import { DisplayerStatus } from '@beescreens/beescreens';

export const DisplayerStatusPropertySpecs: ApiPropertyOptions = {
  description: 'Status of the displayer',
  type: 'string',
  enum: DisplayerStatus,
};
