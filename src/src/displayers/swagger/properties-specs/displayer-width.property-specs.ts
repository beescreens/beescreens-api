import { ApiPropertyOptions } from '@nestjs/swagger';

export const DisplayerWidthPropertySpecs: ApiPropertyOptions = {
  description: 'Width of the displayer (`-1` meaning unknown)',
  type: 'integer',
};
