import { ApiPropertyOptions } from '@nestjs/swagger';
import { ReadDisplayerDto } from '../../dto';

export const DisplayersPropertySpecs: ApiPropertyOptions = {
  description: 'The retrieved displayers.',
  type: () => [ReadDisplayerDto],
};
