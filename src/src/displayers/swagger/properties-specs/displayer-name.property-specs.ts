import { ApiPropertyOptions } from '@nestjs/swagger';

export const DisplayerNamePropertySpecs: ApiPropertyOptions = {
  description: 'Name of the displayer',
  type: 'string',
  minLength: 1,
};
