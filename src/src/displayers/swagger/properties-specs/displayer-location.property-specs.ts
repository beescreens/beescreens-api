import { ApiPropertyOptions } from '@nestjs/swagger';

export const DisplayerLocationPropertySpecs: ApiPropertyOptions = {
  description: 'Location of the displayer',
  type: 'string',
  minLength: 1,
};
