import { Test, TestingModule } from '@nestjs/testing';
import {
  ConflictException,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { transports } from 'winston';
import { DisplayerRole, DisplayerStatus } from '@beescreens/beescreens';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { DisplayersController } from './displayers.controller';
import {
  ReadDisplayerDto,
  CreateDisplayerDto,
  UpdateDisplayerDto,
  DisplayersPaginationDto,
} from './dto';
import { DisplayersService } from './displayers.service';
import { PaginationMetaDto, PaginationLinksDto } from '../common/dto';
import { DisplayersGateway } from './displayers.gateway';
import { DisplayerEntity } from './entities';
import { selectRandomEnum } from '../helpers';

describe('DisplayersController', () => {
  let app: TestingModule;
  let displayersController: DisplayersController;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'sqlite',
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: true,
          database: ':memory:',
        }),
        TypeOrmModule.forFeature([DisplayerEntity]),
        WinstonModule.forRoot({
          transports: [
            new transports.Console({
              silent: true,
            }),
          ],
        }),
      ],
      controllers: [DisplayersController],
      providers: [DisplayersService, DisplayersGateway],
    }).compile();

    displayersController = app.get<DisplayersController>(DisplayersController);
  });

  afterEach(async () => {
    await app.close();
  });

  it('should be able to add the new displayer', async () => {
    const newDisplayer = new CreateDisplayerDto(
      'newDisplayer',
      'locationForNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const createdDisplayer = await displayersController.createDisplayer(
      newDisplayer,
    );

    expect(createdDisplayer.id).toBeDefined();
    expect(createdDisplayer.apiKey).toBeDefined();
    expect(createdDisplayer.name).toEqual(newDisplayer.name);
    expect(createdDisplayer.location).toEqual(newDisplayer.location);
    expect(createdDisplayer.width).toEqual(-1);
    expect(createdDisplayer.height).toEqual(-1);
    expect(createdDisplayer.role).toEqual(newDisplayer.role);
    expect(createdDisplayer.status).toEqual(newDisplayer.status);
    expect(createdDisplayer.createdAt).toBeDefined();
    expect(createdDisplayer.updatedAt).toBeDefined();
  });

  it('adding a new displayer with the same displayername should throw an error', async () => {
    const newDisplayer = new CreateDisplayerDto(
      'newDisplayer',
      'locationForNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    try {
      await displayersController.createDisplayer(newDisplayer);
      await displayersController.createDisplayer(newDisplayer);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(ConflictException);
    }
  });

  it('should be able to get the new displayer and have all the properties', async () => {
    const newDisplayer = new CreateDisplayerDto(
      'newDisplayer',
      'locationForNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const createdDisplayer = await displayersController.createDisplayer(
      newDisplayer,
    );

    const foundDisplayer = await displayersController.getDisplayer(
      createdDisplayer.id,
    );

    expect(foundDisplayer.id).toEqual(createdDisplayer.id);
    expect(foundDisplayer).not.toHaveProperty('apiKey');
    expect(foundDisplayer.name).toEqual(createdDisplayer.name);
    expect(foundDisplayer.location).toEqual(createdDisplayer.location);
    expect(foundDisplayer.width).toEqual(createdDisplayer.width);
    expect(foundDisplayer.height).toEqual(createdDisplayer.height);
    expect(foundDisplayer.role).toEqual(createdDisplayer.role);
    expect(foundDisplayer.status).toEqual(createdDisplayer.status);
    expect(foundDisplayer.createdAt).toEqual(createdDisplayer.createdAt);
    expect(foundDisplayer.updatedAt).toEqual(createdDisplayer.updatedAt);
  });

  it('should throw an error when trying to get an undefined displayer', async () => {
    try {
      await displayersController.getDisplayer('404');
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to update the displayer', async () => {
    const newDisplayer = new CreateDisplayerDto(
      'newDisplayer',
      'locationForNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const updateDisplayer = new UpdateDisplayerDto(
      'updatedDisplayer',
      'locationUpdatedDisplayer',
      DisplayerRole.PASSIVE,
      DisplayerStatus.DISABLED,
    );

    const createdDisplayer = await displayersController.createDisplayer(
      newDisplayer,
    );

    const updatedDisplayer = await displayersController.updateDisplayer(
      createdDisplayer.id,
      updateDisplayer,
    );

    expect(updatedDisplayer.id).toEqual(createdDisplayer.id);
    expect(updatedDisplayer).not.toHaveProperty('apiKey');
    expect(updatedDisplayer.name).toEqual(updateDisplayer.name);
    expect(updatedDisplayer.location).toEqual(updateDisplayer.location);
    expect(updatedDisplayer.width).toEqual(createdDisplayer.width);
    expect(updatedDisplayer.height).toEqual(createdDisplayer.width);
    expect(updatedDisplayer.role).toEqual(updateDisplayer.role);
    expect(updatedDisplayer.status).toEqual(updateDisplayer.status);
    expect(updatedDisplayer.createdAt).toEqual(createdDisplayer.createdAt);
    expect(updatedDisplayer.updatedAt).toBeDefined();
  });

  it('should throw an error when trying to update an undefined displayer', async () => {
    try {
      await displayersController.updateDisplayer(
        '404',
        new UpdateDisplayerDto(
          'newDisplayer',
          'locationForNewDisplayer',
          DisplayerRole.ACTIVE,
          DisplayerStatus.ENABLED,
        ),
      );
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to revoke the displayer', async () => {
    const newDisplayer = new CreateDisplayerDto(
      'newDisplayer',
      'locationForNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const createdDisplayer = await displayersController.createDisplayer(
      newDisplayer,
    );

    const revokedDisplayer = await displayersController.revokeDisplayer(
      createdDisplayer.id,
    );

    expect(revokedDisplayer.apiKey).not.toEqual(createdDisplayer.apiKey);
  });

  it('should throw an error when trying to revoke an undefined displayer', async () => {
    try {
      await displayersController.revokeDisplayer('404');
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to delete the displayer', async () => {
    const newDisplayer = new CreateDisplayerDto(
      'newDisplayer',
      'locationForNewDisplayer',
      DisplayerRole.ACTIVE,
      DisplayerStatus.ENABLED,
    );

    const createdDisplayer = await displayersController.createDisplayer(
      newDisplayer,
    );

    const deletedDisplayer = await displayersController.deleteDisplayer(
      createdDisplayer.id,
    );

    expect(deletedDisplayer.id).toEqual(createdDisplayer.id);
    expect(deletedDisplayer).not.toHaveProperty('apiKey');
    expect(deletedDisplayer.name).toEqual(createdDisplayer.name);
    expect(deletedDisplayer.location).toEqual(createdDisplayer.location);
    expect(deletedDisplayer.width).toEqual(createdDisplayer.width);
    expect(deletedDisplayer.height).toEqual(createdDisplayer.width);
    expect(deletedDisplayer.role).toEqual(createdDisplayer.role);
    expect(deletedDisplayer.status).toEqual(createdDisplayer.status);
    expect(deletedDisplayer.createdAt).toEqual(createdDisplayer.createdAt);
    expect(deletedDisplayer.updatedAt).toEqual(createdDisplayer.updatedAt);
  });

  it('should throw an error when trying to delete an undefined displayer', async () => {
    try {
      await displayersController.deleteDisplayer('404');
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
    }
  });

  it('should be able to get all displayers', async () => {
    const page = 2;
    const limit = 10;

    const start = (page - 1) * limit;
    const end = start + limit;

    const createdDisplayers: ReadDisplayerDto[] = [];

    // eslint-disable-next-line no-plusplus
    for (let displayerNb = 0; displayerNb < 35; displayerNb++) {
      // eslint-disable-next-line no-await-in-loop
      const createdDisplayer = await displayersController.createDisplayer(
        new CreateDisplayerDto(
          `displayer${displayerNb}`,
          `displayer${displayerNb}`,
          selectRandomEnum<DisplayerRole>(DisplayerRole),
          selectRandomEnum<DisplayerStatus>(DisplayerStatus),
        ),
      );

      createdDisplayers.push(
        new ReadDisplayerDto(
          createdDisplayer.id,
          createdDisplayer.name,
          createdDisplayer.location,
          -1,
          -1,
          createdDisplayer.role,
          createdDisplayer.status,
          createdDisplayer.createdAt,
          createdDisplayer.updatedAt,
        ),
      );
    }

    createdDisplayers.sort(
      (displayer1: ReadDisplayerDto, displayer2: ReadDisplayerDto) =>
        displayer1.name.localeCompare(displayer2.name),
    );

    const pagination = await displayersController.getDisplayers(page, limit);

    const results = new DisplayersPaginationDto(
      createdDisplayers.slice(start, end),
      new PaginationMetaDto(2, 10, 10, 35, 4),
      new PaginationLinksDto(
        '/displayers?limit=10',
        '/displayers?page=4&limit=10',
        '/displayers?page=3&limit=10',
        '/displayers?page=1&limit=10',
      ),
    );

    expect(pagination).toEqual(results);
  });

  it('should throw an error when trying to retrieve displayers page under 1', async () => {
    try {
      await displayersController.getDisplayers(0);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(BadRequestException);
    }
  });

  it('should throw an error when trying to retrieve displayers limit under 1', async () => {
    try {
      await displayersController.getDisplayers(1, 0);
      throw new Error();
    } catch (error) {
      expect(error).toBeInstanceOf(BadRequestException);
    }
  });
});
