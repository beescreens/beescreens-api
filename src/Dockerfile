# Base image as builder
FROM mhart/alpine-node:14 AS builder

# Work directory
WORKDIR /app

# Required packages
RUN apk add --no-cache \
    python2 \
    g++ \
    make

# Copy the package.json
COPY .npmrc .npmrc
COPY package.json package.json
COPY package-lock.json package-lock.json

# Install the dependencies
RUN npm install

# Copy the source files of the app
COPY src src
COPY .babel.config.js .babel.config.js
COPY tsconfig.build.json tsconfig.build.json
COPY tsconfig.json tsconfig.json

# Build the app
RUN npm run build

# Base image as staging
FROM mhart/alpine-node:latest AS staging

# Work directory
WORKDIR /app

# Required packages
RUN apk add --no-cache \
    python2 \
    g++ \
    make

# Copy the files from the build stage
COPY --from=builder /app/dist dist
COPY --from=builder /app/.npmrc .npmrc
COPY --from=builder /app/package.json package.json
COPY --from=builder /app/package-lock.json package-lock.json

# Install the dependencies
RUN npm install --only=prod

# Base image as production
FROM mhart/alpine-node:14 AS production

# Work directory
WORKDIR /app

# Copy the files from the staging stage
COPY --from=staging /app/dist dist
COPY --from=staging /app/.npmrc .npmrc
COPY --from=staging /app/node_modules node_modules
COPY --from=staging /app/package.json package.json
COPY --from=staging /app/package-lock.json package-lock.json

# Environment variables
ENV CONSOLE_LOGGING_LEVEL=${CONSOLE_LOGGING_LEVEL}
ENV FILE_LOGGING_LEVEL=${FILE_LOGGING_LEVEL}
ENV JWT_SECRET=${JWT_SECRET}
ENV JWT_EXPIRE_TIME=${JWT_EXPIRE_TIME}
ENV DEFAULT_USERNAME=${DEFAULT_USERNAME}
ENV DEFAULT_PASSWORD=${DEFAULT_PASSWORD}

# Volumes
VOLUME [ "/app/data" ]

# Exposed ports
EXPOSE 63350

CMD ["node", "dist/main.js"]
