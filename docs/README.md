# BeeScreens API
[![Pipeline Status][pipeline-status-badge]][pipeline-status-url]
[![Coverage Report][coverage-report-badge]][coverage-report-url]
[![License][license-badge]][license-url]
[![Code of Conduct][code-of-conduct-badge]][code-of-conduct-url]
[![Keep a Changelog v1.1.0 badge][changelog-badge]][changelog-url]
[![BeeScreens' Telegram Channel][telegram-channel-badge]][telegram-channel-url]
[![BeeScreens' Telegram Community][telegram-community-badge]][telegram-community-url]

## Introduction

BeeScreens is a framework allowing any third-party developpers to develop new interactive applications to be streamed over the Internet from one media (i.e. a phone, computer, etc) to any other media both only using a modern Web Browser. See the [BeeScreens official website][beescreens-url] for more details on how BeeScreens works!

## Run BeeScreens API

There are two methods to run the BeeScreens API:

- Using `docker` without installing any other programs (recommended method)
- Locally with all prerequisites

### Using `docker`

*Please refer to the [BeeScreens Docker Files website][docker-files-website] for details on how to use this repository with `docker`.*

The API can be started with:

```sh
docker-compose up beescreens-api
```

### Locally

These instructions will get you a copy of the project up and running on your computer.

#### Prerequisites

- [`git`](https://git-scm.com/) must be installed.
- [`Node.js`](https://nodejs.org) must be installed.

#### Clone the repository and configure the environment variables

```sh
# Clone the repository
git clone https://gitlab.com/beescreens/beescreens-api.git

# Move to the cloned directory
cd beescreens-api/src

# Install the dependencies
npm install

# Copy the environment variables file
cp .env.example .env

# Edit the environment variables file
vim .env
```

#### Start the API

```sh
# Install all the dependencies
npm install

# Start the API
npm start
```

The API is now running and can be accessed on [http://localhost:63350](http://localhost:63350)

## Technologies used

The BeeScreens API uses the following technologies:

- [Node.js](https://nodejs.org/) - Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine
- [Nest](https://nestjs.com/) - A progressive Node.js framework for building efficient, reliable and scalable server-side applications
- [TypeScript](https://www.typescriptlang.org/) - TypeScript is a typed superset of JavaScript that compiles to plain JavaScript
- [Socket.IO](https://socket.io/) - Real-time, bidirectional and event-based communication between the browser and the server
- [OpenAPI (Swagger)](https://swagger.io/specification/) - Defines a standard, language-agnostic interface to RESTful APIs
- [TypeORM](https://typeorm.io/) - Amazing ORM for TypeScript and JavaScript
- [class-validator](https://github.com/typestack/class-validator)
- [Passport.js](http://www.passportjs.org/) - Validation made easy using TypeScript decorators
- [compression](https://github.com/expressjs/compression) - Compression middleware
- [helmet](https://github.com/helmetjs/helmet) - Help secure Express apps with various HTTP headers
- [CORS](https://github.com/expressjs/cors) - CORS middleware
- [winston](https://www.npmjs.com/package/winston) - Simple and universal logging library with support for multiple transports
- [ESLint](https://eslint.org/) - Find and fix problems in your JavaScript code
- [Prettier](https://prettier.io/) - An opinionated code formatter
- [Jest](https://jestjs.io/) - Delightful JavaScript Testing Framework with a focus on simplicity
- [Husky](https://www.npmjs.com/package/husky) - Husky can prevent bad git commit, git push and more

## Changelog

Changelog can be found in the [CHANGELOG.md][changelog-url] file.

## License

This project is free and will always be. The source code is licensed under the MIT License - see the [LICENSE.md][license-url]
file for details.

## Contributing

Thank you for considering contributing to BeeScreens! Here is some help to get you started to contribute to the project:

1. Please start by reading our code of conduct available in the [CODE_OF_CONDUCT.md][code-of-conduct-url] file.
2. All contribution information is available in the [CONTRIBUTING.md][contributor-url] file.

Feel free to contribute to the project in any way that you can think of, your contributions are more than welcome!

## Want to reach us?

BeeScreens has the following main channels to communicate:

- [GitLab][repository-url], using [issues][issue-url]
- [BeeScreens' Telegram Channel][telegram-channel-url], where updates are sent when someone is working on BeeScreens
- [BeeScreens' Telegram Community][telegram-community-url], group where discussions are allowed to ask anything related to BeeScreens

Feel free to use any of the communication channels to reach us!

[pipeline-status-badge]: https://gitlab.com/beescreens/beescreens-api/badges/master/pipeline.svg
[pipeline-status-url]: https://gitlab.com/beescreens/beescreens-api/commits/master

[coverage-report-badge]: https://gitlab.com/beescreens/beescreens-api/badges/master/coverage.svg
[coverage-report-url]: https://gitlab.com/beescreens/beescreens-api/commits/master

[license-badge]: https://img.shields.io/badge/license-MIT-blue.svg
[license-url]: https://gitlab.com/beescreens/beescreens-api/blob/master/LICENSE.md

[code-of-conduct-badge]: https://img.shields.io/badge/code%20of%20conduct%20-Contributor%20Covenant%20v2.0-ff69b4.svg
[code-of-conduct-url]: https://gitlab.com/beescreens/beescreens-api/blob/master/CODE_OF_CONDUCT.md

[contributor-url]: https://gitlab.com/beescreens/beescreens-api/blob/master/CONTRIBUTING.md

[changelog-badge]: https://img.shields.io/badge/changelog-Keep%20a%20Changelog%20v1.1.0-%23E05735.svg
[changelog-url]: https://gitlab.com/beescreens/beescreens-api/blob/master/CHANGELOG.md

[telegram-channel-badge]: https://img.shields.io/badge/telegram-BeeScreens%20Channel-blue.svg
[telegram-channel-url]: https://t.me/beescreens

[telegram-community-badge]: https://img.shields.io/badge/telegram-BeeScreens%20Community-blue.svg
[telegram-community-url]: https://t.me/beescreens_community

[beescreens-url]: https://beescreens.ch
[docker-files-website]: https://docs.docker.beescreens.ch

[repository-url]: https://gitlab.com/beescreens/beescreens-api
[issue-url]: https://gitlab.com/beescreens/beescreens-api/blob/master/ISSUE_TEMPLATE.md
