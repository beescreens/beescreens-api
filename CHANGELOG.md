# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.0] - 2020-05-22

### Changed

- Changed objects from `Created*` to `*Created`
- Changed objects from `Updated*` to `*Updated`
- Changed objects from `Deleted*` to `*Deleted`
- Uniformed the `new*` to `*Created` for consistency

### Removed

- Removed `*One*` and `*All*` from codebase and documentation as the singular and plural forms are sufficient.

## [1.1.0] - 2020-05-14

### Added

- Set up a complete pipeline regarding the application:
  - Be able to create a application
  - Access with an API key
  - Revoke an API key
  - Send a message to a specific application

### Changed

- Updated incorrect swagger documentation

## [1.0.0] - 2020-05-13

### Added

- Set up a complete pipeline regarding the displayer:
  - Be able to create a displayer
  - Access with an API key
  - Revoke an API key
  - Send a message to a specific displayer
  - Notify the server the displayer's resolution has changed

### Changed

- Changed `Read*` to `Created*`, `Updated*` and `Deleted*` interfaces and DTO where appropriated
- WebSockets events `AUTHENTICATE` and `AUTHENTICATED` are replaced with `JOIN_AS_INTERNAL_ENTITY`, `JOIN_AS_EXTERNAL_ENTITY` and `JOINED`

## [0.0.2] - 2020-04-17

### Added

- Set up a complete pipeline regarding the API:
  - Be able to create a user
  - Validate inputs/outputs
  - Swagger-UI
  - Connected to a SQLite database
  - Unit testing
  - Be able to send messages through WebSockets

## [0.0.1] - 2020-02-17

### Added

- Set up a complete pipeline with best pratices regarding:
  - Documentation
  - Contributions files
  - CI/CD
  - Unit testing
  - Building packages
  - Auditing
  - Linting
  - Scripts helpers

[2.0.0]: https://gitlab.com/beescreens/beescreens-api/-/releases#2.0.0
[1.1.0]: https://gitlab.com/beescreens/beescreens-api/-/releases#1.1.0
[1.0.0]: https://gitlab.com/beescreens/beescreens-api/-/releases#1.0.0
[0.0.2]: https://gitlab.com/beescreens/beescreens-api/-/releases#0.0.2
[0.0.1]: https://gitlab.com/beescreens/beescreens-api/-/releases#0.0.1
